<h3 class="text-center">Contact</h3>
<div class="form-group">
    <b>First Name:</b> {{$dados['first_name']}}
</div>
<div class="form-group">
    <b>Last Name:</b> {{$dados['last_name']}}
</div>
<div class="form-group">
    <b>E-mail:</b> {{$dados['email']}}
</div>
<div class="form-group">
    <b>Topic:</b> {{$dados['topic']}}
</div>
<div class="form-group">
    <b>Message:</b> {{$dados['message']}}
</div>
<div class="form-group">
    <b>I wish to be called</b> <b>Topic:</b> {{$dados['called']}}
</div>
<div class="form-group">
    <b>I want to sign up for a webinar</b> {{$dados['webinar']}}
</div>
<div class="form-group">
    <b>Phone number</b> {{$dados['phone']}}
</div>