<p>Hello {{$dados['first_name']}},</p>
<br>
<br>
<p>The following message has been sent successfully, thanks for contacting us! We will get back to you as soon as we can.</p>
<br>
--
<p>
<a href="http://studentbackr.com/wp-content/uploads/2015/04/green-logo-for-signature.png">
    <img src="http://studentbackr.com/wp-content/uploads/2015/04/green-logo-for-signature.png" alt="green logo for signature" width="110" height="29" />
</a></p>
<p>The StudentBackr Team - www.studentbackr.com - JUST FOR STUDENTS!</p>
<p><em>The information transmitted is intended only for the person to whom or entity to which it is addressed and may contain confidential and/or privileged attachment. Any review, retransmission, dissemination or other use of, or taking of any action in reliance upon this information by persons or entities other than the intended recipient, is prohibited.If you received this in error, please contact us at contact@studentbackr.com and delete all copies of the message and its attachment.</em></p>