<?php
$page_title = "Pages";
$page_description = "Manage Pages";
?>
@extends('admin_template')

@section('content')
    <div ng-controller="pagesController">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header" style="height: 46px;">
                        <a href="#" ng-click="toggle('add',page.id)" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-plus"></i> Add new</a>
                        <div class="box-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="search" ng-model="searchName" name="table_search" class="form-control pull-right" placeholder="Search">

                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody><tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Language</th>
                                <th>Shortcut</th>
                                <th>Actions</th>
                            </tr>
                            <tr ng-repeat="page in pages| filter:searchName">
                                <td>@{{page.id}}</td>
                                <td>@{{page.title}}</td>
                                <td>@{{page.language}}</td>
                                <td>@{{page.shortcut}}</td>
                                <td>
                                <a href="#" ng-click="toggle('edit',page.id)" class="btn btn-primary btn-xs" title="Edit"><i class="glyphicon glyphicon-pencil"></i> </a>
                                    <a href="#" class="btn btn-danger btn-xs" title="Remove"><i class="glyphicon glyphicon-remove"></i></a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="pageModal" tabindex="-1" role="dialog" aria-labelledby="projectModalLabel">
            <form name="formPage" id="formPage" class="form-horizontal" ng-upload="uploadComplete(content)" action="/api/v1/uppages">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="projectModalLabel">@{{form_title}}</h4>
                    </div>
                    <div class="modal-body">                        
                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="control-label">Title</label>
                                    <input type="text" class="form-control" ng-model="page.title">
                                </div>
                                <div class="col-sm-4">
                                    <label class="control-label">Shortcut</label>
                                    <input type="text" class="form-control" ng-model="page.shortcut">
                                </div>
                                <div class="col-sm-4">
                                    <label class="control-label">Language</label><br>
                                    <select class="form-control" ng-model="page.language">
                                        <option value="EN">English</option>
                                        <option value="FR">French</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">SEO Title</label>
                                    <input type="text" class="form-control" ng-model="page.seo_title">
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">SEO Description</label>
                                    <textarea class="form-control" ng-model="page.seo_description"></textarea>
                                </div>
                            </div>
                            <div class="row"><!-- SEO photo start -->
                                <div class="col-md-8">
                                    <label for="" class="control-label">SEO Photo</label>                                
                                    <input type="file" name="seo_image" id="seo_image" nv-file-select uploader="uploader" id="file-seo_image"/>
                                    <br><small style="font-weight: normal">Dimensions: 1200 x 627</small>
                                    <div ng-messages="formPage.seo_image.$error">
                                        <span ng-show="formPage.seo_image.$error.required" class="help-inline">{{trans('front.required')}}</span>
                                    </div>
                                <div class="col-md-4">
                                    <table class="table" ng-show="uploader.queue.length > 0">
                                        <tbody>
                                        <tr ng-repeat="item in uploader.queue">
                                            <td style="border: 0 !important"><button type="button" class="btn btn-success btn-xs" ng-click="item.upload()" ng-disabled="item.isReady || item.isUploading || item.isSuccess">
                                                    <span class="glyphicon glyphicon-check"></span> {{trans('front.click_upload')}}
                                                </button></td>
                                            <td ng-show="uploader.isHTML5" width="50%" style="border: 0 !important">
                                                <div class="progress" style="margin-bottom: 0;" ng-show="!item.isSuccess">
                                                    <div class="progress-bar" role="progressbar" ng-style="{ 'width': item.progress + '%' }"></div>
                                                </div>
                                                <span ng-show="item.isSuccess"><i class="glyphicon glyphicon-ok"></i></span>
                                                <span ng-show="item.isCancel"><i class="glyphicon glyphicon-ban-circle"></i></span>
                                                <span ng-show="item.isError"><i class="glyphicon glyphicon-remove"></i></span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div><!-- SEO photo end -->
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="control-label">Content</label>
                                    <text-angular ng-model="page.content"></text-angular>
                                </div>
                            </div>                        
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" ng-model="project.id_student">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" ng-click="save(modalstate, id)">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
@endsection