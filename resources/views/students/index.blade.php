<?php
$page_title = "Students";
$page_description = "Manage Students";
?>
@extends('admin_template')

@section('content')
    <div ng-controller="studentsController">
        <div class="row"> <!-- Top | Useful data -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-blue"><i class="ion ion-ios-people-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Students</span>
                        <span class="info-box-number">@{{students.length}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
        </div><!-- Top end -->

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header" style="height: 46px;">
                        <div class="box-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="search" ng-model="searchName" name="table_search" class="form-control pull-right" placeholder="Search">

                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody><tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>E-mail</th>
                                <th>Language</th>
                                <th>Registration Date</th>
                                <th>MangoPay ID</th>
                                <th>Type</th>
                                <th>Actions</th>
                            </tr>
                            <tr ng-repeat="student in students | filter:searchName">
                                <td ng-cloak>@{{student.id}}</td>
                                <td ng-cloak>@{{student.first_name}} @{{student.last_name}}</td>
                                <td ng-cloak>@{{student.email}}</td>
                                <td ng-cloak>@{{student.language}}</td>
                                <td ng-cloak>@{{student.created_at}}</td>
                                <td ng-cloak>@{{student.mp_user_id}} <a href="#" class="btn btn-warning btn-xs" ng-if="!student.mp_user_id" ng-click="newmpuser(student.id)"><b>Generate MP ID</b></a></td>
                                <td ng-cloak><span class="@{{ student.mp_user_id?'label label-success':'label label-danger' }}">@{{student.mp_user_id?'Verified':'Not Verified'}}</span></td>
                                <td>
                                    <a href="#" ng-click="toggle('edit',student.id)" class="btn btn-primary btn-xs" title="Edit"><i class="glyphicon glyphicon-pencil"></i> </a>
                                    <a href="#" class="btn btn-danger btn-xs" title="Remove"><i class="glyphicon glyphicon-remove"></i></a>
                                    <a href="#" class="btn btn-info btn-xs" title="See Projects"><i class="glyphicon glyphicon-th-list"></i></a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="studentModal" tabindex="-1" role="dialog" aria-labelledby="studentModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="studentModalLabel">@{{form_title}}</h4>
                    </div>
                    <div class="modal-body">
                        <form name="formStudent" class="form-horizontal">
                            <div class="row">
                                <div class="col-sm-8">
                                    <label class=" control-label">First Name</label>
                                    <input type="text" class="form-control" ng-model="student.first_name">
                                    <label class="control-label">Last Name</label>
                                    <input type="text" class="form-control" ng-model="student.last_name">
                                </div>
                                <div class="col-sm-4" style="text-align: center;padding-top: 30px">
                                    <img ng-src="@{{student.photo}}" height="80">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="control-label">Date of birth</label>
                                    <input type="text" class="form-control" ng-model="student.dob">
                                </div>
                                <div class="col-sm-6">
                                    <label class="control-label">Gender</label>
                                    <select class="form-control" ng-model="student.gender">
                                        <option value="M" ng-selected="student.gender=='M'">Male</option>
                                        <option value="F" ng-selected="student.gender=='F'">Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="control-label">E-mail</label>
                                    <input type="email" class="form-control" ng-model="student.email">
                                </div>
                                <div class="col-sm-6">
                                    <label class="control-label">Country</label>
                                    <select class="form-control" ng-model="student.id_country" ng-options="country.id as country.name for country in countries">
                                        <option value="">Select an option</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="control-label">Address</label>
                                    <input type="text" class="form-control" ng-model="student.address">
                                </div>
                                <div class="col-sm-6">
                                    <label class="control-label">City</label>
                                    <input type="text" class="form-control" ng-model="student.city">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="control-label">Phone</label>
                                    <input type="text" class="form-control" ng-model="student.phone">
                                </div>
                                <div class="col-sm-6">
                                    <label class="control-label">Annual income</label>
                                    <input type="text" class="form-control" ng-model="student.annual_income">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12"><br>
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>IBAN</th>
                                            <th>BIC</th>
                                            <th>ACCOUNT</th>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;@{{ student.iban }}</td>
                                            <td>&nbsp;@{{ student.bic }}</td>
                                            <td>&nbsp;@{{ student.account_number }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="control-label">Biographical Information</label>
                                    <text-angular ng-model="student.biographical_inf"></text-angular>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" ng-click="save(modalstate, id)">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection