<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        studentbackr.com
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">Student Backr</a>.</strong> All rights reserved.
</footer>