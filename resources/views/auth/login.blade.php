@include('front.header')

<div class="container signup-login">
        <div class="row">
            <div class="panel panel-primary col-md-12">
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                        <!--
                        <div class="form-group">
                            <p class="text-center"><small>Login with Facebook</small></p>
                            <button id="signupSubmit" type="submit" class="btn btn-facebook btn-block btn-lg">Login with Facebook</button>
                        </div>
                        <p class="text-center">OR</p>
                        <p class="text-center"><small>Login with Email</small></p>
                        -->
                        <h3 class="text-center">{{ trans('front.login') }}</h3>
                        <div class="form-group">
                            <input id="email" placeholder="{{ trans('front.your_email') }}" type="email" class="form-control" name="email" value="{{ old('email') }}">

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input id="password" placeholder="{{ trans('front.your_password') }}" type="password" class="form-control" name="password">

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> {{ trans('front.remember_me') }}
                            </label>
                            <br>
                            <a href="/password/reset">{{trans('front.forgot_password')}}</a>
                        </div>


                        <div class="form-group">
                            <button id="signupSubmit" type="submit" class="btn btn-info btn-block btn-lg">{{ trans('front.login') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@include('front.footer')
