<?php
$page_title = "Payout";
$page_description = "Create Payout";
?>
@extends('admin_template')

@section('content')
    <div class="row"> <!-- Top | Useful data -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-blue"><i class="fa fa-desktop"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Project</span>
                    <span class="info-box-number">{{$project->title}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-user"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Student</span>
                    <span class="info-box-number">{{$student->first_name}} {{$student->last_name}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-orange"><i class="fa fa-money"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Target</span>
                    <span class="info-box-number">{{$project->target}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-bank"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Total Funded</span>
                    <span class="info-box-number">{{$total_funded}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>

        </div>
    </div><!-- Top end -->

    <?php
    if(!$msg){
        $tx = $total_funded >= $project->target?7:10;
        $fees = ($tx * $total_funded) / 100;
    ?>
        <form method="post" action="/admin/payout/1">
            <div class="row">
                <div class="col-md-12">
                    <p class="bg-primary" style="padding: 6px">Confirm the data bellow and click in the button to confirm the <b>PAYOUT</b></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <label>Author ID:</label>
                     <input type="text" class="form-control" name="author_id" readonly value="{{$student->mp_user_id}}">
                </div>
                <div class="col-md-6 form-group">
                    <label>Wallet ID:</label>
                    <input type="text" class="form-control" name="wallet_id" readonly value="{{$project->mp_wallet_id}}">
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group">
                    <label>Currency:</label>
                    <input type="text" name="currency" readonly class="form-control" value="{{$project->currency}}">
                </div>
                <div class="col-md-6 form-group">
                    <label>Amount:</label>
                    <input type="text" class="form-control" readonly name="amount" value="{{$total_funded*100}}">
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group">
                    <label>Fees:</label>
                    <input type="text" name="fees" readonly class="form-control" value="{{$fees*100}}">
                </div>
                <div class="col-md-6 form-group">
                    <label>Bank ID:</label>
                    <input type="text" class="form-control" readonly name="bank_id" value="{{$student->mp_bank_id}}">
                </div>
            </div>
            <div class="form-group">
                <input type="submit" name="act" value="Generate Payout" class="btn btn-primary">
            </div>
            <div class="row col-md-12">
                <b>ATENTION:</b> Please contact the support if the data is wrong.
            </div>
        </form>
    <?php
    }
    ?>
@endsection