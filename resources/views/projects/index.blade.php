<?php
$page_title = "Projects";
$page_description = "Manage Projects";
?>
@extends('admin_template')

@section('content')
    <div ng-controller="projectsController">
        <div class="row"> <!-- Top | Useful data -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-blue"><i class="ion ion-ios-people-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">All Projects</span>
                        <span class="info-box-number">@{{projects.length}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
        </div><!-- Top end -->

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header" style="height: 46px;">
                        <div class="box-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="search" ng-model="searchName" name="table_search" class="form-control pull-right" placeholder="Search">

                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody><tr>
                                <th>ID</th>
                                <th>Student</th>
                                <th>Creation Date</th>
                                <th>Title</th>
                                <th>Target</th>
                                <th>End Date</th>
                                <th>Status</th>
                                <th>Language</th>
                                <th>Actions</th>
                            </tr>
                            <tr ng-repeat="project in projects | filter:searchName">
                                <td ng-cloak>@{{project.id}}</td>
                                <td ng-cloak>@{{project.student.first_name}} @{{project.student.last_name}}<br>@{{ project.student.email }}</td>
                                <td ng-cloak>@{{project.created_at}}</td>
                                <td ng-cloak>@{{project.title}}</td>
                                <td ng-cloak>@{{project.target}}</td>
                                <td ng-cloak>@{{project.end_date}}</td>
                                <td ng-cloak>@{{showStatus(project.status)}}</td>
                                <td ng-cloak>@{{project.language}}</td>
                                <td>
                                    <a href="#" ng-click="toggle('edit',project.id)" class="btn btn-primary btn-xs" title="Edit"><i class="glyphicon glyphicon-pencil"></i> </a>
                                    <a href="#" ng-click="confirmDelete(project.id)" class="btn btn-danger btn-xs" title="Remove"><i class="glyphicon glyphicon-remove"></i></a>
                                    <a href="#" ng-click="showDonations(project)" class="btn btn-success btn-xs" title="See Donations"><i class="fa fa-money"></i></a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="projectModal" tabindex="-1" role="dialog" aria-labelledby="projectModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="projectModalLabel">@{{form_title}}</h4>
                    </div>
                    <div class="modal-body">
                        <form name="formProject" class="form-horizontal">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="control-label">Title</label>
                                    <input type="text" class="form-control" ng-model="project.title">
                                </div>
                                <div class="col-sm-6">
                                    <label class="control-label">Student</label><br>
                                    @{{project.student.first_name}} @{{project.student.last_name}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="control-label">Target</label>
                                    <input type="text" class="form-control" ng-model="project.target">
                                </div>
                                <div class="col-sm-6">
                                    <label class="control-label">Status</label><br>
                                    <select class="form-control" ng-model="project.status" ng-options="st.id as st.label for st in opstatus"></select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="control-label">End Date</label>
                                    <p class="input-group">
                                        <input type="text" class="form-control" uib-datepicker-popup="@{{format}}" ng-model="end_date" ng-model-options="{timezone: +0000}" is-open="popup1.opened" datepicker-options="dateOptions" ng-required="true" close-text="Close" alt-input-formats="altInputFormats" />
                                          <span class="input-group-btn">
                                            <button type="button" class="btn btn-default" ng-click="open1()"><i class="glyphicon glyphicon-calendar"></i></button>
                                          </span>
                                    </p>
                                </div>
                                <div class="col-sm-6">
                                    <label class="control-label">Featured</label>
                                    <select class="form-control" ng-model="project.featured">
                                        <option value="N">No</option>
                                        <option value="Y">Yes</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="control-label">Categories</label>
                                    <select class="form-control" ng-model="project.id_category">
                                        <option value="@{{ category.id }}" ng-repeat="category in categories_proj">@{{ category.name }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="control-label">Rewards</label>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Description</th>
                                                <th>Minimum</th>
                                                <th>Quantity</th>
                                                <th>Purchase</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="reward in rewards">
                                                <td>@{{ reward.title }}</td>
                                                <td>@{{ reward.description }}</td>
                                                <td>@{{ reward.minimum }}</td>
                                                <td>@{{ reward.quantity }}</td>
                                                <td>@{{ reward.purchase }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="control-label">Story</label>
                                    <text-angular ng-model="project.story"></text-angular>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" ng-model="project.id_student">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" ng-click="save(modalstate, id)">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Donations-->
        <div class="modal fade" id="donationModal" tabindex="-1" role="dialog" aria-labelledby="donationModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="donationModalLabel">Donations</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered">
                            <tr>
                                <th class="info">Total</th>
                                <th class="success">Success</th>
                                <th class="danger">Fail</th>
                                <th class="active">Target</th>
                                <th class="active">Fees</th>
                                <th class="info" style="width: 3%;">$ <i class="glyphicon glyphicon-share-alt"></i> </th>
                            </tr>
                            <tr>
                                <td>@{{ total | currency:type_currency}}</td>
                                <td>@{{ totalS | currency:type_currency}} </td>
                                <td>@{{ totalF | currency:type_currency}}</td>
                                <td>@{{ target | currency:type_currency}}</td>
                                <td>@{{ fee_value | currency:type_currency}} (@{{ fee }}%)</td>
                                <td><a href="/admin/payout/@{{ project_id }}" class="btn btn-primary btn-xs">Payout</a></td>
                            </tr>
                        </table>

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Donator</th>
                                <th>E-mail</th>
                                <th>Date</th>
                                <th>Amount</th>
                                <th>Reward</th>
                                <th>Status</th>
                                <th>MP response</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="donation in donations" style="font-size: 12px">
                                <td>@{{ donation.donator_name }}</td>
                                <td>@{{ donation.donator_email }}</td>
                                <td>@{{ donation.date }}</td>
                                <td>@{{ donation.amount }} @{{ donation.currency }}</td>
                                <td>@{{ donation.reward_name }}</td>
                                <td ng-bind-html="showRstatus(donation.status)"></td>
                                <td>@{{ donation.mp_return }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" ng-model="project.id_student">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection