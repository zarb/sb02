<?php
$page_title = "Categories";
$page_description = "Manage Categories";
?>
@extends('admin_template')

@section('content')
    <div ng-controller="categoriesController">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header" style="height: 46px;">
                        <a href="#" ng-click="toggle('add',category.id)" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-plus"></i> Add new</a>
                        <div class="box-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="search" ng-model="searchName" name="table_search" class="form-control pull-right" placeholder="Search">

                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody><tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Active</th>
                                <th>Language</th>
                                <th>Created at</th>
                                <th>Updated at</th>
                                <th>Actions</th>
                            </tr>
                            <tr ng-repeat="category in categories| filter:searchName">
                                <td>@{{category.id}}</td>
                                <td>@{{category.name}}</td>
                                <td>@{{category.active=='Y'?'Yes':'No'}}</td>
                                <td>@{{category.language}}</td>
                                <td>@{{category.created_at}}</td>
                                <td>@{{category.updated_at}}</td>
                                <td>
                                <a href="#" ng-click="toggle('edit',category.id)" class="btn btn-primary btn-xs" title="Edit"><i class="glyphicon glyphicon-pencil"></i> </a>
                                    <a href="#" class="btn btn-danger btn-xs" title="Remove"><i class="glyphicon glyphicon-remove"></i></a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="categoryModal" tabindex="-1" role="dialog" aria-labelledby="projectModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="projectModalLabel">@{{form_title}}</h4>
                    </div>
                    <div class="modal-body">
                        <form name="formProject" class="form-horizontal">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="control-label">Name</label>
                                    <input type="text" class="form-control" ng-model="category.name">
                                </div>
                                <div class="col-sm-6">
                                    <label class="control-label">Active</label><br>
                                    <select class="form-control" ng-model="category.active">
                                        <option value="Y">Yes</option>
                                        <option value="N">No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="control-label">Language</label><br>
                                    <select class="form-control" ng-model="category.language">
                                        <option value="EN">English</option>
                                        <option value="FR">French</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" ng-model="project.id_student">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" ng-click="save(modalstate, id)">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection