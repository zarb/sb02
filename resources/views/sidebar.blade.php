<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        @if (!Auth::guest())
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <!-- <img src="bower_components/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> -->
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">MENU</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="active"><a href="/admin/students"><i class="fa fa-mortar-board"></i> <span>Students</span></a></li>
            <li><a href="/admin/projects"><i class="fa fa-align-justify"></i> <span>Projects</span></a></li>
            <li><a href="/admin/categories"><i class="fa fa-th-list"></i> <span>Categories</span></a></li>
            <li><a href="/admin/pages"><i class="fa fa-files-o"></i> <span>Pages</span></a></li>
            <li><a href="/admin/notifications"><i class="fa fa-envelope"></i> <span>Notifications</span></a></li>
            <li><a href="/admin/donations"><i class="fa fa-money"></i> <span>Donations</span></a></li>
        </ul>
        <!-- /.sidebar-menu -->
        @endif
    </section>
    <!-- /.sidebar -->
</aside>