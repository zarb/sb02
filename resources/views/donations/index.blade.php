<?php
$page_title = "Donations";
$page_description = "Manage Donations";
?>
@extends('admin_template')

@section('content')
    <div ng-controller="donationsController">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header" style="height: 46px;">
                        <div class="box-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="search" ng-model="searchName" name="table_search" class="form-control pull-right" placeholder="Search">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody><tr>
                                <th>ID</th>
                                <th>Date</th>
                                <th>Project</th>
                                <th>Reward</th>
                                <th>Amount</th>
                                <th>Donator</th>      
                                <th>Status</th>
                                <th>MangoPay ID</th>                          
                            </tr>
                            <tr ng-repeat="donation in donations | filter:searchName">
                                <td>@{{donation.id}}</td>
                                <td>@{{donation.date}}</td>
                                <td>@{{donation.project}}</td>
                                <td>@{{donation.reward_name}}</td>
                                <td>@{{donation.amount}} @{{donation.currency}}</td>
                                <td>@{{donation.donator_name}}<br>@{{donation.donator_email}}</td>
                                <td><span ng-if="donation.status=='S'" class="label label-success">Success</span><span ng-if="donation.status!='S'" class="label label-danger">Failed</span></td>
                                <td>@{{donation.mp_id}}<br>@{{donation.message}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>

    </div>
@endsection