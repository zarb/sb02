@if (!Auth::check())
<div class="cta-bar">
    <div class="container">
        <p class="text-center">{{trans('front.footer_tip')}}
            <a href="/signup{{app()->getLocale()!='en'&&app()->getLocale()?'/'.app()->getLocale():''}}" class="btn btn-info btn-lg">{{trans('front.sign_up')}}</a>
        </p>
    </div>
</div>
@endif

<footer>
    <div class="container">
        <div class="col-md-3 col-sm-3 col-xs-12">
            <img src="/front/img/logo-STUDENTBACKR.png" alt="Student Backr" class="img-responsive" width="160">
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <ul class="list-unstyled">
                @if(app()->getLocale()=='en' || !app()->getLocale())
                    <!-- <li><a href="/posting-crowdfunding-updates{{app()->getLocale()!='en'&&app()->getLocale()?'/'.app()->getLocale():''}}">{{trans('front.posting_crowdfunding_updates')}}</a></li> -->
                    <li><a href="/how-it-works/#the_process">{{trans('front.the_process')}}</a></li>
                    <li><a href="/how-it-works/#starting_off">{{trans('front.starting_off')}}</a></li>
                    <li><a href="/how-it-works/#prepare_promote">{{trans('front.prepare_&_promote')}}</a></li>
                    <li><a href="/how-it-works/#pricing">{{trans('front.pricing')}}</a></li>
                    <li><a href="/how-it-works/#faq">{{trans('front.faq')}}</a></li>
                    <li><a href="/contact{{app()->getLocale()!='en'&&app()->getLocale()?'/'.app()->getLocale():''}}">{{trans('front.contact_us')}}</a></li>
                @elseif(app()->getLocale()=='fr')
                    <!-- <li><a href="/posting-crowdfunding-updates{{app()->getLocale()!='en'&&app()->getLocale()?'/'.app()->getLocale():''}}">{{trans('front.posting_crowdfunding_updates')}}</a></li> -->
                    <li><a href="/how-it-works/?lang=fr#the_process">{{trans('front.the_process')}}</a></li>
                    <li><a href="/how-it-works/?lang=fr#starting_off">{{trans('front.starting_off')}}</a></li>
                    <li><a href="/how-it-works/?lang=fr#prepare_promote">{{trans('front.prepare_&_promote')}}</a></li>
                    <li><a href="/how-it-works/?lang=fr#pricing">{{trans('front.pricing')}}</a></li>
                    <li><a href="/how-it-works/?lang=fr#faq">{{trans('front.faq')}}</a></li>
                    <li><a href="/contact{{app()->getLocale()!='en'&&app()->getLocale()?'/'.app()->getLocale():''}}">{{trans('front.contact_us')}}</a></li>
                @endif
            <ul>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <ul class="list-unstyled">
                @if(app()->getLocale()=='en' || !app()->getLocale())
                    <li><a href="/about-us{{app()->getLocale()!='en'&&app()->getLocale()?'/'.app()->getLocale():''}}">{{trans('front.about_us')}}</a></li>
                    <li><a href="/sponsorship-programme{{app()->getLocale()!='en'&&app()->getLocale()?'/'.app()->getLocale():''}}">{{trans('front.corporate_sponsors')}}</a></li>
                    <li><a href="/partnerships{{app()->getLocale()!='en'&&app()->getLocale()?'/'.app()->getLocale():''}}">{{trans('front.partnerships')}}</a></li>
                    <li><a href="/press{{app()->getLocale()!='en'&&app()->getLocale()?'/'.app()->getLocale():''}}">{{trans('front.press')}}</a></li>
                    <li><a href="/terms-and-conditions{{app()->getLocale()!='en'&&app()->getLocale()?'/'.app()->getLocale():''}}">{{trans('front.terms_and_conditions')}}</a></li>
                    <li><a href="/legal-notices{{app()->getLocale()!='en'&&app()->getLocale()?'/'.app()->getLocale():''}}">{{trans('front.legal_notices')}}</a></li>
                    <li><a href="http://studentbackr.us12.list-manage2.com/subscribe?u=0ab4150de2e24b1a6dadde6cf&id=a9514ef1d7">{{trans('front.newsletter')}}</a></li>
                    <li><a href="/backers{{app()->getLocale()!='en'&&app()->getLocale()?'/'.app()->getLocale():''}}">{{trans('front.backers')}}</a></li>
                @elseif(app()->getLocale()=='fr')
                    <li><a href="/about-us{{app()->getLocale()!='en'&&app()->getLocale()?'/'.app()->getLocale():''}}">{{trans('front.about_us')}}</a></li>
                    <li><a href="/sponsorship-programme{{app()->getLocale()!='en'&&app()->getLocale()?'/'.app()->getLocale():''}}">{{trans('front.corporate_sponsors')}}</a></li>
                    <li><a href="/partnerships{{app()->getLocale()!='en'&&app()->getLocale()?'/'.app()->getLocale():''}}">{{trans('front.partnerships')}}</a></li>
                    <li><a href="/press{{app()->getLocale()!='en'&&app()->getLocale()?'/'.app()->getLocale():''}}">{{trans('front.press')}}</a></li>
                    <li><a href="/terms-and-conditions{{app()->getLocale()!='en'&&app()->getLocale()?'/'.app()->getLocale():''}}">{{trans('front.terms_and_conditions')}}</a></li>
                    <li><a href="/legal-notices{{app()->getLocale()!='en'&&app()->getLocale()?'/'.app()->getLocale():''}}">{{trans('front.legal_notices')}}</a></li>
                    <li><a href="http://studentbackr.us12.list-manage2.com/subscribe?u=0ab4150de2e24b1a6dadde6cf&id=a9514ef1d7">{{trans('front.newsletter')}}</a></li>
                    <li><a href="/backers{{app()->getLocale()!='en'&&app()->getLocale()?'/'.app()->getLocale():''}}">{{trans('front.backers')}}</a></li>
                @endif

            <ul>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="dropdown pull-right">
                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <span class="c_lang">{{trans('front.change_language')}}</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li><a @if(app()->getLocale() && app()->getLocale()!='en')onclick="change_lang('{{app()->getLocale()}}','en')"@endif>English</a></li>
                    <li><a @if(app()->getLocale() != 'fr')onclick="change_lang('{{app()->getLocale()}}','fr')"@endif>French</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins)
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
-->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Angular -->
<script src="<?= asset('app/lib/angular/angular.min.js') ?>"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-animate.js"></script>

<script src="<?= asset('front/js/jquery.min.js');?>"></script>
<script src="<?= asset('front/js/bootstrap.min.js');?>"></script>
<script src="<?= asset('front/js/jquery.bootstrap.wizard.js');?>"></script>
<script src="<?= asset('front/js/jquery.validate.min.js');?>"></script>
<script src="<?= asset('front/js/wizard.js');?>"></script>
<script src="<?= asset('js/jquery.flip.min.js');?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>

<script src="<?= asset('app/app.js');?>"></script>
<script src="<?= asset('app/controllers/home.front.js');?>"></script>
<script src="<?= asset('app/controllers/all.front.js');?>"></script>
<script src="<?= asset('app/controllers/newproject.js');?>"></script>
<script src="<?= asset('app/controllers/projects.js');?>"></script>
<script src="<?= asset('app/controllers/student.js');?>"></script>
<script src="<?= asset('app/controllers/updates.js');?>"></script>

<link rel='stylesheet' href='{{ asset("/bower_components") }}/textAngular/dist/textAngular.css'>
<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-2.0.2.js"></script>
<script src='{{ asset("/bower_components") }}/textAngular/dist/textAngular-rangy.min.js'></script>
<script src='{{ asset("/bower_components") }}/textAngular/dist/textAngular-sanitize.min.js'></script>
<script src='{{ asset("/bower_components") }}/textAngular/dist/textAngular.min.js'></script>
<script src="{{ asset("/bower_components") }}/angular-input-masks/angular-input-masks-standalone.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js"></script>
<!-- Angular File Upload -->
<script src="{{ asset("/node_modules") }}/angular-file-upload/dist/angular-file-upload.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js"></script>
<script src="{{ asset("/front") }}/js/ekko-lightbox.min.js"></script>

<script>
    function change_lang(lang,new_lang){
        _end = window.location.href.slice(-2);

        if (!lang) {
            new_url = '/?lang=' + new_lang;
        }
        else if (lang != new_lang) {
            if (lang == _end) { // already have language at the end of url
                new_url = window.location.href.slice(0, -2) + new_lang;
            }
            else {
                new_url = window.location.href + '/' + new_lang;
            }
        }
        else
            new_url = window.location.href + '/' + new_lang;

        <?php
        //custom rules for specific pages
        if(isset($is_home)){
            echo "new_url = '/?lang=' + new_lang;";
        }
        else if(isset($is_wizard)){
            echo "new_url = '?lang=' + new_lang;";
        }
        else if(isset($page)){
            if($page == 'how-it-works' || $page == 'comment-ca-marche'){
                echo 'var url = window.location.href';
                echo "\n";
                echo 'var hash = url.substring(url.indexOf("#")+1);';
                echo "\n";
                echo "new_url = '/how-it-works/?lang=' + new_lang + '#' + hash;";
            }
            else if($page == 'project'){
                echo "new_url = '/project/".$project->id."/?lang=' + new_lang";
            }
        }
        ?>

        new_url = new_url.replace('m//','m/');

        document.location=new_url;
    }
    $(document).ready(function(){
        @if(!app()->getLocale() || strtolower(app()->getLocale()) == 'en')
            $('.c_lang').text('English');
        @elseif(strtolower(app()->getLocale()) == 'fr')
            $('.c_lang').text('Français');
        @endif
    })
</script>
</body>
</html>
