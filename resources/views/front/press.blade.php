@include('front.header')
<section class="container">
    <h2 class="text-center">{{trans('front.press1')}}</h2>
    <p class="text-center">{{trans('front.press2')}}</p>
    <p>&nbsp;</p>
    <div class="row">
        <div class="col-md-3">
            <div class="thumbnail_container" style="height: 220px">
                <div class="thumbnail">
                    <a href="http://www.letudiant.fr/loisirsvie-pratique/aides-financieres/crowdfunding-trois-sites-pour-financer-vos-etudes-au-banc-d-essai.html">
                    <img src="http://studentbackr.com/wp-content/uploads/2015/11/logo-letudiant.jpg" class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="thumbnail_container" style="height: 220px">
                <div class="thumbnail">
                    <a href="http://www.assurbanque20.fr/2015/11/31301-finance-innovation-va-labelliser-une-centaine-de-fintechs/">
                    <img src="http://studentbackr.com/wp-content/uploads/2015/11/logo-ab20-header.png" class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="thumbnail_container" style="height: 220px">
                <div class="thumbnail">
                    <a href="http://www.finyear.com/La-Finance-se-mobilise-autour-des-FINTECH-pour-liberer-leur-potentiel-de-croissance_a34657.html">
                    <img src="http://studentbackr.com/wp-content/uploads/2015/11/mod-7452306.png" class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="thumbnail_container" style="height: 220px">
                <div class="thumbnail">
                    <img src="http://studentbackr.com/wp-content/uploads/2016/09/logo-mesetudes.png" class="img-responsive">
                </div>
            </div>
        </div>
    </div>
    <p>&nbsp;</p>
    <div class="row">
        <div class="col-md-3">
            <div class="thumbnail_container" style="height: 220px">
                <div class="thumbnail">
                    <a href="http://www.theoueb.com/site-6399-studentbackr.php">
                    <img src="http://studentbackr.com/wp-content/uploads/2015/11/logo-1.png" class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="thumbnail_container" style="height: 220px">
                <div class="thumbnail">
                    <a href="http://www.argusdelassurance.com/acteurs/aston-itrade-elue-fintech-de-l-annee.101040">
                    <img src="http://studentbackr.com/wp-content/uploads/2015/11/logo-argus.png" class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="thumbnail_container" style="height: 220px">
                <div class="thumbnail">
                    <a href="http://www.capital.fr/bourse/dossiers/fintech-ces-start-up-font-trembler-les-banques-1120962">
                    <img src="http://www.acpm.fr/var/ojd/storage/files/logos/B/logo_5491.jpg" class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="thumbnail_container" style="height: 220px">
                <div class="thumbnail">
                    <a href="http://etudiant.bfmtv.com/actualites/vie-etudiante/474-comment-financer-ses-etudes.html">
                    <img src="http://studentbackr.com/wp-content/uploads/2016/06/logoBFMTV1.png" class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <p>&nbsp;</p>
    <div class="row">
        <div class="col-md-3">
            <div class="thumbnail_container" style="height: 220px">
                <div class="thumbnail">
                    <a href="http://mcetv.fr/mon-mag-campus/audencia-business-school-publie-1er-livre-blanc-financement-etudes-superieures-1505/">
                    <img src="http://studentbackr.com/wp-content/uploads/2016/06/mcetv-logo.png" class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="thumbnail_container" style="height: 220px">
                <div class="thumbnail">
                    <a href="http://start.lesechos.fr/continuer-etudes/vie-etudiante/toutes-les-astuces-pour-financer-vos-etudes-4618.php">
                    <img src="http://studentbackr.com/wp-content/uploads/2016/06/logo_echo-start.png" class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="thumbnail_container" style="height: 220px">
                <div class="thumbnail">
                    <a href="http://www.eleconomista.es/a-debate/noticias/7967790/11/16/Una-plataforma-francesa-acerca-la-financiacion-participativa-a-estudiantes.html">
                    <img src="http://studentbackr.com/wp-content/uploads/2016/10/elEconomistaes.png" class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <h3 class="text-center">{{trans('front.press3')}}</h3>
    <p class="text-center"><iframe width="500" height="281" src="{{ trans('front.press_video') }}" frameborder="0" allowfullscreen></iframe></p>
    <hr>
    <h3 class="text-center">{{trans('front.IDEAE_title')}}</h3>
    <p class="text-center">{!!trans('front.IDEAE_text')!!}</p>
    <p class="text-center"><img src="/front/img/image_IDEAE.png" alt="Student Backr"></p>
    <hr>
    <h3 class="text-center">{{trans('front.FINTECH_title')}}</h3>
    <p class="text-center">{!!trans('front.FINTECH_text')!!}</p>
    <p class="text-center"><img src="/front/img/image_FINTECH.png" alt="Student Backr"></p>
    <hr>
    <h3 class="text-center">{{trans('front.press9')}}</h3>
    <p class="text-center">
        {{trans('front.press10')}} <img src="http://studentbackr.com/wp-includes/images/smilies/icon_smile.gif" alt=":)">
        <br>
        <img class="aligncenter wp-image-29210 size-full" src="http://studentbackr.com/wp-content/uploads/2015/12/ESCP-europe-SEED.jpg" alt="ESCP europe SEED" width="696" height="462">
    </p>
    <hr>
    <h3 class="text-center">{{trans('front.press5')}}</h3>
    <p class="text-center">{{trans('front.press6')}}</p>
    <p class="text-center"><img src="http://studentbackr.com/wp-content/uploads/2015/12/westartpassiontira.png"></p>
    <h3 class="text-center">{{trans('front.press7')}}</h3>
    <p class="text-center">{{trans('front.click')}} <a href="/yamin-chalabi-ceo" target="_blank">{{trans('front.here')}}</a> {{trans('front.press8')}}</p>
    <p class="text-center"><img src="http://studentbackr.com/wp-content/uploads/2015/12/12247182_1084621411550168_1268225078617145815_n-e1461062477600.jpg" alt="StudentBackr"></p>
    <p><img src="http://studentbackr.com/wp-content/uploads/2015/11/Main-Character-1_150-by-178.png"></p>
</section>

<style>
    .thumbnail_container {
        position: relative;
        width: 100%;
        padding-bottom: 100%;
        margin-bottom:0px;
    }

    .thumbnail {
        position:absolute;
        width:100%;
        height:100%;
        min-height: 200px !important;
    }
    .thumbnail img {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        max-width: 130px !important;
    }
    .thumbnail p{
        padding: 4px;
    }
</style>
@include('front.footer')