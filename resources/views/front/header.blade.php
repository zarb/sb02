<?php
$user = Auth::user();
if(isset($user))
    $student = \App\Student::where('id_user',$user->id)->first();


if(is_array(app()->getLocale()))
    app()->setlocale('en');
?>
<!DOCTYPE html>
<html lang="en" ng-app="sbackrApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="{{isset($content->seo_description)?$content->seo_description:''}}">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{isset($content->seo_title)?$content->seo_title:'StudentBackr'}}" />
    <meta property="og:description" content="{{isset($content->seo_description)?$content->seo_description:''}}" />
    <meta property="og:url" content="{{isset($uri_path)?$uri_path:'http://www.studentbackr.com/'}}" />
    <meta property="og:image" content="http://studentbackr.com/seo_photos/{{isset($content->seo_image)?$content->seo_image:''}}" />
    <meta property="og:locale" content="{{isset($lang)?$lang:'en'}}"/>
    <meta property="og:site_name" content="StudentBackr"/>
    <meta property="article:publisher" content="https://www.facebook.com/studentbackr/"/>

    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:title" content="{{isset($content->seo_title)?$content->seo_title:'StudentBackr'}}"/>
    <meta name="twitter:description" content="{{isset($content->seo_description)?$content->seo_description:''}}"/>
    <meta name="twitter:image" content="http://studentbackr.com/seo_photos/{{isset($content->seo_image)?$content->seo_image:''}}"/>
    <meta name="twitter:site" content="@StudentBackr"/>
    <meta name="twitter:creator" content="@StudentBackr"/>

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>{{isset($content->seo_title)?$content->seo_title:'StudentBackr'}}</title>

    <!-- Bootstrap -->
    <link href="<?= asset('front/css/bootstrap.css');?>" rel="stylesheet">
    <link href="<?= asset('front/css/theme.css');?>" rel="stylesheet">
    <link href="<?= asset('front/css/font-awesome.min.css');?>" rel="stylesheet">
    <link href="<?= asset('front/css/ekko-lightbox.min.css');?>" rel="stylesheet">
    <link rel="shortcut icon" href="http://www.studentbackr.com/wp-content/uploads/2014/07/favicon.ico" />

    <!--
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KKBT5J2');</script>
    <!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KKBT5J2"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Static navbar -->
<nav class="navbar navbar-default navbar-static-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/{{app()->getLocale()!='en'?'?lang='.app()->getLocale():''}}" style="margin:0;padding: 3px"><img src="/front/img/logo-STUDENTBACKR.png" alt="Student Backr" class="img-responsive" width="160"></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class=""><a href="/how-it-works{{app()->getLocale()!='en'?'/'.app()->getLocale():''}}">{{trans('front.how_it_works')}}</a></li>
                <li><a href="/all-projects{{app()->getLocale()!='en'?'/'.app()->getLocale():''}}">{{trans('front.projects')}}</a></li>
                <li><a href="http://blog.studentbackr.com" target="_blank">Blog</a></li>
                <form action="/all-projects{{app()->getLocale()!='en'?'/'.app()->getLocale():''}}" class="navbar-form navbar-left">
                    <div class="form-group search">
                        <input type="text" name="search" class="form-control" placeholder="">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </div>
                </form>
            </ul>
            @if (!Auth::check())
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/signup{{app()->getLocale()!='en'?'/'.app()->getLocale():''}}">{{trans('front.register')}}</a></li>
                <li><a href="/login{{app()->getLocale()!='en'?'/'.app()->getLocale():''}}">{{trans('front.login')}}</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="c_lang">{{trans('front.change_language')}}</span>
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a onclick="change_lang('{{app()->getLocale()}}','en')">English</a></li>
                        <li><a onclick="change_lang('{{app()->getLocale()}}','fr')">{{ trans('front.French')}}</a></li>
                    </ul>
                </li>
            </ul>
            @else
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/wizard/?lang={{app()->getLocale()!='en'?app()->getLocale():''}}">{{trans('front.submit_project')}}</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <?php
                                if($student->photo)
                                    $pic_user = '/profile_photos/'.$student->photo;
                                else
                                    $pic_user = '/front/img/defaultuser.jpg';
                            ?>
                            <img src="{{$pic_user}}" alt="" width="24px" class="img-circle">{{ $student->first_name }}<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/profile{{app()->getLocale()!='en'?'/'.app()->getLocale():''}}"><i class="fa fa-user"></i> {{trans('front.profile')}}</a></li>
                            <li><a href="/onboard{{app()->getLocale()!='en'?'/'.app()->getLocale():''}}"><i class="fa fa-cloud-upload"></i> {{trans('front.my_projects')}}</a></li>
                            <li class="divider"></li>
                            <li><a href="/logout"><i class="fa fa-sign-out"></i> {{trans('front.logout')}}</a></li>
                        </ul>
                    </li>
                </ul>
            @endif
        </div><!--/.nav-collapse -->
    </div>
</nav>
