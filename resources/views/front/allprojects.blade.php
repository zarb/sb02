@include('front.header')
<div class="container" ng-controller="allfrontController" <?php if(isset($_GET['search'])) echo 'ng-init="search(\''.$_GET['search'].'\')"'; else echo 'ng-init="all()"'; ?>>

    <div class="row">
        <div class="col-sm-9 col-md-9">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-6" ng-repeat="project in projects">
                    <div class="thumbnail" style="height: 416px">
                        <a href="/project/@{{ project.id }}"><img class="img-responsive" style="height: 186px" ng-src="<?= asset('projects_photos');?>/@{{ project.photo }}" alt="..."></a>
                        <div class="caption">
                            <a href="/project/@{{ project.id }}"><h3>@{{ project.title }}</h3></a>
                            <h5><em>by</em> @{{ project.student.first_name }}</h5>
                            <p ng-bind-html="project.small_story + '...'"></p>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: @{{ project.percent }}%;">
                                    <span class="sr-only">@{{ project.percent }}% Complete</span>
                                </div>
                            </div>
                            <div class="stats">
                                <div class="row">
                                    <div class="col-md-3 col-sm-3 col-xs-3 text-center"><strong>@{{ project.percent }}%</strong><br> funded</div>
                                    <div class="col-md-4 col-sm-4 col-xs-4 text-center"><strong><span ng-bind="getSymbol(project.currency)"></span> @{{ project.target|number:0 }}</strong><br> goal</div>
                                    <div class="col-md-5 col-sm-5 col-xs-5 text-center"><strong ng-bind="diffDays('<?php echo date("Y-m-d");?>',project.end_date)"></strong><br> days to go</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-3 filter">
            <ul class="nav nav-pills nav-stacked">
                <li class="active cat" id="li_N"><a href="#" ng-click="filterByCategory('N')">All categories</a></li>
                <li class="cat" ng-repeat="cat in categories" id="li_@{{ cat.id }}"><a ng-click="filterByCategory(cat.id)" style="cursor: pointer">@{{ cat.name }}</a></li>
            </ul>
        </div>


    </div>


</div>
@include('front.footer')