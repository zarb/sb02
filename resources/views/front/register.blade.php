@include('front.header')
<div class="container signup-login">
    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-body">
                <?php
                if(isset($msg)){
                    echo '<p class="bg-warning">'.trans('front.password_not_match').'</p>';
                }
                ?>
                <form method="POST" action="" role="form">
                    <!--
                    <div class="form-group">
                        <p class="text-center"><small>Signup with Facebook</small></p>
                        <button id="signupSubmit" type="submit" class="btn btn-facebook btn-block btn-lg">Signup with Facebook</button>
                    </div>
                    <p class="text-center">OR</p>
                    -->
                    <h3 class="text-center">{{ trans('front.sign_up') }}</h3>
                    <div class="form-group">
                        <input id="signupEmail" name="signupEmail" type="email" value="{{isset($fields['signupEmail'])?$fields['signupEmail']:''}}" required="required" maxlength="50" placeholder="Email" class="form-control">
                    </div>
                    <div class="form-group">
                        <input id="signupPassword" name="signupPassword" type="password" required="required" maxlength="25" class="form-control" placeholder="{{trans('front.password_placeholder')}}" title="at least 6 characters" pattern=".{6,}" required>
                    </div>
                    <div class="form-group">
                        <input id="signupPasswordagain" name="signupPasswordagain" type="password" required="required" maxlength="25" placeholder="{{trans('front.repeatpass_placeholder')}}" class="form-control" pattern=".{6,}" title="at least 6 characters" required>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" required="required"> <a href="/terms-and-conditions" target="_blank">{{ trans('front.terms') }}</a>
                        </label>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button id="signupSubmit" type="submit" class="btn btn-info btn-block btn-lg">{{ trans('front.register') }}</button>
                    </div>
                    <p></p>{{ trans('front.have_account') }} <a href="/login{{app()->getLocale()!='en'&&app()->getLocale()?'/'.app()->getLocale():''}}">{{ trans('front.sign_in') }}</a></p>
                </form>
            </div>
        </div>
    </div>
</div>
@include('front.footer')