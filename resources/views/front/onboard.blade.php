@include('front.header')
@if(isset($_GET['new']))
<!-- Google Code for Inscription Conversion Page -->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 945734799;
    var google_conversion_language = "en";
    var google_conversion_format = "3";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "SOemCJDvuF8Qj4n7wgM";
    var google_remarketing_only = false;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/945734799/?label=SOemCJDvuF8Qj4n7wgM&amp;guid=ON&amp;script=0"/>
    </div>
</noscript>
@endif
<div class="container onboard" ng-controller="onboardController">
    <div class="row" ng-if="projects.length > 0">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="panel panel-primary">
                <h3 class="text-center">{{ trans('front.your_projects') }}</h3>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-6" ng-repeat="project in projects">
                            <div class="thumbnail">
                                <div class="col-sm-12 thumbnail2 text-center">
                                    <a href="/wizard/@{{ project.id }}">
                                        <img class="img-responsive" style="height: 176px" ng-src="<?= asset('projects_photos');?>/@{{ project.photo }}" alt="...">
                                    </a>
                                    <div class="tags tags-left">
                                        <span class="label-tags"><span ng-class="classTag(project.status)" ng-bind="labelTag(project.status,'{{app()->getLocale()}}')"></span></span>
                                    </div>
                                </div>
                                <div class="caption">
                                    <a href="/wizard/@{{ project.id }}"><h3 style="line-height: normal !important;">@{{ project.title }}</h3></a>
                                    <h5><em>{{trans('front.by')}}</em> @{{ project.student.first_name }}</h5>
                                    <p>@{{ project.small_story }}...</p>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="@{{ project.percent }}" aria-valuemin="0" aria-valuemax="100" style="width: @{{ project.percent }}%;">
                                            <span class="sr-only">@{{ project.percent }}% Complete</span>
                                        </div>
                                    </div>
                                    <div class="stats">
                                        <div class="row">
                                            <div class="col-md-4 col-sm-4 col-xs-3 text-center"><strong>@{{ project.percent }}%</strong><br> {{trans('front.funded')}}</div>
                                            <div class="col-md-4 col-sm-4 col-xs-4 text-center"><strong><span ng-bind="getSymbol(project.currency)"></span>@{{ project.target }}</strong><br> {{trans('front.goal')}}</div>
                                            <div class="col-md-4 col-sm-4 col-xs-5 text-center" ng-if="diffDays('<?php echo date("Y-m-d");?>',project.end_date) > 0"><strong ng-bind="diffDays('<?php echo date("Y-m-d");?>',project.end_date)"></strong><br>{{trans('front.days_to_go')}}</div>
                                        </div>
                                        <div class="row" style="margin: 20px 0 -20px 0" ng-if="project.status == 'D'">
                                            <p class="text-center">
                                                <a class="btn btn-primary btn-sm" href="/wizard/@{{ project.id }}">{{trans('front.edit_project')}}</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(!$student->mp_type)
                        <hr>
                        <div class="row">
                            <p class="text-center">
                                <a href="/profile">Complete Your Profile</a>
                            </p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row" ng-show="projects.length == 0">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="text-center"><i class="fa fa-cloud-upload fa-5x"></i></div>
                    <p class="text-center">{{trans('front.you_dont_have')}}</p>
                    <br>
                    <br>
                    <div class="text-center"><a href="/wizard" class="btn btn-primary btn-lg">{{trans('front.create_a_project')}}</a></div>
                    @if(!$student->mp_type)
                        <hr>
                        <div class="row" style="margin-top: 30px;margin-bottom: -30px !important;font-size:12px">
                            <p class="text-center">
                                <a href="/profile">{{trans('front.complete_your_profile')}}</a>
                            </p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>


</div>
<style>
    .thumbnail2 .tags-left {
        left: 0;
        top: 0;
        direction: ltr;
    }

    .thumbnail2 .tags {
        position: absolute;
        top: 0;
        right: 0;
        display: table;
        overflow: visible;
        width: auto;
        height: auto;
        margin: 0;
        padding: 6px;
        vertical-align: inherit;
        border-width: 0;
        background-color: transparent;
        direction: rtl;
    }
    .caption2 {
        width:100%;
        bottom: 0;
        position: absolute;
        background:#000;
        background: -webkit-linear-gradient(bottom, #000 40%, rgba(0, 0, 0, 0) 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
        background: -moz-linear-gradient(bottom, #000 40%, rgba(0, 0, 0, 0) 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
        background: -o-linear-gradient(bottom, #000 40%, rgba(0, 0, 0, 0) 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
        background: linear-gradient(to top, #000 40%, rgba(0, 0, 0, 0) 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
    }

    .thumbnail2 {
        border: 0 none;
        box-shadow: none;
        margin:0;
        padding:0;
    }

    .caption2 h4 {
        font-size: .8em;
        color: #fff;
        -webkit-font-smoothing: antialiased;
    }
</style>
@include('front.footer')