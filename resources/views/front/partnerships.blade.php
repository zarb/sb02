@include('front.header')
<section class="container">
    <h3>{{trans('front.partnerships')}}</h3>
    <div class="row" style="height: 150px">
        <div class="col-md-3 col-sm-4 col-xs-6">
            <div class="thumbnail_container">
                <div class="thumbnail">
                    <a target="_blank" href="http://www.zdnet.com/article/minas-gerais-picks-40-startups-for-acceleration-program/">
                        <img src="http://studentbackr.com/wp-content/uploads/2015/05/seed.png"  class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6">
            <div class="thumbnail_container">
                <div class="thumbnail">
                    <a target="_blank" href="http://planetexpat.org/">
                        <img src="http://studentbackr.com/wp-content/uploads/2015/05/PlanetExpat_Logo-Site-bleu-Width-87.png" class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6">
            <div class="thumbnail_container">
                <div class="thumbnail">
                    <a target="_blank" href="http://www.eae.es/">
                        <img src="http://studentbackr.com/wp-content/uploads/2016/05/eae_home_logo-1.png" class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6">
            <div class="thumbnail_container">
                <div class="thumbnail">
                    <a target="_blank" href="http://www.escpeurope.eu/campus/escp-europe-campus-madrid/escp-europe-madrid-campus/">
                        <img src="http://studentbackr.com/wp-content/uploads/2016/05/ESCP-EUROPE-Business_School_seul_quadri-copia.png" class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div>&nbsp;</div>
    <div class="row" style="height: 150px">
        <div class="col-md-3 col-sm-4 col-xs-6">
            <div class="thumbnail_container">
                <div class="thumbnail">
                    <a target="_blank" href="http://www.chaireeee.eu/en/paris/formation/">
                        <img src="http://studentbackr.com/wp-content/uploads/2016/05/Logo_ChaireEEE_2013-monochrome-05.png"  class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6">
            <div class="thumbnail_container">
                <div class="thumbnail">
                    <a target="_blank" href="http://optione.chaireeee.eu/madrid/">
                        <img src="http://studentbackr.com/wp-content/uploads/2016/05/NEW_Logo_OptionE-09.png" class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6">
            <div class="thumbnail_container">
                <div class="thumbnail">
                    <a target="_blank" href="http://www.mes-etudes.com/">
                        <img src="http://studentbackr.com/wp-content/uploads/2016/06/logo-mesetudes.png" class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6">
            <div class="thumbnail_container">
                <div class="thumbnail">
                    <a target="_blank" href="http://www.ensemble2generations.fr/">
                        <img src="http://studentbackr.com/wp-content/uploads/2015/05/logo_rond2.jpg" class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div>&nbsp;</div>
    <div class="row" style="height: 150px">
        <div class="col-md-3 col-sm-4 col-xs-6">
            <div class="thumbnail_container">
                <div class="thumbnail">
                    <a target="_blank" href="http://www.lescoursduparnasse.com/">
                        <img src="http://studentbackr.com/wp-content/uploads/2016/06/les-cours-du-parnasse.png"  class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6">
            <div class="thumbnail_container">
                <div class="thumbnail">
                    <a target="_blank" href="http://www.lecoursdassas.com/">
                        <img src="http://studentbackr.com/wp-content/uploads/2016/06/le-cours-dassas.png" class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6">
            <div class="thumbnail_container">
                <div class="thumbnail">
                    <a target="_blank" href="https://www.mangopay.com/">
                        <img src="http://studentbackr.com/wp-content/uploads/2015/05/mangopay.png" class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6">
            <div class="thumbnail_container">
                <div class="thumbnail">
                    <a target="_blank" href="http://www.iwill.org.uk/">
                        <img src="http://studentbackr.com/wp-content/uploads/2016/11/logo.png" class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <p>&nbsp;</p>
</section>
<style>
    .thumbnail_container {
        position: relative;
        width: 100%;
        padding-bottom: 100%;
        margin-bottom:0px;
    }

    .thumbnail {
        position:absolute;
        width:100%;
        height:100%;
        min-height: 200px !important;
    }
    .thumbnail img {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        max-width: 130px !important;
    }
</style>
@include('front.footer')