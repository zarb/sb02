@include('front.header')
<section ng-controller="homefrontController">
    <div class="featured-image home">
        <img class="img-responsive" src="<?= asset('front/img/hero.jpeg');?>">
        <div class="gradient-overlay"></div>
    </div> <!-- /container -->

    <div class="container">
        <div class="row featured-row">
            <div class="col-sm-9">
                <div class="row">
                    <div class="col-sm-12"><h4>{{trans('front.featured_project')}}</h4></div>
                </div>
                <div class="row featured-project">
                    <div class="col-sm-4">
                        <a href="/project/@{{ featured.id }}"><img class="img-responsive" ng-src="<?= asset('projects_photos');?>/@{{ featured.photo }}" alt="..."></a>
                    </div>
                    <div class="col-sm-8">
                        <div class="caption">
                            <a href="/project/@{{ featured.id }}"  ng-cloak><h3 ng-cloak>@{{ featured.title }}</h3></a>
                            <h5 ng-cloak><em>{{trans('front.by')}}</em> @{{ featured.student }}</h5>
                            <p ng-cloak ng-bind-html="featured.small_story + '...'"></p>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: @{{ featured.percent }}%;">
                                    <span class="sr-only">@{{ featured.percent }}% Complete</span>
                                </div>
                            </div>
                            <div class="stats">
                                <div class="row">
                                    <div class="col-md-3 col-sm-3 col-xs-3" ng-cloak><strong>@{{ featured.percent }}%</strong><br> {{trans('front.funded')}}</div>
                                    <div class="col-md-4 col-sm-4 col-xs-4" ng-cloak><strong>@{{ featured.target|number:0 }}</strong><br> {{trans('front.goal')}}</div>
                                    <div class="col-md-5 col-sm-5 col-xs-5 text-center" ng-cloak><strong ng-bind="diffDays('<?php echo date("Y-m-d");?>',featured.end_date)"></strong><br> {{trans('front.days_to_go')}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <a href="/all-projects" class="btn btn-lg btn-primary pull-right">{{trans('front.view_all_projects')}}</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 filter filter-home">
                <ul class="nav nav-pills nav-stacked">
                    <li class="cat" ng-class="$index==0?'active':''" ng-repeat="cat in categories" id="li_@{{ cat.id }}" ng-cloak><a href="#" ng-click="setFeatured(cat.id)" ng-cloak>@{{ cat.name }}</a></li>
                </ul>
            </div>
        </div>

        <!-- Most urgent start -->
        <div class="row">
            <h4>{{trans('front.most_urgent')}}</h4>
            <div class="col-md-3 col-sm-6 col-xs-6" ng-repeat="project in urgents">
                <div class="thumbnail" style="height: 416px">
                    <a href="/project/@{{ project.id }}"><img class="img-responsive" style="height: 186px" ng-src="<?= asset('projects_photos');?>/@{{ project.photo }}" alt="..."></a>
                    <div class="caption">
                        <a href="/project/@{{ project.id }}"><h3>@{{ project.title }}</h3></a>
                        <h5><em>by</em> @{{ project.student.first_name }}</h5>
                        <p ng-bind-html="project.small_story + '...'"></p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: @{{ project.percent }}%;">
                                <span class="sr-only">@{{ project.percent }}% Complete</span>
                            </div>
                        </div>
                        <div class="stats">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-center"><strong>@{{ project.percent }}%</strong><br> funded</div>
                                <div class="col-md-4 col-sm-4 col-xs-4 text-center"><strong>@{{ project.target|number:0 }}</strong><br> goal</div>
                                <div class="col-md-5 col-sm-5 col-xs-5 text-center"><strong ng-bind="diffDays('<?php echo date("Y-m-d");?>',project.end_date)"></strong><br> days to go</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Most urgent end -->

        <!-- Most recent start -->
        <div class="row">
            <h4>{{trans('front.most_recent')}}</h4>
            <div class="col-md-3 col-sm-6 col-xs-6" ng-repeat="project in recents">
                <div class="thumbnail" style="height: 416px">
                    <a href="/project/@{{ project.id }}"><img class="img-responsive" style="height: 186px" ng-src="<?= asset('projects_photos');?>/@{{ project.photo }}" alt="..."></a>
                    <div class="caption">
                        <a href="/project/@{{ project.id }}"><h3>@{{ project.title }}</h3></a>
                        <h5><em>by</em> @{{ project.student.first_name }}</h5>
                        <p ng-bind-html="project.small_story + '...'"></p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: @{{ project.percent }}%;">
                                <span class="sr-only">@{{ project.percent }}% Complete</span>
                            </div>
                        </div>
                        <div class="stats">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-center"><strong>@{{ project.percent }}%</strong><br> funded</div>
                                <div class="col-md-4 col-sm-4 col-xs-4 text-center"><strong>@{{ project.target|number:0 }}</strong><br> goal</div>
                                <div class="col-md-5 col-sm-5 col-xs-5 text-center"><strong ng-bind="diffDays('<?php echo date("Y-m-d");?>',project.end_date)"></strong><br> days to go</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Most recent end -->

        <!-- Most popular start -->
        <div class="row">
            <h4>{{trans('front.most_popular')}}</h4>
            <div class="col-md-3 col-sm-6 col-xs-6" ng-repeat="project in popular">
                <div class="thumbnail" style="height: 416px">
                    <a href="/project/@{{ project.id }}"><img class="img-responsive" style="height: 186px" ng-src="<?= asset('projects_photos');?>/@{{ project.photo }}" alt="..."></a>
                    <div class="caption">
                        <a href="/project/@{{ project.id }}"><h3>@{{ project.title }}</h3></a>
                        <h5><em>by</em> @{{ project.student.first_name }}</h5>
                        <p ng-bind-html="project.small_story + '...'"></p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: @{{ project.percent }}%;">
                                <span class="sr-only">@{{ project.percent }}% Complete</span>
                            </div>
                        </div>
                        <div class="stats">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-center"><strong>@{{ project.percent }}%</strong><br> funded</div>
                                <div class="col-md-4 col-sm-4 col-xs-4 text-center"><strong>@{{ project.target|number:0 }}</strong><br> goal</div>
                                <div class="col-md-5 col-sm-5 col-xs-5 text-center"><strong ng-bind="diffDays('<?php echo date("Y-m-d");?>',project.end_date)"></strong><br> days to go</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Most popular end -->

        <!-- Mini projects start -->
        <div class="row">
            <h4>{{trans('front.mini_projects')}}</h4>
            <div class="col-md-3 col-sm-6 col-xs-6" ng-repeat="project in mini">
                <div class="thumbnail" style="height: 416px">
                    <a href="/project/@{{ project.id }}"><img class="img-responsive" style="height: 186px" ng-src="<?= asset('projects_photos');?>/@{{ project.photo }}" alt="..."></a>
                    <div class="caption">
                        <a href="/project/@{{ project.id }}"><h3>@{{ project.title }}</h3></a>
                        <h5><em>by</em> @{{ project.student.first_name }}</h5>
                        <p ng-bind-html="project.small_story + '...'"></p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: @{{ project.percent }}%;">
                                <span class="sr-only">@{{ project.percent }}% Complete</span>
                            </div>
                        </div>
                        <div class="stats">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-center"><strong>@{{ project.percent }}%</strong><br> funded</div>
                                <div class="col-md-4 col-sm-4 col-xs-4 text-center"><strong>@{{ project.target|number:0 }}</strong><br> goal</div>
                                <div class="col-md-5 col-sm-5 col-xs-5 text-center"><strong ng-bind="diffDays('<?php echo date("Y-m-d");?>',project.end_date)"></strong><br> days to go</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Most popular end -->
    </div>
</section>
@include('front.footer')