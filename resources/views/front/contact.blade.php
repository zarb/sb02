@include('front.header')
<div class="container signup-login">
    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-body">
                <?php
                if(isset($msg)){
                    echo '<p class="alert alert-success">'.$msg.'</p>';
                }
                ?>
                <form method="POST" action="#" role="form">
                    <!--
                    <div class="form-group">
                        <p class="text-center"><small>Signup with Facebook</small></p>
                        <button id="signupSubmit" type="submit" class="btn btn-facebook btn-block btn-lg">Signup with Facebook</button>
                    </div>
                    <p class="text-center">OR</p>
                    -->
                    <h3 class="text-center">Contact</h3>
                    <div class="form-group">
                        <input name="first_name" type="text" required="required" placeholder="{{trans('front.first_name')}}" class="form-control">
                    </div>
                    <div class="form-group">
                        <input name="last_name" type="text" required="required" placeholder="{{trans('front.last_name')}}" class="form-control">
                    </div>
                    <div class="form-group">
                        <input name="email" type="email" required="required" placeholder="{{trans('front.e-mail')}}" class="form-control">
                    </div>
                    <div class="form-group">
                        <input name="topic" type="text" required="required" placeholder="{{trans('front.topic')}}" class="form-control">
                    </div>
                    <div class="form-group">
                        <textarea name="message" required="required" placeholder="{{trans('front.your_message')}}" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label>{{trans('front.i_wish_to_be_called')}}</label>
                        <select class="form-control" name="called">
                            <option value=""></option>
                            <option value="Y">Yes</option>
                            <option value="N">No</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{trans('front.i_want_to_webinar')}}</label>
                        <select class="form-control" name="webinar">
                            <option value=""></option>
                            <option value="Y">Yes</option>
                            <option value="N">No</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input name="phone" type="text" placeholder="{{trans('front.phone_number')}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button id="contactSubmit" type="submit" class="btn btn-info btn-block btn-lg">{{trans('front.submit')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('front.footer')