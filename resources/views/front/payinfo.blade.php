@include('front.header')
<?php
$amount = number_format($_POST['amount'],2);
?>
<section class="container-fluid" style="width: 70%; margin: 70px auto; border: 1px solid #ccc; padding: 20px">
    <img src="http://studentbackr.com/wp-content/uploads/2015/04/logo-STUDENTBACKR.png" alt="Student Backr">
    <hr>
    <div style="text-align: center">
        <h3>FINALIZE PAYMENT</h3>
        <img style="margin: 0 auto" src="/front/img/mangopay_all_logos.png" alt="Mangopay">
    </div>
    <div class="row">
        <form action="<?php print $createdCardRegister->CardRegistrationURL; ?>" method="post">
            <div class="row" style="width: 95%; margin-left: 14px">
                <div class="col-md-5" style="margin: 0; padding: 0"><!-- left column -->
                    <table class="table table-bordered table-striped" width="95%">
                        <tr style="height: 51px">
                            <td><b>First Name</b></td>
                            <td><?php echo $_POST['name'];?></td>
                        </tr>
                        <tr style="height: 51px">
                            <td><b>Last Name</b></td>
                            <td><?php echo $_POST['lastname'];?></td>
                        </tr>
                        <tr style="height: 51px">
                            <td><b>Project</b></td>
                            <td>{{$project->title}}</td>
                        </tr>
                        <tr style="height: 51px">
                            <td><b>Reward</b></td>
                            <td>{{$reward->title}}</td>
                        </tr>

                    </table>
                </div><!-- end left column -->
                <div class="col-md-7" style="margin: 0; padding: 0"><!-- right column -->
                    <table class="table table-bordered table-striped" width="95%">
                        <tr style="height: 50px">
                            <td><b>Card Number</b></td>
                            <td width="60%">
                                <input type="text" maxlength="19" id="preCard" class="form-control" data-mask="9999.9999.9999.9999" placeholder="Credit Card Number">
                                <input type="hidden" name="cardNumber" id="cardNumber">
                            </td>
                        </tr>
                        <tr style="height: 50px">
                            <td><b>Expiration Date</b></td>
                            <td>
                                <input type="text" id="preExpiration" class="form-control" data-mask="99/99" placeholder="Expires MM / YY">
                                <input type="hidden" name="cardExpirationDate" id="cardExpirationDate">
                            </td>
                        </tr>
                        <tr style="height: 50px">
                            <td><b>Card Security</b> <a href="#" data-toggle="modal" data-target=".modal-cvv"><img height="20" src="/front/img/interrogation-icon.png" alt="CVV"></a></td>
                            <td><input type="text" name="cardCvx" class="form-control" data-mask="999" placeholder="CVV"></td>
                        </tr>
                        <tr style="height: 50px">
                            <td><b>Amount</b></td>
                            <td><input type="text" class="form-control" readonly  value="<?php echo $amount; ?>"></td>
                        </tr>
                    </table>
                </div><!-- end right column -->
            </div>
            <div class="row" style="text-align: center">
                <input type="hidden" name="data" value="<?php print $createdCardRegister->PreregistrationData; ?>" />
                <input type="hidden" name="accessKeyRef" value="<?php print $createdCardRegister->AccessKey; ?>" />
                <input type="hidden" name="returnURL" value="<?php print $returnUrl; ?>" />

                <button type="submit" class="btn btn-lg btn-success">VALIDATE</button>
                <a href="#" onclick="document.location=history.back()" class="btn btn-lg btn-danger">BACK</a>
                <p style="margin-top: 20px">You are on a payment server secured by SSL powered by Mangopay</p>
                <img src="http://studentbackr.com/wp-content/uploads/2015/05/www-mangopay.png" style="width: 160px">
            </div>
        </form>
    </div>
</section>

<div class="modal fade modal-cvv" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" style="width: 400px">
        <div class="modal-content" style="padding:20px">
            <img src="/front/img/cvv.gif">
        </div>
    </div>
</div>
@include('front.footer')
<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
<script>
    $(document).ready(function(){

        $('#preCard').blur(function(){
            newcard = $(this).val().replace(/\./g, "");
            $('#cardNumber').val(newcard);
        })

        $('#preExpiration').blur(function(){
            expiration = $(this).val();
            newexpiration = expiration.replace("/", "");
            $('#cardExpirationDate').val(newexpiration);
        })
    })
</script>