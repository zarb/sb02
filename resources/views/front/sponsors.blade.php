@include('front.header')
<section class="container">
    <h2 class="text-center">{{trans('front.sponsors1')}}</h2>
    <h3 class="text-center">{{trans('front.sponsors2')}}</h3>
    <p>{{trans('front.sponsors3')}}</p>
    <p>&nbsp;</p>
    <div class="row">
        <div class="col-md-3">
            <div class="flip">
                <div class="front">
                    <div class="thumbnail">
                        <p class="text-center"><a href="http://deliveroo.evyy.net/c/344788/309065/3968">Deliveroo</a></p>
                        <img src="http://studentbackr.com/wp-content/uploads/2016/11/Deliveroo_LinkedIn_Logo-450x450-1-300x300.jpg" class="img-responsive">
                    </div>
                </div>
                <div class="back">
                    <div class="divcontent"><a href="http://deliveroo.evyy.net/c/344788/309065/3968" target="_blank">{{trans('front.deliveroo_tip')}}</a></div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="flip">
                <div class="front">
                    <div class="thumbnail">
                        <p class="text-center"><a href="https://misabogados.com/">MisAbogados.com</a></p>
                        <img src="http://studentbackr.com/wp-content/uploads/2016/11/logo_misabogados.png" class="img-responsive">
                    </div>
                </div>
                <div class="back">
                    <div class="divcontent"><a href="https://misabogados.com/" target="_blank">{{trans('front.misabogados_tip')}}</a></div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="flip">
                <div class="front">
                    <div class="thumbnail">
                        <p class="text-center"><a href="http://www.dadaroom.com/">Dada Room</a></p>
                        <img src="http://studentbackr.com/wp-content/uploads/2016/11/logo2.png" class="img-responsive">
                    </div>
                </div>
                <div class="back">
                    <div class="divcontent"><a href="http://www.dadaroom.com/" target="_blank">{{trans('front.dadaroom_tip')}}</a></div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="flip">
                <div class="front">
                    <div class="thumbnail">
                        <p class="text-center"><a href="http://planetexpat.org/">Planet Expat</a></p>
                        <img src="http://studentbackr.com/wp-content/uploads/2016/11/Logo-Site-bleu-Width-56.png" class="img-responsive">
                    </div>
                </div>
                <div class="back">
                    <div class="divcontent"><a href="http://planetexpat.org/" target="_blank">{{trans('front.planet_tip')}}</a></div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-3">
            <div class="flip">
                <div class="front">
                    <div class="thumbnail">
                        <p class="text-center"><a href="http://www.jechange.fr/">JeChange</a></p>
                        <img src="/front/img/logo-jechange.png" class="img-responsive">
                    </div>
                </div>
                <div class="back">
                    <div class="divcontent"><a href="http://www.jechange.fr/" target="_blank">{{trans('front.jechange_tip')}}</a></div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="flip">
                <div class="front">
                    <div class="thumbnail">
                        <p class="text-center"><a href="{{ trans('front.url_mymooc') }}">MyMooc</a></p>
                        <img src="/front/img/Logo My Mooc.png" class="img-responsive">
                    </div>
                </div>
                <div class="back">
                    <div class="divcontent"><a href="{{ trans('front.url_mymooc') }}" target="_blank">{{trans('front.mymooc_tip')}}</a></div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="flip">
                <div class="front">
                    <div class="thumbnail">
                        <p class="text-center"><a href="http://www.tenzingconseil.fr">Tenzing</a></p>
                        <img src="/front/img/TENZING-RVB.jpg" class="img-responsive">
                    </div>
                </div>
                <div class="back">
                    <div class="divcontent"><a href="http://www.tenzingconseil.fr" target="_blank">{{trans('front.tenzing_tip')}}</a></div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="flip">
                <div class="front">
                    <div class="thumbnail">
                        <p class="text-center"><a href="{{ trans('front.qobuz_url') }}">Qobuz</a></p>
                        <img src="/front/img/new_logo_qobuz.png" class="img-responsive">
                    </div>
                </div>
                <div class="back">
                    <div class="divcontent"><a href="{{ trans('front.qobuz_url') }}" target="_blank">{{trans('front.qobuz_tip')}}</a></div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-3">
            <div class="flip">
                <div class="front">
                    <div class="thumbnail">
                        <p class="text-center"><a href="{{ trans('front.languageCourse_url') }}">LanguageCourse.Net </a></p>
                        <img src="/front/img/LOGO-LangCourse.png" class="img-responsive">
                    </div>
                </div>
                <div class="back">
                    <div class="divcontent"><a href="{{ trans('front.languageCourse_url') }}" target="_blank">{{trans('front.languageCourse_tip')}}</a></div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="flip">
                <div class="front">
                    <div class="thumbnail">
                        <p class="text-center"><a href="http://www.massage-chouette.com/">Massage Chouette</a></p>
                        <img src="/front/img/logo_massage.png" class="img-responsive">
                    </div>
                </div>
                <div class="back">
                    <div class="divcontent"><a href="http://www.massage-chouette.com/" target="_blank">{{trans('front.massage_tip')}}</a></div>
                </div>
            </div>
        </div>
    </div>
    <p>&nbsp;</p>
    <p>{{trans('front.sponsors4')}} <a href="mailto:contact@studentbackr.com">contact@studentbackr.com</a>!</p>
    <p>&nbsp;</p>
</section>

<style>
    .thumbnail_container {
        position: relative;
        width: 100%;
        padding-bottom: 100%;
        margin-bottom:0px;
    }

    .thumbnail {
        position:absolute;
        width:100%;
        height:100%;
        min-height: 200px !important;
    }
    .thumbnail img {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        max-width: 130px !important;
    }
    .thumbnail p{
        padding: 4px;
    }
    .divcontent{
        padding: 6px;
        font-size: 12px;
        border-radius: 4px;
        height: 220px;
        background: rgba(29, 219, 109, 0.9);
        color: #fff;
    }

    .divcontent a{
        font-weight: bold;
        color: #fff;
        text-decoration: none;
    }
</style>
@include('front.footer')
<script>
//    $(document).on('mouseover','.div-sponsor',function(){
//        this_content = $(this).attr('data-content');
//        this_url = $(this).attr('data-url');
//        original_content = $(this).parent().html();
//        $(this).parent().addClass('divhover');
//
//        $(this).parent().html('<div class="divcontent"><a href="'+this_url+'" target="_blank">'+this_content+'</a></div>');
//    })
//
//    $(document).on("mouseout",".divcontent", function(){
//        $(this).parent().removeClass('divhover');
//        $(this).parent().html(original_content);
//    })

    $(function(){
        $(".flip").flip({
            trigger: 'hover'
        });
    });
</script>