@include('front.header')
<section class="container-fluid" style="width: 70%; margin: 70px auto; border: 1px solid #ccc; padding: 20px">
    <img src="http://studentbackr.com/wp-content/uploads/2015/04/logo-STUDENTBACKR.png" alt="Student Backr">
    <hr>
    <div style="text-align: center">
        <h3>FINALIZE PAYMENT</h3>
        <img style="margin: 0 auto" src="/front/img/mangopay_all_logos.png" alt="Mangopay">
    </div>
    <div class="row">
        <p style="text-align: center;">
            <br><br>
            <?php
            echo $msg;
            ?>
            <br><br><a href="/" class="btn btn-lg btn-success">BACK</a>
        </p>
    </div>
</section>

<div class="modal fade modal-cvv" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" style="width: 400px">
        <div class="modal-content" style="padding:20px">
            <img src="/front/img/cvv.gif">
        </div>
    </div>
</div>
@include('front.footer')