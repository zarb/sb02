@include('front.header')
<div class="container page" role="main" style="background: #f7fafa; width: 100%; padding-left: 20px">
    <div class="row">
        <div class="span12">
            <div class="container" style="padding-right: 20px; background: #fff; margin: 30px auto;border-width: 1px; border-color: #d9d9de; border-style: solid; border-radius: 6px;">
                <div class="container">
                    <h1><?php echo trans('front.Project_budgeting_calculator'); ?></h1>
                    <p><?php echo trans('front.choose_the_rewards'); ?></p>
                    <div class="well">
                        <table class="one">
                            <tbody>
                            <tr>
                                <td><?php echo trans('front.Project_Cost');?></td>
                                <td><div class="input-prepend"><span class="add-on">$</span><input type="text" class="input-mini" id="project-cost" width="50" value="0"></div></td>
                                <td style="padding-left: 25px"><?php echo trans('front.The_amount_of_money'); ?></td>
                            </tr>
                            <tr>
                                <td><?php echo trans('front.Total_Rewards_Cost'); ?></td>
                                <td><div class="input-prepend"><span class="add-on">$</span><input readonly="readonly" type="text" class="input-mini" id="total-incentive-cost" width="50" value=""></div></td>
                                <td style="padding-left: 25px"><?php echo trans('front.Automatically_generated');?></td>
                            </tr>
                            <tr>
                                <td><?php echo trans('front.StudentBackr_fee');?></td>
                                <td><div class="input-prepend"><span class="add-on">$</span><input readonly="readonly" type="text" class="input-mini" id="sb-need" width="50"></div></td>
                                <td style="padding-left: 25px"><?php echo trans('front.Calculated_at_7');?></td>
                            </tr>
                            <tr>
                                <td><?php echo trans('front.Total_to_raise');?></td>
                                <td><div class="input-prepend"><span class="add-on">$</span><input readonly="readonly" type="text" class="input-mini" id="total-to-raise" width="50" value=""></div></td>
                                <td style="padding-left: 25px"><?php echo trans('front.Total_including_project');?></td>
                            </tr>
                            <tr>
                                <td><?php echo trans('front.Predicted_to_raise');?></td>
                                <td><div class="input-prepend"><span class="add-on">$</span><input readonly="readonly" type="text" class="input-mini" id="total-raise-prediction" width="50" value=""></div></td>
                                <td style="padding-left: 25px"><?php echo trans('front.Amount_predicted'); ?></td>
                            </tr>
                            <tr>
                                <td><?php echo trans('front.Difference');?></td>
                                <td><div class="input-prepend"><span class="add-on">$</span><input readonly="readonly" type="text" class="input-mini" id="difference" width="50" value=""></div></td>
                                <td style="padding-left: 25px"></td>
                            </tr>
                            <tr>
                                <td><?php echo trans('front.Total_Backers');?></td>
                                <td><div class="input-prepend"><input class="backer-shift input-mini" readonly="readonly" type="text" id="backer-prediction-answer" width="100"></div></td>
                                <td style="padding-left: 25px"><?php echo trans('front.How_many_total_backers'); ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <style>
                    .two .input-mini{width: 120px !important;}
                </style>

                <div class="container">
                    <h1><?php echo trans('front.rewards'); ?></h1>
                    <p></p>
                    <div class="well">
                        <div class="incentives">
                            <table class="two"> <tbody><tr><td><a href="#" id="incent4" rel="tooltip" data-original-title="A name for what you're giving away"><?php echo trans('front.Reward_description');?></a></td><td><a href="#" id="head2" rel="tooltip" data-original-title="What donation will you require for backers to receive this incentive?"><?php echo trans('front.Price_to_backer');?></a></td><td><a href="#" id="incent5" rel="tooltip" data-original-title="How much will it cost you to give this to your backers?"><?php echo trans('front.Reward_postage');?></a></td><td><a href="#" id="backers" rel="tooltip" data-original-title="How many backers do you predict will donate for this incentive?"><?php echo trans('front.Estimate_Backer');?></a></td><td><?php echo trans('front.Total_Raised'); ?></td><td><?php echo trans('front.Total_cost');?></td></tr>
                                <tr>
                                    <td><div class="input-prepend incentive-title"><input type="text" class="input-mini" data-id="1" width="100"></div></td>
                                    <td><div class="input-prepend"><span class="add-on">$</span><input type="text" class="input-mini donation-required" data-id="1" width="50" value="0"></div></td>
                                    <td><div class="input-prepend"><span class="add-on">$</span><input type="text" class="input-mini reward-cost" data-id="1" width="50" value="0"></div></td>
                                    <td><div class="input-prepend"><input type="text" class="input-mini backers-quantity" data-id="1" width="50" value="0"></div></td>
                                    <td><div class="input-prepend"><span class="add-on">$</span><input readonly="readonly" type="text" class="input-mini total-raised"  data-id="1" width="100" value=""></div></td>
                                    <td><div class="input-prepend"><span class="add-on">$</span><input readonly="readonly" type="text" class="input-mini total-cost"  data-id="1" width="100" value=""></div></td>
                                </tr>
                                <tr>
                                    <td><div class="input-prepend incentive-title"><input type="text" class="input-mini" data-id="2" width="100"></div></td>
                                    <td><div class="input-prepend"><span class="add-on">$</span><input type="text" class="input-mini donation-required" data-id="2" width="50" value="0"></div></td>
                                    <td><div class="input-prepend"><span class="add-on">$</span><input type="text" class="input-mini reward-cost" data-id="2" width="50" value="0"></div></td>
                                    <td><div class="input-prepend"><input type="text" class="input-mini backers-quantity" data-id="2" width="50" value="0"></div></td>
                                    <td><div class="input-prepend"><span class="add-on">$</span><input readonly="readonly" type="text" class="input-mini total-raised"  data-id="2" width="100" value=""></div></td>
                                    <td><div class="input-prepend"><span class="add-on">$</span><input readonly="readonly" type="text" class="input-mini total-cost"  data-id="2" width="100" value=""></div></td>
                                </tr>
                                <tr>
                                    <td><div class="input-prepend incentive-title"><input type="text" class="input-mini" data-id="3" width="100"></div></td>
                                    <td><div class="input-prepend"><span class="add-on">$</span><input type="text" class="input-mini donation-required" data-id="3" width="50" value="0"></div></td>
                                    <td><div class="input-prepend"><span class="add-on">$</span><input type="text" class="input-mini reward-cost" data-id="3" width="50" value="0"></div></td>
                                    <td><div class="input-prepend"><input type="text" class="input-mini backers-quantity" data-id="3" width="50" value="0"></div></td>
                                    <td><div class="input-prepend"><span class="add-on">$</span><input readonly="readonly" type="text" class="input-mini total-raised"  data-id="3" width="100" value=""></div></td>
                                    <td><div class="input-prepend"><span class="add-on">$</span><input readonly="readonly" type="text" class="input-mini total-cost"  data-id="3" width="100" value=""></div></td>
                                </tr>

                                </tbody></table>
                        </div>

                        <form>
                            <!--input type="button" value="Apply" name="button1" onclick="apply()"-->
                            <button class="btn btn-success" type="button" value="Add Reward" id="add_line"><i class="glyphicon glyphicon-plus"> </i> <?php echo trans('front.Add_Reward'); ?></button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous"></script>
<script>
    var n_lines = 3;
    var allow_sum = false;
    jQuery(document).ready(function(){
        jQuery('.input-mini').blur(function(){
            inputs_backer_price = jQuery('.donation-required');
            total_backers = total_rewards_cost = total_raise_prediction = 0;

            var $input_project_cost = jQuery('#project-cost');

            inputs_backer_price.each(function(i){

                var $input = jQuery( this),
                        id_field = $input.attr('data-id')
                $input_total_raise = jQuery(".total-raised[data-id='"+id_field+"']"),
                        $input_total_cost = jQuery(".total-cost[data-id='"+id_field+"']");


                backer_price = jQuery(".donation-required[data-id='"+id_field+"']").val();
                reward_cost = jQuery(".reward-cost[data-id='"+id_field+"']").val();
                backers_quantity = jQuery(".backers-quantity[data-id='"+id_field+"']").val();

                //old line
                //if(backer_price != 0 && reward_cost != 0 && backers_quantity != 0) {
                if(backer_price != 0 && backers_quantity != 0) {
                    allow_sum = true;

                    total_raised = backer_price * backers_quantity;
                    total_cost = reward_cost * backers_quantity;

                    $input_total_raise.val(total_raised);
                    $input_total_cost.val(total_cost);

                    total_backers += parseFloat(backers_quantity);
                    total_rewards_cost += parseFloat(total_cost);
                    total_raise_prediction += parseFloat(total_raised);
                }
            })
            if(allow_sum == true) {
                fee = (parseFloat($input_project_cost.val()) + total_rewards_cost) * 0.07;
                jQuery('#sb-need').val(fee.toFixed(2)); // fee field
                jQuery('#backer-prediction-answer').val(total_backers); //total backers field
                jQuery('#total-incentive-cost').val(total_rewards_cost); // total rewards cost
                total_to_raise = parseFloat($input_project_cost.val()) + total_rewards_cost + fee;
                jQuery('#total-to-raise').val(total_to_raise.toFixed(2)); // total to raise field
                jQuery('#total-raise-prediction').val(total_raise_prediction); // predicted to raise field
                difference = total_raise_prediction - total_to_raise;
                jQuery('#difference').val(difference); // difference field
                allow_sum = false;
            }
        })

        jQuery('#add_line').click(function(){
            n_lines++;
            line_content = '<tr>';
            line_content+= '<td><div class="input-prepend incentive-title"><input type="text" class="input-mini" data-id="'+n_lines+'" width="100"></div></td>';
            line_content+= '<td><div class="input-prepend"><span class="add-on">$</span><input type="text" class="input-mini donation-required" data-id="'+n_lines+'" width="50" value="0"></div></td>';
            line_content+= '<td><div class="input-prepend"><span class="add-on">$</span><input type="text" class="input-mini reward-cost" data-id="'+n_lines+'" width="50" value="0"></div></td>';
            line_content+= '<td><div class="input-prepend"><input type="text" class="input-mini backers-quantity" data-id="'+n_lines+'" width="50" value="0"></div></td>';
            line_content+= '<td><div class="input-prepend"><span class="add-on">$</span><input readonly="readonly" type="text" class="input-mini total-raised"  data-id="'+n_lines+'" width="100" value=""></div></td>';
            line_content+= '<td><div class="input-prepend"><span class="add-on">$</span><input readonly="readonly" type="text" class="input-mini total-cost"  data-id="'+n_lines+'" width="100" value=""></div></td>';
            line_content+= '</tr>';

            jQuery('.two tbody').append(line_content);
        })
    })
</script>
@include('front.footer')