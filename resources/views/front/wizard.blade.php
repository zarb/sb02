@include('front.header')
<div class="container wizard-container" ng-controller="newprojectController" <?php if(isset($project)){echo 'ng-init="loadProject('.$project->id.')"';}else{echo 'ng-init="clearForm()"';} ?> style="margin-bottom: 20px;">
    <div class="row">
        <div class="col-xs-6 col-sm-7 col-md-8">
            <div class="wizard-card" id="wizard">
                <ul class="nav nav-pills nav-wizard">
                    <li class="active"><a data-toggle="tab" href="#brief">{{trans('front.brief')}}</a><div class="nav-arrow"></div></li>
                    <li><div class="nav-wedge"></div><a data-toggle="tab" href="#details">{{trans('front.details')}}</a><div class="nav-arrow"></div></li>
                    <li><div class="nav-wedge"></div><a data-toggle="tab" href="#rewards">{{trans('front.rewards')}}</a></li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="brief">
                        <form class="form-horizontal" name="wizardform1" id="wizardform1">
                            <?php
                            if(!isset($project)){
                            ?>
                            <div class="row"><!-- Student photo start -->
                                <label for="" class="col-sm-3 control-label">{{trans('front.project_cover')}}</label>
                                <div class="col-sm-5">
                                    <input type="file" name="cover" id="cover" nv-file-select uploader="uploader" id="file-cover"/>
                                    <br><small style="font-weight: normal">{{trans('front.recommended_width')}}</small>
                                    <div ng-messages="wizardform.cover.$error">
                                        <span ng-show="wizardform.cover.$error.required" class="help-inline">{{trans('front.required')}}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <table class="table" ng-show="uploader.queue.length > 0">
                                        <tbody>
                                        <tr ng-repeat="item in uploader.queue">
                                            <td style="border: 0 !important"><button type="button" class="btn btn-success btn-xs" ng-click="item.upload()" ng-disabled="item.isReady || item.isUploading || item.isSuccess">
                                                    <span class="glyphicon glyphicon-check"></span> {{trans('front.click_upload')}}
                                                </button></td>
                                            <td ng-show="uploader.isHTML5" width="50%" style="border: 0 !important">
                                                <div class="progress" style="margin-bottom: 0;" ng-show="!item.isSuccess">
                                                    <div class="progress-bar" role="progressbar" ng-style="{ 'width': item.progress + '%' }"></div>
                                                </div>
                                                <span ng-show="item.isSuccess"><i class="glyphicon glyphicon-ok"></i></span>
                                                <span ng-show="item.isCancel"><i class="glyphicon glyphicon-ban-circle"></i></span>
                                                <span ng-show="item.isError"><i class="glyphicon glyphicon-remove"></i></span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div><!-- Student photo end -->
                            <?php
                            }
                            ?>
                            @if(isset($_GET['m']))
                                <div class="alert alert-info text-center"><b>{{trans('front.last_step')}}:</b> {{trans('front.simply_click')}}</div>
                            @endif
                            <hr style="margin-top: 4px !important">
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">{{trans('front.title')}}</label>
                                <div class="col-sm-9">
                                    <input type="text" name="title" id="title" ng-model="project.title" class="form-control" placeholder="{{trans('front.give_your_project_a_title')}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">{{trans('front.describe_your_project')}}</label>
                                <div class="col-sm-9">
                                    <textarea ng-model="project.resume" name="resume" id="resume" class="form-control" maxlength="140" rows="3" placeholder="{{trans('front.keep_it_short')}}"></textarea>
                                </div>
                            </div>
                            <div class="form-group"><!-- Project photo start -->
                                <label for="" class="col-sm-3 control-label">{{trans('front.project_photos')}}</label>
                                <div class="col-md-9">
                                    <input type="file" id="file-project" name="file-project" style="display: none" nv-file-select uploader="uploader2"/><br/>
                                    <div class="col-md-12">
                                        <table class="table" width="80%" ng-show="uploader2.queue.length > 0">
                                            <thead>
                                            <tr>
                                                <th width="50%">{{trans('front.name')}}</th>
                                                <th ng-show="uploader2.isHTML5">{{trans('front.progress')}}</th>
                                                <th>{{trans('front.status')}}</th>
                                                <th>{{trans('front.actions')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr ng-repeat="item in uploader2.queue">
                                                <td><strong>@{{ item.file.name }}</strong></td>
                                                <td ng-show="uploader2.isHTML5">
                                                    <div class="progress" style="margin-bottom: 0;">
                                                        <div class="progress-bar" role="progressbar" ng-style="{ 'width': item.progress + '%' }"></div>
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <span ng-show="item.isSuccess"><i class="glyphicon glyphicon-ok"></i></span>
                                                    <span ng-show="item.isCancel"><i class="glyphicon glyphicon-ban-circle"></i></span>
                                                    <span ng-show="item.isError"><i class="glyphicon glyphicon-remove"></i></span>
                                                </td>
                                                <td nowrap>
                                                    <button type="button" class="btn btn-success btn-xs" ng-click="item.upload()" ng-disabled="item.isReady || item.isUploading || item.isSuccess">
                                                        <span class="glyphicon glyphic  on-check"></span> {{trans('front.click_upload')}}
                                                    </button>
                                                    <button type="button" class="btn btn-danger btn-xs" ng-click="item.remove()">
                                                        <span class="glyphicon glyphicon-trash"></span>
                                                    </button>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <button type="button" class="btn btn-primary btn-sm" id="add-photo"><i class="glyphicon glyphicon-plus"></i> {{trans('front.add_files')}}</button>
                                </div>
                                <div class="row" ng-if="project.photos.length > 0">
                                    <div class="col-md-9 col-md-offset-3" style="padding-top: 10px">
                                        <div class="col-md-2 text-center" ng-repeat="photo in project.photos">
                                            <img ng-src="/projects_photos/@{{photo.photo}}" width="52">
                                             <a href="#" class="btn btn-danger btn-xs" style="postion:relative;top:0" ng-click="deletePhoto(photo.id)">{{trans('front.remove')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- Project photo end -->

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">{{trans('front.amount_you_want_to_raise')}}</label>
                                <div class="col-sm-9">
                                    <input type="number" name="target" id="target" min="1" string-to-number step="any" ng-model="project.target" class="form-control" placeholder="{{trans('front.your_target')}}">
                                    <div ng-messages="wizardform.number.$error">
                                        <span ng-show="wizardform.target.$error.required" class="help-inline">{{trans('front.required')}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">{{trans('front.end_date_of_campaign')}}</label>
                                <div class="col-sm-9">
                                    <div ng-messages="wizardform.enddate.$error">
                                        <span ng-show="wizardform.enddate.$error.required" class="help-inline">{{trans('front.required')}}</span>
                                    </div>
                                    <p class="input-group">
                                        <input type="text" name="enddate" id="enddate" class="form-control" uib-datepicker-popup="@{{format}}" ng-model="end_date" is-open="popup1.opened" datepicker-options="dateOptions" close-text="Close" alt-input-formats="altInputFormats" />
                                          <span class="input-group-btn">
                                            <button type="button" class="btn btn-default" ng-click="open1()"><i class="glyphicon glyphicon-calendar"></i></button>
                                          </span>

                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">{{trans('front.video')}}</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" ng-model="project.video" placeholder="{{trans('front.put_the_url_of_video')}}">
                                </div>
                            </div>
                            </form>
                    </div>
                    <div class="tab-pane" id="details">
                        <form class="form-horizontal" name="wizardform2" id="wizardform2">
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">{{trans('front.currency')}}</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="currency" id="currency" ng-model="project.currency">
                                        <option value="">Select</option>
                                        <option value="CAD">Canadian Dollar</option>
                                        <option value="EUR">Euro</option>
                                        <option value="GBP">Pound Sterling</option>
                                        <option value="CHF">Swiss Franc</option>
                                        <option value="USD">U.S. Dollar</option>
                                    </select>
                                    <div ng-messages="wizardform.currency.$error">
                                        <span ng-show="wizardform.currency.$error.required" class="help-inline">{{trans('front.required')}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">{{trans('front.category')}}</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="category" id="category" ng-model="project.category">
                                        <option ng-repeat="category in categories" ng-selected="category.id == project.category" value="@{{ category.id }}">@{{ category.name }}</option>
                                    </select>
                                    <div ng-messages="wizardform.category.$error">
                                        <span ng-show="wizardform.category.$error.required" class="help-inline">{{trans('front.required')}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">{{trans('front.project_language')}}</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="language" id="language" ng-model="project.language">
                                        <option ng-selected="project.language == 'en'" value="en">ENGLISH</option>
                                        <option ng-selected="project.language == 'fr'" value="fr">FRENCH</option>
                                    </select>
                                    <div ng-messages="wizardform.language.$error">
                                        <span ng-show="wizardform.language.$error.required" class="help-inline">{{trans('front.required')}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">{{trans('front.describe_your_project')}}</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="story" id="story" ng-model="project.story" rows="10" placeholder="{{trans('front.no_detail_too_small')}}"></textarea>
                                    <div ng-messages="wizardform.history.$error">
                                        <span ng-show="wizardform.history.$error.required" class="help-inline">{{trans('front.required')}}</span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane" id="rewards">
                        <form class="form-horizontal" name="wizardform3" id="wizardform3">
                        <p class="text-center"><small>* {{trans('front.please_order')}} <b>"{{trans('front.minimum_donations')}}"</b></small></p>
                        <div id="area_rewards">
                            <div id="frm_reward" ng-repeat="reward in rewards">
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">{{trans('front.reward_title')}}</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" ng-required="true" name="rtitle" id="rtitle" ng-model="reward.title" id="" placeholder="{{trans('front.give_your_reward_a_title')}}">
                                        <div ng-messages="wizardform3.rtitle.$error">
                                            <span ng-show="wizardform3.rtitle.$error.required && tsubmit" class="help-inline">{{trans('front.required')}}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">{{trans('front.reward_description')}}</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" ng-required="true" name="rdescription" id="rdescription" ng-model="reward.description" rows="5" placeholder=""></textarea>
                                        <div ng-messages="wizardform3.rdescription.$error">
                                            <span ng-show="wizardform3.rdescription.$error.required && tsubmit" class="help-inline">{{trans('front.required')}}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">{{trans('front.quantity_of_reward')}}</label>
                                    <div class="col-sm-9">
                                        <input type="number" string-to-number min="1" id="minimum" class="form-control" ng-model="reward.quantity" ng-readonly=" reward.uquantity || reward.quantity < 1" placeholder="{{trans('front.how_many_people')}}">
                                        <input type="checkbox" class="check_unlimited" ng-model="reward.uquantity" ng-click="reward.quantity = checkQuantity(reward.uquantity,reward.quantity)" > {{trans('front.unlimited')}}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">{{trans('front.minimum_donation')}}</label>
                                    <div class="col-sm-9">
                                        <input type="text" ng-required="true" name="mdonation" id="mdonation" string-to-number min="1" ui-number-mask="0" class="form-control minimum" ng-model="reward.minimum" placeholder="{{trans('front.minimum_donation_for')}}">
                                        <br> <button class="btn btn-xs btn-danger" ng-if="reward.id" ng-click="removeReward(reward.id,project.id)">{{ trans('front.Remove_Reward') }}</button>
                                        <div ng-messages="wizardform3.mdonation.$error">
                                            <span ng-show="wizardform3.mdonation.$error.required && tsubmit" class="help-inline">{{trans('front.required')}}</span>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>
                        </div>
                        <div class="row add-reward">
                            <div class="col-sm-9 col-sm-offset-3">
                                <a href="" class="btn btn-default" ng-click="addReward()">
                                    <i class="fa fa-plus"></i> {{trans('front.add_a_new_reward')}}
                                </a>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
                <div class="wizard-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pull-right" ng-show="!message">
                                <input class='btn btn-next btn-fill btn-info'
                                       name='next' type='button' value='{{trans('front.next')}}'>
                                <input <?php if(isset($project)){echo 'ng-click="!wizardform3.$error.required && save(\'edit\','.$project->id.',\'D\',\''.$student->mp_type.'\')"';}else{echo 'ng-click="save(\'new\',0,\'D\')"';} ?> class=
                                                                                      'btn btn-finish btn-fill btn-info btn-wd btn-sm btn-submit' name=
                                                                                      'finish' type='button' value='{{trans('front.save')}}'>

                                <input <?php if(isset($project)){echo 'ng-click="!wizardform3.$error.required && save(\'edit\','.$project->id.',\'PE\',\''.$student->mp_type.'\')"';}else{echo 'ng-click="save(\'new\',0,\'PE\')"';} ?> class=
                                       'btn btn-finish btn-fill btn-info btn-wd btn-sm btn-submit' name='finish' type='button' value='{{trans('front.publish')}}'>
                            </div>
                            <div class="pull-right" ng-show="message">
                                Saving...
                            </div>
                            <div class="pull-left">
                                <input class='btn btn-previous btn-fill btn-default' name=
                                'previous' type='button' value='{{trans('front.previous')}}'>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-5 col-md-3 col-md-offset-1">
            <div class="thumbnail">
                <a href=""><img class="img-responsive" ng-if="project.photo" ng-src="/projects_photos/@{{project.photo}}" alt="..."></a>
                <div class="caption">
                    <a href=""><h3>@{{ project.title==''?'Project Name':project.title }}</h3></a>
                    <h5><em>{{trans('front.by')}}</em> {{ $user_info->name  }}</h5>
                    <p>@{{ project.resume }}</p>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                            <span class="sr-only">0% Complete</span>
                        </div>
                    </div>
                    <div class="stats">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-4"><strong>0%</strong><br> {{trans('front.funded')}}</div>
                            <div class="col-md-4 col-sm-4 col-xs-4"><strong>@{{ project.target }}</strong><br> {{trans('front.goal')}}</div>
                            <div class="col-md-4 col-sm-4 col-xs-4"><strong>@{{ project.days }}</strong><br> {{trans('front.days_to_go')}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- row -->
</div><!--  big container -->
@{{wizardform.input.$error.required}}
@include('front.footer')
<script>
    $(document).ready(function(){
        $('#add-photo').click(function(){
            $('#file-project').click();
        })
        /*
        $('body').on('click','.check_unlimited',function(){
            status = $(this).prop('checked');
            if(status == 'true') {
                $('#minimum').val('');
                $('#minimum').attr('readonly', 'true');
            }
            else
                $('#minimum').attr('readonly', false);
        })*/

        /*
        $('.btn-submit').click(function(){
            $('.pull-right').html('saving...');
        })
        */
    })
</script>
<style>
    .my-drop-zone { border: dotted 3px lightgray; }
    .nv-file-over { border: dotted 3px red; } /* Default class applied to drop zones on over */
    .another-file-over-class { border: dotted 3px green; }
    html, body { height: 100%; }
</style>
