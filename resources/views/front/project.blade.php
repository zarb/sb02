@include('front.header')
<?php
switch($project->currency){
    case "CAD": $symbol = '$'; break;
    case "EUR": $symbol = '€'; break;
    case "GBP": $symbol = '£'; break;
    case "CHF": $symbol = '₣'; break;
    case "USD": $symbol = '$'; break;
}
?>
<header class="featured-image detail" style="background-image:url('{{'/projects_photos/'.$project->cover}}')">
    <h1 class="text-center">{{$project->title}}</h1>
    <div class="text-center">
        @if($project->video)
        <a href="{{$project->video}}" data-toggle="lightbox" data-gallery="sb-photos"><i class="fa fa-play-circle fa-3x" id="open-popup"></i></a>
        @endif
        <div>
            @if(isset($proj_photos))
                <ul class="list-inline" id="open-popup">
                    @foreach($proj_photos as $photo)
                        <li><a href="/projects_photos/{{$photo->photo}}" data-toggle="lightbox" data-gallery="sb-photos">
                                <img src="/projects_photos/{{$photo->photo}}" width="32" class="img-fluid">
                            </a></li>
                    @endforeach
                </ul>
            @endif
        </div>
    </div>
    <div class="gradient-overlay"></div>
</header>

<div class="detail-panel">
    <div class="container">
        <div class="row">
            <div class="col-md-4 user">
                <div class="row">
                    <div class="col-md-5 col-sm-5 col-xs-5"><img src="/profile_photos/{{$student->photo}}" alt="..." height="132" width="132" class="img-circle"></div>
                    <div class="col-md-7 col-sm-7 col-xs-7"><h5>{{$student->first_name.' '.$student->last_name}}</h5></div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p>{{strlen($student->biographical_inf)>1?substr($student->biographical_inf,0,155).'...':''}}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="{{ $project->percent }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $project->percent }}%;">
                        <span class="sr-only">{{ $project->percent }}% Complete</span>
                    </div>
                </div>
                <div class="stats">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-4"><strong>{{ $project->percent }}%</strong><br> {{ trans('front.funded')  }}</div>
                        <div class="col-md-4 col-sm-4 col-xs-4"><strong>{{ (int)$project->target}}</strong><br> {{ trans('front.goal') }} </div>
                        <div class="col-md-4 col-sm-4 col-xs-4"><strong>{{ $project->days }}</strong><br> {{ trans('front.days_to_go') }}</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-10 col-md-offset-2">
                        <a href="/donate/{{$project->id}}" class="btn btn-primary btn-block btn-lg">{{ trans('front.fund_this_project') }}</a>
                        <br>
                        <div class="text-center">
                            <a href=""><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                            <a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href=""><i class="fa fa-google-plus-official" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="tabs-strip">
    <div class="container">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#description" aria-controls="description" role="tab" data-toggle="tab">Description</a></li>
            <li role="presentation"><a href="#updates" aria-controls="updates" role="tab" data-toggle="tab">{{trans('front.updates')}}</a></li>
            <li role="presentation"><a href="#comments" aria-controls="comments" role="tab" data-toggle="tab">{{trans('front.comments')}}</a></li>
            <li role="presentation"><a href="#backers" aria-controls="backers" role="tab" data-toggle="tab">Backers</a></li>
        </ul>
    </div>
</div>

<div class="container project-info">
    <div class="row">
        <div class="col-md-8">
            <!-- Tab panes -->
            <div class="tab-content" ng-controller="updatesController" ng-init="load({{$project->id}},'{{$project->id_student}}','<?php if($logged_student)echo $logged_student->id;?>')">
                <div role="tabpanel" class="tab-pane active" id="description">
                    <div>
                        {!! nl2br($project->story) !!}
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="updates">
                    <?php
                    if($logged_student){
                        if($logged_student->id == $project->id_student){ //show updates form
                    ?>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Day</label>
                                <input type="number" min="1" maxlength="2" style="width: 100px" placeholder="dd" class="form-control" ng-model="regupdate.day">
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Month</label>
                                    <select class="form-control" ng-model="regupdate.month">
                                        <option value="">Select a month</option>
                                        <option>January</option>
                                        <option>February</option>
                                        <option>March</option>
                                        <option>April</option>
                                        <option>May</option>
                                        <option>June</option>
                                        <option>July</option>
                                        <option>August</option>
                                        <option>September</option>
                                        <option>October</option>
                                        <option>November</option>
                                        <option>December</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Year</label>
                                    <input type="number" min="1" maxlength="4" style="width: 100px" placeholder="yyyy" class="form-control" ng-model="regupdate.year">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" class="form-control" ng-model="regupdate.title" placeholder="Title of your update">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" ng-model="regupdate.description" placeholder="Description of your update"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <button type="button" href="#" ng-click="save({{$project->id}},{{$project->id}},'{{$project->id_student}}','<?php if($logged_student)echo $logged_student->id;?>')" class="btn btn-sm btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <?php
                        }
                    }
                    ?>
                    <div class="timeline">
                        <dl>
                            <dd ng-class="$index%2==0?'pos-right clearfix':'pos-left clearfix'" ng-repeat="up in updates">
                                <div class="circ"></div>
                                <div class="time">@{{ up.day }} @{{ up.month }} @{{ up.year }}</div>
                                <div class="events">
                                    <div class="events-body">
                                        <h4 class="events-heading">@{{ up.title }}</h4>
                                        <p>@{{ up.description }}</p>
                                    </div>
                                    <a href="#" ng-if="isOwner" style="color: red;" ng-click="exclui(up.id)"><i class="glyphicon glyphicon-remove"></i></a>
                                </div>
                            </dd>
                        </dl>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="comments">
                    <div class="media" ng-repeat="comment in comments">
                        <div class="media-body">
                            <h5 class="media-heading">@{{ comment.first_name }} @{{ comment.last_name }} <small></small></h5>
                            <p>
                                @{{ comment.comment }}
                            </p>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="backers">
                    <ul class="list-group">
                        <li class="list-group-item" ng-repeat="backer in backers">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="pull-left"><span class="backer">@{{ backer.first_name }} @{{ backer.last_name }}</span><br>
                                        <span class="backer" style="font-size: 12px !important;">Reward: @{{ backer.reward }}</span><br>
                                        <span class="backer" style="font-size: 12px !important;">@{{ backer.date }}</span>
                                    </div>
                                    <div class="pull-right backed-reward">{{$symbol}}@{{ backer.amount|number:0  }}</div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- display rewards -->

        <div class="col-md-4">
            @foreach($rewards as $reward)
            <div class="row">
                <div class="col-md-10 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-body panel-reward">
                            <h4>{{$reward->title}}</h4>
                            <p class="text-muted">{{$reward->quantity=='0'?trans('front.unlimited'):$reward->quantity}}</p>
                            <p class="text-muted reward-text">{{$reward->description}}</p>
                            <p class="text-muted">{{(int)$reward->minimum}}</p>
                            <a href="/donate/{{$project->id}}?reward={{$reward->id}}&value={{$reward->minimum>0?$reward->minimum:''}}">
                                <span class="link-spanner"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

    </div>
</div>
@include('front.footer')
<script>
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox({alwaysShowClose: false});
    });
</script>
<style>
    .modal-header{display: none !important;}
</style>