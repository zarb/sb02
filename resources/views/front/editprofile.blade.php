@include('front.header')
<div class="container create-profile">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <h3 class="text-center">Edit Profile</h3>
                    <form class="form-horizontal">
                        <fieldset>
                            <hr>
                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Name</label>
                                <div class="col-md-4">
                                    <input id="textinput" name="textinput" type="text" placeholder="" class="form-control input-md">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Date of Birth</label>
                                <div class="col-md-4">
                                    <input type="date" class="form-control" id="exampleInputDOB1" placeholder="Date of Birth">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Gender</label>
                                <div class="col-md-4">
                                    <select id="selectbasic" name="selectbasic" class="form-control">
                                        <option value="0"></option>
                                        <option value="1">Male</option>
                                        <option value="2">Female</option>
                                    </select>
                                </div>
                            </div>


                            <!-- Select Basic -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="selectbasic">Country</label>
                                <div class="col-md-4">
                                    <select id="selectbasic" name="selectbasic" class="form-control">
                                        <option value="0"></option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                    </select>
                                </div>
                            </div>

                            <!-- Textarea -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textarea">Tell us a bit about yourself</label>
                                <div class="col-md-4">
                                    <textarea class="form-control" id="textarea" name="textarea"></textarea>
                                </div>
                            </div>

                            <!-- File Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="filebutton">Profile photo</label>
                                <div class="col-md-4">
                                    <input id="filebutton" name="filebutton" class="input-file" type="file">
                                </div>
                            </div>

                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for=""></label>
                                <div class="col-md-4">
                                    <button id="" name="" class="btn btn-primary btn-lg btn-block">submit</button>
                                </div>
                            </div>

                        </fieldset>
                    </form>




                </div>
            </div>
        </div>
    </div>
</div>
@include('front.footer')