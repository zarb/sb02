@include('front.header')
<div class="container create-profile" ng-controller="studentController">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <h3 class="text-center">{{trans('front.edit_profile')}}</h3>
                    <form class="form-horizontal" name="profileform">
                        <fieldset>
                            <hr>
                            @if(isset($_GET['m']))
                                @if($_GET['m']=='success')
                                    <div class="alert alert-success text-center">{{trans('front.your_data_saved')}}</div>
                                @else
                                    <div class="alert alert-danger text-center">{{trans('front.complete_profile')}}</div>
                                @endif
                            @endif
                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">{{trans('front.first_name')}}</label>
                                <div class="col-md-4">
                                    <input type="text" required="true" name="firstName" id="firstName" ng-model="student.first_name" placeholder="Your first name" class="form-control input-md">
                                    <div ng-messages="profileform.firstName.$error">
                                        <span ng-show="profileform.firstName.$error.required" class="help-inline">{{trans('front.required')}}</span>
                                    </div>
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">{{trans('front.last_name')}}</label>
                                <div class="col-md-4">
                                    <input type="text" ng-required="true" name="lastName" id="lastName" ng-model="student.last_name" placeholder="Your last name" class="form-control input-md">
                                    <div ng-messages="profileform.lastName.$error">
                                        <span ng-show="profileform.lastName.$error.required" class="help-inline">{{trans('front.required')}}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">{{trans('front.date_of_birth')}}*</label>
                                <div class="col-md-4">
                                    <input type="date" ng-required="true" class="form-control" name="dob" id="dob" ng-model="dob" placeholder="{{trans('front.date_of_birth')}}">
                                    <div ng-messages="profileform.dob.$error">
                                        <span ng-show="profileform.dob.$error.required" class="help-inline">{{trans('front.required')}}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">{{trans('front.gender')}}</label>
                                <div class="col-md-4">
                                    <select ng-model="student.gender" class="form-control">
                                        <option value=""></option>
                                        <option value="M">{{trans('front.male')}}</option>
                                        <option value="F">{{trans('front.female')}}</option>
                                    </select>
                                </div>
                            </div>


                            <!-- Select Basic -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="selectbasic">{{trans('front.country')}}</label>
                                <div class="col-md-4">
                                    <select class="form-control" ng-required="true" name="country" id="country" ng-model="student.id_country">
                                        <option value="">Select an option</option>
                                        <option ng-repeat="country in countries| orderBy:'name'" ng-selected="country.id == student.id_country" value="@{{ country.id }}">@{{ country.name }}</option>
                                    </select>
                                    <div ng-messages="profileform.lastName.$error">
                                        <span ng-show="profileform.country.$error.required" class="help-inline">{{trans('front.required')}}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">{{trans('front.city')}}</label>
                                <div class="col-md-4">
                                    <input class="form-control" ng-model="student.city">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">{{trans('front.address')}}</label>
                                <div class="col-md-4">
                                    <input class="form-control" ng-model="student.address">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">{{trans('front.postal_code')}}</label>
                                <div class="col-md-4">
                                    <input class="form-control" ng-model="student.postal_code">
                                </div>
                            </div>

                            <!-- Textarea -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textarea">{{trans('front.tell_us_a_bit')}}</label>
                                <div class="col-md-4">
                                    <textarea class="form-control" ng-model="student.biographical_inf" id="textarea" name="textarea"></textarea>
                                </div>
                            </div>

                            <!-- File Button -->
                            <div class="form-group"><!-- Student photo start -->
                                <label class="col-md-4 control-label">{{trans('front.change_profile_photo')}}</label>
                                <div class="col-md-4">
                                    <input type="file" nv-file-select uploader="uploader"/><br/>
                                    <div class="col-md-1">
                                    </div>
                                    <div class="col-md-11">
                                        <table class="table" width="80%" ng-show="uploader.queue.length > 0">
                                            <thead>
                                            <tr>
                                                <th width="50%">Name</th>
                                                <th ng-show="uploader2.isHTML5">Progress</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr ng-repeat="item in uploader.queue">
                                                <td><strong>@{{ item.file.name }}</strong></td>
                                                <td ng-show="uploader.isHTML5">
                                                    <div class="progress" style="margin-bottom: 0;">
                                                        <div class="progress-bar" role="progressbar" ng-style="{ 'width': item.progress + '%' }"></div>
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <span ng-show="item.isSuccess"><i class="glyphicon glyphicon-ok"></i></span>
                                                    <span ng-show="item.isCancel"><i class="glyphicon glyphicon-ban-circle"></i></span>
                                                    <span ng-show="item.isError"><i class="glyphicon glyphicon-remove"></i></span>
                                                </td>
                                                <td nowrap>
                                                    <button type="button" class="btn btn-success btn-xs" ng-click="item.upload()" ng-disabled="item.isReady || item.isUploading || item.isSuccess">
                                                        <span class="glyphicon glyphic  on-check"></span> Click to Upload
                                                    </button>
                                                    <button type="button" class="btn btn-danger btn-xs" ng-click="item.remove()">
                                                        <span class="glyphicon glyphicon-trash"></span>
                                                    </button>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div><!-- Project photo end -->

                            <hr>

                            <h4 class="text-center" style="margin-bottom: 20px">{{trans('front.bank_details')}}<br><small>{{trans('front.receive_your_funds')}}</small></h4>

                            <div class="form-group">
                                <label class="col-md-4 control-label">{{trans('front.currency')}}</label>
                                <div class="col-md-4">
                                    <select class="form-control" ng-model="student.currency">
                                        <option value="">-- Choose an option --</option>
                                        <option value="EURO">EURO</option>
                                        <option value="POUND">POUND STERLING</option>
                                        <option value="CAD">CANADIAN DOLLAR</option>
                                        <option value="CHF">SWISS FRANC</option>
                                        <option value="USD">U.S. DOLLAR</option>
                                    </select>
                                </div>
                            </div>

                            <div id="area_euro" ng-if="student.currency=='EURO'">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Iban</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" ng-model="student.iban">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Bic</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" ng-model="student.bic">
                                    </div>
                                </div>
                            </div>

                            <div id="area_pound" ng-if="student.currency=='CHF'">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Bic</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" ng-model="student.bic">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Account number</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" ng-model="student.account_number">
                                    </div>
                                </div>
                            </div>

                            <div id="area_pound" ng-if="student.currency=='POUND'">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Account number</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" ng-model="student.account_number">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Sort Code</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" ng-model="student.sort_code">
                                    </div>
                                </div>
                            </div>

                            <div id="area_pound" ng-if="student.currency=='USD'">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Account number</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" ng-model="student.account_number">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">ABA</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" ng-model="student.aba">
                                    </div>
                                </div>
                            </div>

                            <div id="area_pound" ng-if="student.currency=='CAD'">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Branch Code</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" ng-model="student.branch_code">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Bank Name</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" ng-model="student.bank_name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Institution number</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" ng-model="student.institution_number">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Account number</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" ng-model="student.account_number">
                                    </div>
                                </div>
                            </div>


                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for=""></label>
                                <div class="col-md-4">
                                    <button type="button" id="btn-submit" ng-click="profileform.$valid && save('<?php if(isset($_GET['id']))echo $_GET['id']; ?>')" class="btn btn-primary btn-lg btn-block">{{trans('front.submit')}}</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .help-inline{
        color: red;
        font-weight: bold;
    }
</style>
@include('front.footer')
<script>
    $('#btn-submit').click(function(){
        if($('#firstName').val()=='')
            $('#firstName').focus();
        else if($('#lastName').val()=='')
            $('#lastName').focus();
        else if($('#country').val()=='')
            $('#lastName').focus();
    })
</script>