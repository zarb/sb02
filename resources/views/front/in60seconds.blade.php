@include('front.header')
<section class="container">
    <div class="col-sm-12 col-md-12">
        <h1 style="font-size: 38.5px;text-align: center;">{{ trans('front.60_need_money') }}</h1>
        <h2 style="font-size: 17.5px;text-align: center;">{{ trans('front.60_discover_student') }}</h2>
        <p><br></p>
        <div style="text-align: center;">
            <iframe width="736" height="414" src="{{ trans('front.60_url_video') }}" frameborder="0" allowfullscreen></iframe>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h3><a href="/contact/{{$lang}}">{{ trans('front.contact_us') }}</a></h3>
                <p>{{ trans('front.60_cant_find') }}</p>
                <p><strong><u><a href="/contact/{{$lang}}">{{ trans('front.60_click_here') }}</a></u></strong> {{ trans('front.60_to_send_us') }}</p>
            </div>
            <div class="col-md-4">
                <h3><a href="/contact/{{$lang}}">{{ trans('front.60_request') }}</a></h3>
                <p>{{ trans('front.60_every_week') }}</p>
                <p>{{ trans('front.60_request_a_place') }} <a href="/contact/{{$lang}}">{{ trans('front.here') }}</a></p>
            </div>
            <div class="col-md-4">
                <h3><a href="/login/{{$lang}}">{{ trans('front.60_create_project') }}</a></h3>
                <p>{{ trans('front.60_ready_finance') }}</p>
                <p>{{ trans('front.click') }} <a href="/login/{{$lang}}">{{ trans('front.60_here_create') }}</a></p>
            </div>
        </div>
    </div>
</section>
@include('front.footer')