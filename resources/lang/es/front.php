<?php

return [
    'terms'=>'Estoy de acuerdo con los términos y condiciones',
    'have_account'=>'Ya tiene su cuenta?',
    'sign_in'=>'Registrarse',
    'sign_up'=>'Regístrate',
    'register'=>'Registra tu cuenta',
];