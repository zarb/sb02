var app = angular.module('sbackrApp',['textAngular','ngAnimate', 'ui.bootstrap','angularFileUpload','ui.utils.masks'])
    .constant('API_URL','http://localhost:8000/api/v1/')
    .directive('stringToNumber', function() {
        return {
            require: 'ngModel',
            link: function(scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function(value) {
                    return '' + value;
                });
                ngModel.$formatters.push(function(value) {
                    return parseFloat(value);
                });
            }
        };
    });