app.controller('studentController',function($scope,$http,API_URL,FileUploader){
    $http.get(API_URL + "countries")
        .success(function(response){
            $scope.countries = response;
        })

    $http.get(API_URL + "student")
        .success(function(response){
            $scope.student = response;
            if($scope.student.dob) {
                dob = $scope.student.dob.replace('-', '/');
                $scope.dob = new Date(dob);
            }
        });

    //save new record / update existing record
    $scope.save = function(draft){

        var url = API_URL + 'students/'  + $scope.student.id;
        $scope.student.dob = moment($scope.dob).format('YYYY/MM/DD');

        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.student),
            headers: {'Content-Type':'application/x-www-form-urlencoded'}
        }).success(function(response){
            console.log(response);
            if(draft)
                location.href = '/wizard/'+draft+'?m=publish';
            else
                location.href = '/profile?m=success';
        }).error(function(response){
            console.log(response);
            alert('This is embarassing. An error has occured. Please try again in few minutes');
        });
    }

    //STUDENT PROFILE PICTURE start
    $scope.uploader = new FileUploader({url: '/up-picture'});

    $scope.uploader.onSuccessItem = function(item,result){
        var img = result;
        $scope.student.photo = img;
    };
    //STUDENT PROFILE PICTURE end
})