app.controller('allfrontController',function($scope,$http, API_URL) {
    $scope.all = function(){
        $http.get(API_URL + "projects")
            .success(function (response) {
                $scope.projects = response;
            });
    }

    $http.get(API_URL + "categories")
        .success(function(response){
            $scope.categories = response;
        });

    $scope.search = function(text){
        $http.get(API_URL + "projects/search/"+text)
            .success(function(response){
                $scope.projects = response;
            });
    }

    $scope.filterByCategory = function(id){
        angular.element(document.querySelectorAll('.cat')).removeClass('active');
        angular.element(document.querySelector('#li_'+id)).addClass('active');

        if(id == 'N'){
            $http.get(API_URL + "projects")
                .success(function(response){
                    $scope.projects = response;
                });
        }
        else{
            $http.get(API_URL + "projects/cat/"+id)
                .success(function(response){
                    $scope.projects = response;
                });

        }
    }

    $scope.diffDays = function(date1,date2){
        date1 = moment(new Date(date1));
        date2 = moment(new Date(date2));

        var diffDays = date2.diff(date1, 'days');
        if(diffDays < 1 || isNaN(diffDays))
            return 0;
        else
            return diffDays;
    }

    $scope.getSymbol = function(curr){
        var symbol = '';
        switch (curr){
            case "CAD": symbol = '$'; break;
            case "EUR": symbol = '€'; break;
            case "GBP": symbol = '£'; break;
            case "CHF": symbol = '₣'; break;
            case "USD": symbol = '$'; break;
            default: symbol = ''; break;
        }
        return symbol;
    }
});