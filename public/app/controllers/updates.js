app.controller('updatesController',function($scope,$http,API_URL){
    $scope.load = function(project_id,owner,student)
    {
        if(!$scope.project_id)
            $scope.project_id = project_id;

        if(owner == student)
            $scope.isOwner = owner;
        else
            $scope.isOwner = '';

        $http.get(API_URL + "updates/" + $scope.project_id)
            .success(function (response) {
                $scope.updates = response;
            })

        $http.get(API_URL + "backers/" + project_id)
            .success(function (response) {
                $scope.backers = response;
            })

        $http.get(API_URL + "comments/" + project_id)
                .success(function (response) {
                    $scope.comments = response;
                })
    }

    //save new record / update existing record
    $scope.save = function(project_id){
        console.log('saving...');

        var url = API_URL + 'updates/'  + project_id;

        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.regupdate),
            headers: {'Content-Type':'application/x-www-form-urlencoded'}
        }).success(function(response){
            console.log(response);
            $scope.load(project_id);
            //location.reload();
        }).error(function(response){
            console.log(response);
            //alert('This is embarassing. An error has occured. Please try again in few minutes');
        });
    }

    $scope.exclui = function(id){
        $http.get(API_URL + "delete_update/" + id)
            .success(function () {
                $scope.load();
            })
    }
});