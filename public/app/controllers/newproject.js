app.controller('newprojectController',function($scope,$http,$location,FileUploader,API_URL) {

    $scope.clearForm = function () {
        $scope.project.title = undefined;
    }

    $scope.loadProject = function(id){
        $http.get(API_URL + 'projects/' + id)
            .success(function(response){
                $scope.project = response;
                $scope.project.category = $scope.project.id_category;

                //dt_ini = $scope.project.start_date.replace('-','/');
                dt_end = $scope.project.end_date.replace('-','/');
                //$scope.ini_date = new Date(dt_ini);
                $scope.end_date = new Date(dt_end);

                $scope.getRewards(id); //get rewards
            });
    }

    $scope.checkQuantity = function(item,qtd){
        if(item)
            return 0;
        else
            return 1;
    }

    $scope.deletePhoto = function (photo) {
        var url = API_URL + 'projects/deletephoto/'+photo;

        $http.get(url)
            .success(function(response){
                location.reload();
            })
            .error(function(response){
                alert('error');
            })
    }

    $http.get(API_URL + "categories")
        .success(function(response){
            $scope.categories = response;
        });

    //get all rewards of a specific project
    $scope.getRewards = function(project){
        $http.get(API_URL + "projects/rewards/" + project)
            .success(function(response){
                $scope.rewards = response;
            })
    }

    $scope.removeReward = function(reward_id,project_id){
        $http.get(API_URL + "reward/remove/" + reward_id)
            .success(function(response){
                console.log('Reward removed successfully');
                $scope.getRewards(project_id);
            })
            .error(function(response) {
                console.log('Error: ' + response);
            })
    }

    if(!$scope.project) {
        $scope.project = {};
    }

    if(!$scope.rewards) {
        $scope.rewards = [
            {
                title: '',
                description: '',
                count: 0,
                uquantity: true,
                quantity: 0
            }];
    }

    if(!$scope.photos) {
        $scope.photos = [];
    }

    if($scope.project.photo)
        $scope.project.photo = ' ';
    if($scope.project.cover)
        $scope.project.cover = ' ';

    //PROJECT COVER PICTURE start
    $scope.uploader = new FileUploader({url: '/up-cover'});

    $scope.uploader.onSuccessItem = function(item,result){
        var img = result;
        $scope.project.photo = img;
        $scope.project.cover = img;
    };
    //PROJECT COVER PICTURE end

    //PROJECT PICTURE start
    $scope.uploader2 = new FileUploader({url: '/up-project-picture',alias: 'file2'});

    $scope.uploader2.filters.push({
        name: 'customFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });

    $scope.uploader2.onSuccessItem = function(item,result){
        var img = result;
        //if(!$scope.project.photo)
            //$scope.project.photo = img;

        $scope.photos.push(img);

        console.log(result);
    };
    //PROJECT PICTURE end

    $scope.addReward = function () {
        $scope.rewards.push({
            title: '',
            description: '',
            count: 0,
            minimum: '',
            quantity: 0,
            uquantity: true
        });
    };

    $scope.diffDays = function(date1,date2){
        date1 = moment(new Date(date1));
        date2 = moment(new Date(date2));

        var diffDays = date2.diff(date1, 'days');
        if(diffDays < 0 || isNaN(diffDays))
            return 0;
        else
            return diffDays;
    }

    $scope.save = function(modalstate,id,status,mp_type){
        $scope.tsubmit = 1;

        if($scope.rewards[0].title == '' || $scope.rewards[0].description == '' || $scope.rewards[0].minimum == ''){
            console.log('empty fields');
            console.log($scope.rewards[0].title);
            console.log($scope.rewards[0].description);
            console.log($scope.rewards[0].minimum);
            return false;
        }
        else{
            console.log($scope.rewards[0].title);
            console.log($scope.rewards[0].description);
            console.log($scope.rewards[0].minimum);
        }


        $scope.message = 'saving';

        var url = API_URL + 'projects';

        //append student id to te URL if the form is in edit mode
        if(modalstate === 'edit'){
            url += "/" + id;
        }

        if(!status)
            status = 'D';

        p_status = status;
        if(!mp_type && status == 'PE')//if student has not completed his profile set project status as Draft
            p_status = 'D';

        $scope.project.status = p_status;
        $scope.project.id_category = $scope.project.category;
        $scope.project.end_date = moment($scope.end_date).format('YYYY/MM/DD');

        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.project),
            headers: {'Content-Type':'application/x-www-form-urlencoded'}
        }).success(function(response){
            id_project = response;
            console.log(id_project);

            //---------
            //saving photos in database
            if($scope.photos.length > 0) {
                var url = API_URL + 'projectphotos/'+id_project;

                var jsonData=angular.toJson($scope.photos);
                var objectToSerialize={'photos':jsonData};

                $http({
                    method: 'POST',
                    url: url,
                    data: $.param(objectToSerialize),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    console.log(response);
                }).error(function (response) {
                    console.log(response);
                    console.log('[1]This is embarassing. An error has occured. Please try again in few minutes');
                })
            }

            //----------
            //saving rewards
            var url = API_URL + 'reward';

            for(i in $scope.rewards){
                reward = $scope.rewards[i];
                reward.id_project = id_project;

                console.log('Reward ' + i + ':' + JSON.stringify(reward));

                $http({
                    method: 'POST',
                    url: url,
                    data: $.param(reward),
                    headers: {'Content-Type':'application/x-www-form-urlencoded'}
                }).success(function(response){
                    console.log(response);
                }).error(function(response){
                    console.log('error[2]: ' + response);
                })
            }

            if(!mp_type && status == 'PE')
                location.href='/profile?m=complete&id='+id_project;
            else
                location.href='/onboard';

        }).error(function(response){
            console.log(response);
            alert('[3]This is embarassing. An error has occured. Please try again in few minutes');
        })
    }

    //start of date functions
    $scope.today = function() {
        $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function() {
        $scope.dt = null;
    };

    $scope.inlineOptions = {
        customClass: getDayClass,
        minDate: new Date(),
        showWeeks: true
    };

    $scope.dateOptions = {
        //dateDisabled: disabled,
        formatYear: 'yy',
        maxDate: new Date(2020, 5, 22),
        minDate: new Date(),
        startingDay: 1
    };

    // Disable weekend selection
    function disabled(data) {
        var date = data.date,
            mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }

    $scope.toggleMin = function() {
        $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
        $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
    };

    $scope.toggleMin();

    $scope.open1 = function() {
        $scope.popup1.opened = true;
    };

    $scope.open2 = function() {
        $scope.popup2.opened = true;
    };

    $scope.setDate = function(year, month, day) {
        $scope.dt = new Date(year, month, day);
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[1];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.popup1 = {
        opened: false
    };

    $scope.popup2 = {
        opened: false
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);
    $scope.events = [
        {
            date: tomorrow,
            status: 'full'
        },
        {
            date: afterTomorrow,
            status: 'partially'
        }
    ];

    function getDayClass(data) {
        var date = data.date,
            mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0,0,0,0);

            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }

        return '';
    }

});

