app.controller('studentsController',function($scope,$http, API_URL){
    //retrieve students listing from API
    $http.get(API_URL + "students")
        .success(function(response){
            $scope.students = response;
        });

    $http.get(API_URL + "countries")
        .success(function(response){
            $scope.countries = response;
        })

    //show modal form
    $scope.toggle = function(modalstate, id){
        $scope.modalstate = modalstate;
        switch (modalstate){
            case 'add':
                $scope.form_title = 'Add new Student';
                break;
            case 'edit':
                $scope.form_title = 'Edit Student';
                $scope.id = id;
                $http.get(API_URL + 'students/' + id)
                        .success(function(response){
                            $scope.student = response;
                        });
                break;
            default:
                break;
        }
        $('#studentModal').modal('show');
    }

    $scope.newmpuser = function(id){
        $http.post(API_URL + 'students/newmpuser/'+id)
            .success(function(response){
                location.reload();
            })
            .error(function(){
                alert('Sorry. Please contact the support.');
            })
    }

    //save new record / update existing record
    $scope.save = function(){
        alert(draft);
        var url = API_URL + 'students';

        //append student id to te URL if the form is in edit mode
        if(modalstate === 'edit'){
            url += "/" + id;
        }

        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.student),
            headers: {'Content-Type':'application/x-www-form-urlencoded'}
        }).success(function(response){
            console.log(response);
            location.reload();

        }).error(function(response){
            console.log(response);
            alert('This is embarassing. An error has occured. Please try again in few minutes');
        });
    }

    //delete record
    $scope.confirmDelete = function(id){
        var isConfirmDelete = confirm('Are you sure?');
        if(isConfirmDelete){
            $http({
                method: 'DELETE',
                url: API_URL + 'stuents/' + id
            }).success(function(data){
               console.log(data);
                location.reload();
            }).error(function(data){
                console.log(data);
                alert('Unable to delete');
            });
        }
        else{
            return false;
        }
    }
});