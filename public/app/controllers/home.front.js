app.controller('homefrontController',function($scope,$http, API_URL) {
    /*
    $http.get(API_URL + "projects")
        .success(function(response){
            $scope.projects = response;
        });
    */

    //Most urgent
    $http.get(API_URL + "urgent")
        .success(function(response){
            $scope.urgents = response;
        });

    //Most recent
    $http.get(API_URL + "recent")
        .success(function(response){
            $scope.recents = response;
        });

    //Most popular
    $http.get(API_URL + "popular")
        .success(function(response){
            $scope.popular = response;
        });

    //Mini projects
    $http.get(API_URL + "mini")
        .success(function(response){
            $scope.mini = response;
        });

    $http.get(API_URL + "featuredcategories")
        .success(function(response){
            $scope.categories = response;
            if(response)
                $scope.setFeatured($scope.categories[0].id);
        });

    $scope.setFeatured = function(cat){
        $http.get(API_URL + "featured/" + cat)
            .success(function(response){
                $scope.featured = response;
            });
        angular.element(document.querySelectorAll('.cat')).removeClass('active');
        angular.element(document.querySelector('#li_'+cat)).addClass('active');
    }

    $scope.diffDays = function(date1,date2){
        date1 = moment(new Date(date1));
        date2 = moment(new Date(date2));

        var diffDays = date2.diff(date1, 'days');

        if(diffDays < 0 || isNaN(diffDays))
            return 0;
        else
            return diffDays;
    }
});