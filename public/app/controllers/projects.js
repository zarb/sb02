app.controller('onboardController',function($scope,$http, API_URL) {
    //retrieve projects listing from API
    $http.get(API_URL + "userprojects")
        .success(function(response){
            $scope.projects = response;
        });

    $scope.labelTag = function(s,l='en'){
        var r = '';
        if(l == 'fr'){
            switch (s){
                case 'D': r = 'Brouillon'; break;
                case 'PE': r = 'En attente'; break;
                case 'PU': r = 'Publié'; break;
                case 'F': r = 'Removed'; break;
            }
        }
        else{
            switch (s){
                case 'D': r = 'Draft'; break;
                case 'PE': r = 'Pending'; break;
                case 'PU': r = 'Published'; break;
                case 'F': r = 'Removed'; break;
            }
        }
        return r;
    }

    $scope.classTag = function(s){
        var r = '';
        switch (s){
            case 'D': r = 'label label-danger arrowed-right'; break;
            case 'PE': r = 'label label-info arrowed-right'; break;
            case 'PU': r = 'label label-success arrowed-right'; break;
            case 'F': r = 'label label-warning arrowed-right'; break;
        }
        return r;
    }

    $scope.diffDays = function(date1,date2){
        date1 = moment(new Date(date1));
        date2 = moment(new Date(date2));

        var diffDays = date2.diff(date1, 'days');
        if(diffDays < 0 || isNaN(diffDays))
            return 0;
        else
            return diffDays;
    }

    $scope.getSymbol = function(curr){
        var symbol = '';
        switch (curr){
            case "CAD": symbol = '$'; break;
            case "EUR": symbol = '€'; break;
            case "GBP": symbol = '£'; break;
            case "CHF": symbol = '₣'; break;
            case "USD": symbol = '$'; break;
            default: symbol = ''; break;
        }
        return symbol;
    }
})

app.controller('projectsController',function($scope,$http, API_URL){
    //retrieve projects listing from API
    $http.get(API_URL + "projectsadmin")
        .success(function(response){
            $scope.projects = response;
        });

    $http.get(API_URL + "categories")
        .success(function(response){
            $scope.categories = response;
        });

    //show modal form
    $scope.toggle = function(modalstate, id){
        $scope.modalstate = modalstate;
        switch (modalstate){
            case 'add':
                $scope.form_title = 'Add new Project';
                break;
            case 'edit':
                $scope.form_title = 'Edit Project';
                $scope.id = id;
                $http.get(API_URL + 'projects/' + id)
                        .success(function(response){
                            $scope.project = response;
                            //dt_ini = $scope.project.start_date.replace('-','/');
                            //dt_end = $scope.project.end_date.replace('-','/');
                            dt_end = $scope.project.end_date;
                            //$scope.ini_date = new Date(dt_ini);
                            $scope.end_date = new Date(dt_end+' 00:00:00');

                            $scope.getRewards(id); //get rewards
                            $scope.getCategoriesLang(response.language); //get rewards
                        });
                break;
            default:
                break;
        }
        $('#projectModal').modal('show');
    }

    $scope.showRstatus = function(status){
        switch(status){
            case 'P':
                s = '<span class="label label-warning">Pending</span>';
                break;
            case 'F':
                s = '<span class="label label-danger">Fail</span>';
                break;
            case 'S':
                s = '<span class="label label-success">Success</span>';
                break;
            default:
                s = '<span class="label label-default">' + status + '</span>';
                break;
        }
        return s;
    }

    $scope.showDonations = function(project){
        id_project = project.id;
        $scope.total = $scope.totalS = $scope.totalF = 0;
        $scope.project_id = id_project;
        $http.get(API_URL + "projects/donations/" + id_project)
            .success(function(response){
                $scope.donations = response;
                for(var r in response){
                    $scope.total += parseFloat(response[r].amount);
                    if(response[r].status == 'S')
                        $scope.totalS += parseFloat(response[r].amount);
                    else
                        $scope.totalF += parseFloat(response[r].amount);

                    if(project.currency == 'EUR')
                        $scope.type_currency = '€';
                    else if(project.currency == 'USD')
                        $scope.type_currency = 'USD';

                    $scope.target = project.target;
                    if($scope.totalS >= project.target){
                        $scope.fee = 7;
                        $scope.fee_value = (7 * $scope.totalS) / 100;
                    }
                    else{
                        $scope.fee = 10;
                        $scope.fee_value = (10 * $scope.totalS) / 100;
                    }
                }
            })

        $('#donationModal').modal('show');
    }

    //get all rewards of a specific project
    $scope.getRewards = function(project){
        $http.get(API_URL + "projects/rewards/" + project)
            .success(function(response){
                $scope.rewards = response;
            })
    }

    //get categories by language
    $scope.getCategoriesLang = function(lang){
        $http.get(API_URL + "categoriesbylang/" + lang)
            .success(function(response){
                $scope.categories_proj = response;
            })
    }

    $scope.showStatus = function (status) {
        var r = '';
        switch (status){
            case 'D':
                r = 'Draft';
                break;
            case 'PE':
                r = 'Pending';
                break;
            case 'PU':
                r = 'Published';
                break;
            case 'F':
                r = 'Removed';
                break;
        }
        return r;
    }

    $scope.opstatus = [
                        {
                            id: 'D',
                            label:'Draft',
                        },
                        {
                            id: 'PE',
                            label:'Pending'
                        },
                        {
                            id: 'PU',
                            label:'Published'
                        },
                        {
                            id: 'F',
                            label:'Removed'
                        }];

    //save new record / update existing record
    $scope.save = function(modalstate, id){
        var url = API_URL + 'projects';

        //append project id to te URL if the form is in edit mode
        if(modalstate === 'edit'){
            url += "/" + id;
        }

        //$scope.project.start_date = moment($scope.ini_date).format('YYYY/MM/DD');
        $scope.project.end_date = moment($scope.end_date).format('YYYY/MM/DD');

        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.project),
            headers: {'Content-Type':'application/x-www-form-urlencoded'}
        }).success(function(response){
            console.log(response);
            location.reload();
        }).error(function(response){
            console.log(response);
            alert('This is embarassing. An error has occured. Please try again in few minutes');
        });
    }

    //delete record
    $scope.confirmDelete = function(id){
        var isConfirmDelete = confirm('Are you sure?');
        if(isConfirmDelete){
            $http({
                method: 'GET',
                url: API_URL + 'projects/remove/' + id
            }).success(function(data){
               console.log(data);
               location.reload();
            }).error(function(data){
                console.log(data);
                alert('Unable to delete');
            });
        }
        else{
            return false;
        }
    }

    //start of date functions
    $scope.today = function() {
        $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function() {
        $scope.dt = null;
    };

    $scope.inlineOptions = {
        customClass: getDayClass,
        minDate: new Date(),
        showWeeks: true
    };

    $scope.dateOptions = {
        //dateDisabled: disabled,
    };

    // Disable weekend selection
    function disabled(data) {
        var date = data.date,
            mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }

    $scope.toggleMin = function() {
        $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
        $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
    };

    $scope.toggleMin();

    $scope.open1 = function() {
        $scope.popup1.opened = true;
    };

    $scope.open2 = function() {
        $scope.popup2.opened = true;
    };

    $scope.setDate = function(year, month, day) {
        $scope.dt = new Date(year, month, day);
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy-MM-dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[1];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.popup1 = {
        opened: false
    };

    $scope.popup2 = {
        opened: false
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);
    $scope.events = [
        {
            date: tomorrow,
            status: 'full'
        },
        {
            date: afterTomorrow,
            status: 'partially'
        }
    ];

    function getDayClass(data) {
        var date = data.date,
            mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0,0,0,0);

            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }

        return '';
    }
});