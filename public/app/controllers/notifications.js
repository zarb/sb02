app.controller('notificationsController',function($scope,$http, API_URL){
    //retrieve categories listing from API
    $http.get(API_URL + "notifications")
        .success(function(response){
            $scope.notifications = response;
        });

    //show modal form
    $scope.toggle = function(modalstate, id){
        $scope.modalstate = modalstate;
        switch (modalstate){
            case 'add':
                $scope.form_title = 'Add new Notification';
                break;
            case 'edit':
                $scope.form_title = 'Edit Notification';
                $scope.id = id;
                $http.get(API_URL + 'notifications/' + id)
                        .success(function(response){
                            $scope.notification = response;
                        });
                break;
            default:
                break;
        }
        $('#notificationModal').modal('show');
    }

    //save new record / update existing record
    $scope.save = function(modalstate, id){
        var url = API_URL + 'notifications';

        //append student id to te URL if the form is in edit mode
        if(modalstate === 'edit'){
            url += "/" + id;
        }

        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.notification),
            headers: {'Content-Type':'application/x-www-form-urlencoded'}
        }).success(function(response){
            console.log(response);
            location.reload();
        }).error(function(response){
            console.log(response);
            alert('This is embarassing. An error has occured. Please try again in few minutes');
        });
    }

    //delete record
    $scope.confirmDelete = function(id){
        var isConfirmDelete = confirm('Are you sure?');
        if(isConfirmDelete){
            $http({
                method: 'DELETE',
                url: API_URL + 'notifications/' + id
            }).success(function(data){
               console.log(data);
                location.reload();
            }).error(function(data){
                console.log(data);
                alert('Unable to delete');
            });
        }
        else{
            return false;
        }
    }
});