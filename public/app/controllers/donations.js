app.controller('donationsController',function($scope,$http, API_URL){

    $http.get(API_URL + "lastdonations")
        .success(function(response){
            $scope.donations = response;
        });

});