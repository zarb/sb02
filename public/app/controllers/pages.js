app.controller('pagesController',function($scope,$http,FileUploader,API_URL){
    //retrieve categories listing from API
    $http.get(API_URL + "pages")
        .success(function(response){
            $scope.pages = response;
        });

    //show modal form
    $scope.toggle = function(modalstate, id){
        $scope.modalstate = modalstate;
        switch (modalstate){
            case 'add':
                $scope.form_title = 'Add new Page';
                break;
            case 'edit':
                $scope.form_title = 'Edit Page';
                $scope.id = id;
                $http.get(API_URL + 'pages/' + id)
                        .success(function(response){
                            $scope.page = response;
                        });
                break;
            default:
                break;
        }
        $('#pageModal').modal('show');
    }    

    //SEO photo start
    $scope.uploader = new FileUploader({url: '/up-seo'});

    $scope.uploader.onSuccessItem = function(item,result){
        var img = result;
        $scope.page.seo_image = img;
    };
    //SEO photo end

    //save new record / update existing record
    $scope.save = function(modalstate, id){
        var url = API_URL + 'pages';

        //append student id to te URL if the form is in edit mode
        if(modalstate === 'edit'){
            url += "/" + id;
        }

        console.log(JSON.stringify($scope.page));

        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.page),
            headers: {'Content-Type':'application/x-www-form-urlencoded'}
        }).success(function(response){
            console.log(response);
            location.reload();
        }).error(function(response){
            console.log(response);
            alert('This is embarassing. An error has occured. Please try again in few minutes');
        });
    }

    //delete record
    $scope.confirmDelete = function(id){
        var isConfirmDelete = confirm('Are you sure?');
        if(isConfirmDelete){
            $http({
                method: 'DELETE',
                url: API_URL + 'pages/' + id
            }).success(function(data){
               console.log(data);
                location.reload();
            }).error(function(data){
                console.log(data);
                alert('Unable to delete');
            });
        }
        else{
            return false;
        }
    }
});