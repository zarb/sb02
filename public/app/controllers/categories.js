app.controller('categoriesController',function($scope,$http, API_URL){
    //retrieve categories listing from API
    $http.get(API_URL + "categories")
        .success(function(response){
            $scope.categories = response;
        });

    //show modal form
    $scope.toggle = function(modalstate, id){
        $scope.modalstate = modalstate;
        switch (modalstate){
            case 'add':
                $scope.form_title = 'Add new Category';
                break;
            case 'edit':
                $scope.form_title = 'Edit Category';
                $scope.id = id;
                $http.get(API_URL + 'categories/' + id)
                        .success(function(response){
                            $scope.category = response;
                        });
                break;
            default:
                break;
        }
        $('#categoryModal').modal('show');
    }

    //save new record / update existing record
    $scope.save = function(modalstate, id){
        var url = API_URL + 'categories';

        //append student id to te URL if the form is in edit mode
        if(modalstate === 'edit'){
            url += "/" + id;
        }

        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.category),
            headers: {'Content-Type':'application/x-www-form-urlencoded'}
        }).success(function(response){
            console.log(response);
            location.reload();
        }).error(function(response){
            console.log(response);
            alert('This is embarassing. An error has occured. Please try again in few minutes');
        });
    }

    //delete record
    $scope.confirmDelete = function(id){
        var isConfirmDelete = confirm('Are you sure?');
        if(isConfirmDelete){
            $http({
                method: 'DELETE',
                url: API_URL + 'categories/' + id
            }).success(function(data){
               console.log(data);
                location.reload();
            }).error(function(data){
                console.log(data);
                alert('Unable to delete');
            });
        }
        else{
            return false;
        }
    }
});