$('#open-popup').magnificPopup({
    items: [
      {
        src: 'https://www.youtube.com/watch?v=U2UfQ_sUYi4',
        type: 'iframe' // this overrides default type
      },
      {
        src: 'img/1.jpeg',
        title: ''
      },
      {
        src: 'img/2.jpeg',
        title: ''
      },
      {
        src: 'img/3.jpeg',
        title: ''
      },
      {
        src: 'img/4.jpeg',
        title: ''
      },
    ],
    gallery: {
      enabled: true
    },
    type: 'image' // this is a default type
});