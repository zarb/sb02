<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectPhotos extends Model{

    protected $table = 'project_photos';

}