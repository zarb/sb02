<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryProject extends Model{

    protected $table = 'categories_projects';

}