<?php

namespace App\Http\Controllers;

use App\Category;
use App\Donation;
use App\Donator;
use App\Page;
use App\Project;
use App\ProjectPhotos;
use App\User;
use App\Student;
use App\Reward;
use App\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use MangoPay\MangoPayApi;

class FrontController extends Controller
{
    public function index()
    {
        $lang = isset($_GET['lang'])?$_GET['lang']:'en';
        app()->setLocale($lang);
        return view('front.home')->with('is_home',1);
    }

    public function allprojects($lang='en')
    {
        app()->setLocale($lang);

        $page = 'all-projects';
        $uri_path = 'http://www.studentbackr.com/' . $page. '/' . $lang;
        
        return view('front.allprojects',['uri_path'=>$uri_path]);
    }

    public function callcontent($page = null, $lang = 'en'){
        if(!$page)
            return redirect('/');

        if(isset($_GET['lang'])){
            $lang = $_GET['lang'];
        }
        $_GET['lang']=$lang;

        if($lang == 'fr'){
            if($page == 'about-us')
                return redirect('/a-propos-de-studentbackr/fr');
            else if($page == 'terms-and-conditions')
                return redirect('/conditions-generales-cgu/fr');
            else if($page == 'backers')
                return redirect('/les-backers/fr');
            else if($page == 'legal-notices')
                return redirect('/mentions-legales/fr');
            else if($page == 'budget-and-campaign-length')
                return redirect('/definir-budget-duree-de-sa-campagne/fr');
        }
        else if($lang == 'en' && $page == 'conditions-generales-cgu')
            return redirect('/terms-and-conditions');
        else if($lang == 'en' && $page == 'a-propos-de-studentbackr')
            return redirect('/about-us');
        else if($lang == 'en' && $page == 'mentions-legales')
            return redirect('/legal-notices');
        else if($lang == 'en' && $page == 'les-backers')
            return redirect('/backers');
        else if($lang == 'en' && $page == 'definir-budget-duree-de-sa-campagne')
            return redirect('/budget-and-campaign-length');

        if($page == 'how-it-works' && isset($_GET['lang'])){
            if($_GET['lang']=='fr') {
                $page = 'comment-ca-marche';
            }
        }

        app()->setLocale($lang);
        $content = Page::where('shortcut',$page)->first();

        $en_video = '<iframe width="560" height="315" src="https://www.youtube.com/embed/U2UfQ_sUYi4" frameborder="0" allowfullscreen></iframe>';
        $fr_video = '<iframe width="560" height="315" src="https://www.youtube.com/embed/TMOxh_FsjgU" frameborder="0" allowfullscreen></iframe>';

//        $content->content = str_replace('[EN_VIDEO]',$en_video,$content);
//        $content->content = str_replace('[FR_VIDEO]',$fr_video,$content);

        $uri_path = 'http://www.studentbackr.com/' . $page. '/' . $lang;
        
        return view('front.content',['content'=>$content,'page'=>$page,'lang'=>$lang,'uri_path'=>$uri_path]);
    }

    public function callregister($lang='en')
    {
        app()->setLocale($lang);
        return view('front.register');
    }

    public function callprofile($lang='')
    {
        $locale = session('sblocale');
        if($lang)
            app()->setLocale($lang);
        else if(isset($locale))
            app()->setLocale($locale);
        else
            app()->setLocale('en');


        if(isset($_GET['id']))
            session(['draft'=>$_GET['id']]);
        //old code
        //if(isset($_GET['id']))
            //$request->session()->put('draft',$_GET['id']);

        return view('front.profile');
    }

    public function calleditprofile()
    {
        return view('front.editprofile');
    }

    public function calldonate($id)
    {
        $locale = session('sblocale');
        if(isset($locale))
            app()->setLocale($locale);
        else
            app()->setLocale('en');

        $user = Auth::user();
        if($user)
            $logged_student = Student::where('id_user',$user->id)->first();
        else
            $logged_student = '';

        $project = Project::where('id',$id)->first();
        $student = Student::where('id',$project->id_student)->first();
        $rewards = Reward::where('id_project',$id)->orderBy('minimum')->get();

        //amount funded
        $donations = DB::table('donations')
            ->select(DB::raw('SUM(amount)/100 as total'))
            ->groupBy('status')
            ->where('status','S')
            ->where('id_project',$project->id)
            ->first();

        if($donations) {
            $project->received = $donations->total;
            $project->percent = ceil(($donations->total * 100)/$project->target);
        }else {
            $project->received = $project->percent = 0;
        }

        $project->small_story = substr(strip_tags($project->story),0,80);
        $project->small_story = preg_replace( "/\r|\n/", "", $project->small_story);
        $project->small_story = htmlspecialchars($project->small_story);

        $today = date('Y-m-d');
        $today2 = date('Ymd');
        $end2 = str_replace('-','',$project->end_date);
        $datetime1 = date_create($today);
        $datetime2 = date_create($project->end_date);
        if($today2 > $end2)
            $project->days = '0';
        else {
            $interval = date_diff($datetime1, $datetime2);
            $project->days = $interval->format('%a');
        }

        return view('front.donation',['project'=>$project,'student'=>$student,'rewards'=>$rewards,'lstudent'=>$logged_student]);
    }

    public function contact($lang='en'){

        app()->setLocale($lang);
        
        $page = 'contact';
        $uri_path = 'http://www.studentbackr.com/' . $page. '/' . $lang;

        return view('front.contact',['uri_path'=>$uri_path]);
    }

    public function callproject($id){

        if(session('sblocale')){
            app()->setLocale(session('sblocale'));
            $lang = session('sblocale');
        }else if(isset($_GET['lang'])){
            app()->setLocale($_GET['lang']);
            $lang = $_GET['lang'];
        }
        else
            $lang = 'en';

        $user = Auth::user();
        if($user)
            $logged_student = Student::where('id_user',$user->id)->first();
        else
            $logged_student = '';

        $project = Project::where('id',$id)->first();
        $student = Student::where('id',$project->id_student)->first();
        $rewards = Reward::where('id_project',$id)->orderBy('minimum')->get();

        $donations = DB::table('donations')
            ->select(DB::raw('SUM(amount)/100 as total'))
            ->groupBy('status')
            ->where('status','S')
            ->where('id_project',$project->id)
            ->first();

        $project->donations = $donations;
        if($donations) {
            $project->received = $donations->total;
            $project->percent = ceil(($donations->total * 100)/$project->target);
        }else {
            $project->received = $project->percent = 0;
        }

        $project->small_story = substr(strip_tags($project->story),0,80);
        $project->small_story = preg_replace( "/\r|\n/", "", $project->small_story);
        $project->small_story = htmlspecialchars($project->small_story);

        $today = date('Y-m-d');
        $today2 = date('Ymd');
        $end2 = str_replace('-','',$project->end_date);
        $datetime1 = date_create($today);
        $datetime2 = date_create($project->end_date);
        if($today2 > $end2)
            $project->days = '0';
        else {
            $interval = date_diff($datetime1, $datetime2);
            $project->days = $interval->format('%a');
        }

        //getting project photos
        $proj_photos = ProjectPhotos::where('id_project',$project->id)->get();

        //studentbackr old photo
        //$pos = strpos($student->photo,'wp-content');
        //$url_photo = 'http://studentbackr.com/'.substr($student->photo,$pos);
        //print_r($student);
        //return view('front.project',['project'=>$project,'student'=>$student,'url_photo'=>$url_photo,'rewards'=>$rewards]);

        $page = 'sponsorship-programme';
        $uri_path = 'http://www.studentbackr.com/project/'.$project->id.'/?lang='.$lang;

        return view('front.project',['project'=>$project,'student'=>$student,'rewards'=>$rewards,'user'=>$user,'logged_student'=>$logged_student,'proj_photos'=>$proj_photos,'page'=>'project','uri_path'=>$uri_path]);
    }

    public function callpayinfo($id, Request $request){

        $project = Project::where('id',$id)->first();
        $reward = Reward::where('id',$_POST['reward'])->first();
        $student = Student::where('id',$project->id_student)->first();

        require_once(app_path().'/Mangopay/Autoloader.php');
        require_once(app_path().'/Mangopay/base-mangopay.php');

        $api = new MangoPayApi();
        $api->Config->ClientId = CLIENT_ID;
        $api->Config->ClientPassword = CLIENT_PASS;
        $api->Config->TemporaryFolder = TMP_FOLDER;
        $api->Config->BaseUrl = BASE_URL;

        // create user for payment
        $usermp = new \MangoPay\UserNatural();
        $usermp->FirstName = $_POST['name'];
        $usermp->LastName = $_POST['lastname'];
        $usermp->Email = $_POST['email'];
        //$user->Birthday = $_POST['dob'];
        $usermp->Birthday = 121271;
        $usermp->Nationality = $_POST['country'];
        $usermp->CountryOfResidence = $_POST['country'];
        //$user->Occupation = "programmer";
        //$user->IncomeRange = 3;
        $createdUser = $api->Users->Create($usermp);

        // register card
        $cardRegister = new \MangoPay\CardRegistration();
        $cardRegister->UserId = $createdUser->Id;
        $cardRegister->Currency = $project->currency;
        $cardRegister->CardType = $_POST['carType'];
        $createdCardRegister = $api->CardRegistrations->Create($cardRegister);
        $request->session()->put('cardRegisterId',$createdCardRegister->Id);
        //$cardRegisterId = $createdCardRegister->Id;

        // build the return URL to capture token response
        $returnUrl = 'http' . ( isset($_SERVER['HTTPS']) ? 's' : '' ) . '://' . $_SERVER['HTTP_HOST'];
        //$returnUrl .= substr($_SERVER['REQUEST_URI'], 0, strripos($_SERVER['REQUEST_URI'], '/') + 1);
        $returnUrl .= '/payment';

        //registering sessions
        $request->session()->put('reward_id',$_POST['reward']);
        $request->session()->put('student_name',$student->first_name . ' ' . $student->last_name);
        $request->session()->put('project_title',$project->title);
        $request->session()->put('project_id',$project->id);
        $request->session()->put('normal_amount',$_POST['amount']);
        $request->session()->put('amount',number_format($_POST['amount'],2) * 100);
        $request->session()->put('currency',$project->currency);
        $request->session()->put('cardType',$_POST['carType']);
        $request->session()->put('message',$_REQUEST['message']);
        $request->session()->put('name',$_POST['name'] . ' ' . $_POST['lastname']);
        $request->session()->put('first_name',$_POST['name']);
        $request->session()->put('last_name',$_POST['lastname']);
        $request->session()->put('email',$_POST['email']);
        $request->session()->put('country',$_POST['country']);
        $request->session()->put('wallet',$project->mp_wallet_id);
        $request->session()->put('comment',$_POST['message']);
        $request->session()->put('website','');

        return view('front.payinfo',['project'=>$project,'reward'=>$reward,'createdCardRegister'=>$createdCardRegister,'returnUrl'=>$returnUrl]);
    }

    public function callpayment(Request $request){
        require_once(app_path().'/Mangopay/Autoloader.php');
        require_once(app_path().'/Mangopay/base-mangopay.php');

        $api = new MangoPayApi();
        $api->Config->ClientId = CLIENT_ID;
        $api->Config->ClientPassword = CLIENT_PASS;
        $api->Config->TemporaryFolder = TMP_FOLDER;
        $api->Config->BaseUrl = BASE_URL;

        if(isset($_GET['3Dsuccess'])) { //3D secure payment

            $payinId = $_GET['transactionId'];
            $result = $api->PayIns->Get($payinId);

            $mp_donator = $api->Users->GetNatural($result->AuthorId);

            if($result->Status == 'SUCCEEDED') {

                $msg = 'Your payment has been processed successfully.';

                //INSERT DONATOR HERE
                $donator = new Donator();
                $donator->first_name = $mp_donator->FirstName;
                $donator->last_name = $mp_donator->LastName;
                $donator->email = $mp_donator->Email;
                $donator->id_country = 1;
                $donator->save();

                //INSERT DONATION HERE
                $payin_info = explode('#',$result->Tag);
                $donation = new Donation();
                $donation->id_project = $payin_info[0];
                $donation->id_reward = $payin_info[1];
                $donation->id_donator = $donator->id;
                $donation->date = date("Y-m-d h:i");
                $donation->amount = $result->DebitedFunds->Amount;
                $donation->currency = $result->DebitedFunds->Currency;
                $donation->comment = $payin_info[2];
                $donation->mp_id = $result->Id;
                $donation->status = 'S';
                $donation->save();                
                
                //get notification template
                $locale = session('sblocale');
                if(!$locale)
                     $locale = 'EN';
                
                $notification = Notification::where('shortcut','payment-successful')->where('language',$locale)->first();

                if($notification){
                    //replace user name to the student name
                    $donator_name = $mp_donator->FirstName.' '.$mp_donator->LastName;
                    $donator_email = $mp_donator->Email;
                    $content = str_replace('[USER_NAME]',$donator_name,$notification->content);

                    
                    //send notification
                    Mail::send('templates/notification',['content'=>$content],function($m) use ($donator_name,$donator_email,$notification){
                        $m->from('contact@studentbackr.com','Student Backr');
                        $m->to($donator_email,$donator_name)->subject($notification->name);
                    });
                }                

                // clear data in session to protect against double processing
                $request->session()->flush();

                return view('front.payment',['msg'=>$msg]);
            }else{
                $msg = 'Your payment with 3d Secure was not validated.';

                //INSERT DONATOR HERE
                $donator = new Donator();
                $donator->first_name = $mp_donator->FirstName;
                $donator->last_name = $mp_donator->LastName;
                $donator->email = $mp_donator->Email;
                $donator->id_country = 1;
                $donator->save();

                //INSERT DONATION HERE
                $payin_info = explode('#',$result->Tag);
                $donation = new Donation();
                $donation->id_project = $payin_info[0];
                $donation->id_reward = $payin_info[1];
                $donation->id_donator = $donator->id;
                $donation->date = date("Y-m-d h:i");
                $donation->amount = $result->DebitedFunds->Amount;
                $donation->currency = $result->DebitedFunds->Currency;
                $donation->comment = $payin_info[2];
                $donation->mp_id = $result->Id;
                $donation->status = 'F';
                $donation->save();

                //get notification template
                $locale = session('sblocale');
                if(!$locale)
                     $locale = 'EN';
                
                $notification = Notification::where('shortcut','payment-failed')->where('language',$locale)->first();

                if($notification){
                    //replace user name to the student name
                    $donator_name = $mp_donator->FirstName.' '.$mp_donator->LastName;
                    $donator_email = $mp_donator->Email;
                    $content = str_replace('[USER_NAME]',$donator_name,$notification->content);
                    
                    //send notification
                    Mail::send('templates/notification',['content'=>$content],function($m) use ($donator_name,$donator_email,$notification){
                        $m->from('contact@studentbackr.com','Student Backr');
                        $m->to($donator_email,$donator_name)->subject($notification->name);
                    });
                }

                // clear data in session to protect against double processing
                $request->session()->flush();


                return view('front.payment',['msg'=>$msg]);
            }
        }
        else{

            $msg = '-';

            try {
                $cardRegister = $api->CardRegistrations->Get($request->session()->get('cardRegisterId'));
                $cardRegister->RegistrationData = isset($_GET['data']) ? 'data=' . $_GET['data'] : 'errorCode=' . $_GET['errorCode'];
                $updatedCardRegister = $api->CardRegistrations->Update($cardRegister);

                if ($updatedCardRegister->Status != \MangoPay\CardRegistrationStatus::Validated || !isset($updatedCardRegister->CardId))
                    die('<div style="color:red;">Cannot create card. Payment has not been created.<div>');

                $card = $api->Cards->Get($updatedCardRegister->CardId);

                // create pay-in CARD DIRECT
                $payIn = new \MangoPay\PayIn();
                //$payIn->CreditedWalletId = $createdWallet->Id;
                $payIn->CreditedWalletId = $request->session()->get('wallet');
                $payIn->AuthorId = $updatedCardRegister->UserId;
                $payIn->DebitedFunds = new \MangoPay\Money();
                $payIn->DebitedFunds->Amount = $request->session()->get('amount');
                $payIn->DebitedFunds->Currency = $request->session()->get('currency');
                $payIn->Fees = new \MangoPay\Money();
                $payIn->Fees->Amount = 0;
                $payIn->Fees->Currency = $request->session()->get('currency');
                $payIn->Tag = $request->session()->get('project_id').'#'.$request->session()->get('reward_id').'#'.$request->session()->get('comment');

                // payment type as CARD
                $payIn->PaymentDetails = new \MangoPay\PayInPaymentDetailsCard();
                $payIn->PaymentDetails->CardType = $card->CardType;

                // execution type as DIRECT
                $payIn->ExecutionDetails = new \MangoPay\PayInExecutionDetailsDirect();
                $payIn->ExecutionDetails->CardId = $card->Id;
                if($request->session()->get('amount') > 5000)
                    $payIn->ExecutionDetails->SecureMode = 'FORCE';
                
                //$payIn->ExecutionDetails->SecureModeReturnURL = "http" . (isset($_SERVER['HTTPS']) ? "s" : null) . "://" . $_SERVER["HTTP_HOST"] . $_SERVER["SCRIPT_NAME"] . "?3Dsuccess=1";
                $payIn->ExecutionDetails->SecureModeReturnURL = "http" . (isset($_SERVER['HTTPS']) ? "s" : null) . "://" . $_SERVER["HTTP_HOST"] . '/payment/' . "?3Dsuccess=1";

                // create Pay-In
                $createdPayIn = $api->PayIns->Create($payIn);

                if ($createdPayIn->Status == \MangoPay\PayInStatus::Succeeded) {
                    $msg = 'Your payment has been processed successfully.';

                    //INSERT DONATOR HERE
                    $donator = new Donator();
                    $donator->first_name = $request->session()->get('first_name');
                    $donator->last_name = $request->session()->get('last_name');
                    $donator->email = $request->session()->get('email');
                    $donator->id_country = $request->session()->get('country');
                    $donator->save();

                    //INSERT DONATION HERE
                    $donation = new Donation();
                    $donation->id_project = $request->session()->get('project_id');
                    $donation->id_reward = $request->session()->get('reward_id');
                    $donation->id_donator = $donator->id;
                    $donation->date = date("Y-m-d h:i");
                    $donation->amount = $request->session()->get('amount');
                    $donation->currency = $request->session()->get('currency');
                    $donation->comment = $request->session()->get('comment');
                    $donation->mp_id = $createdPayIn->Id;
                    $donation->status = 'S';
                    $donation->save();

                    $locale = session('sblocale');
                    if(!$locale)
                        $locale = 'EN';

                    //get notification template
                    $notification = Notification::where('shortcut','payment-successful')->where('language',$locale)->first();

                    //replace user name to the student name
                    $donator_name = $request->session()->get('first_name').' '.$request->session()->get('last_name');
                    $donator_email = $request->session()->get('email');
                    $content = str_replace('[USER_NAME]',$donator_name,$notification->content);

                    //send notification
                    Mail::send('templates/notification',['content'=>$content],function($m) use ($donator_name,$donator_email,$notification){
                        $m->from('contact@studentbackr.com','Student Backr');
                        $m->to($donator_email,$donator_name)->subject($notification->name);
                    });

                    $request->session()->flush();
                }
                else if ($createdPayIn->ExecutionDetails->SecureModeNeeded && $createdPayIn->Status != \MangoPay\PayInStatus::Failed) {
                    $request->session()->flush();

                    header("Location: ". $createdPayIn->ExecutionDetails->SecureModeRedirectURL);

                }
                else{
                    //creating failed donation
                    //INSERT DONATOR HERE
                    $donator = new Donator();
                    $donator->first_name = $request->session()->get('first_name');
                    $donator->last_name = $request->session()->get('last_name');
                    $donator->email = $request->session()->get('email');
                    $donator->id_country = $request->session()->get('country');
                    $donator->save();

                    //INSERT DONATION HERE
                    $donation = new Donation();
                    $donation->id_project = $request->session()->get('project_id');
                    $donation->id_reward = $request->session()->get('reward_id');
                    $donation->id_donator = $donator->id;
                    $donation->date = date("Y-m-d h:i");
                    $donation->amount = $request->session()->get('amount');
                    $donation->currency = $request->session()->get('currency');
                    $donation->comment = $request->session()->get('comment');
                    $donation->mp_id = $createdPayIn->Id;
                    $donation->status = 'F';
                    $donation->save();

                    $msg = 'Your payment was not validated. Please go back to the project page and try again.<br>You can use another mean of payment if you have issues with your credit card.';

                    $locale = session('sblocale');
                    if(!$locale)
                        $locale = 'EN';

                    //get notification template
                    $notification = Notification::where('shortcut','payment-failed')->where('language',$locale)->first();

                    //replace user name to the student name
                    $donator_name = $request->session()->get('first_name').' '.$request->session()->get('last_name');
                    $donator_email = $request->session()->get('email');
                    $content = str_replace('[USER_NAME]',$donator_name,$notification->content);

                    //send notification
                    Mail::send('templates/notification',['content'=>$content],function($m) use ($donator_name,$donator_email,$notification){
                        $m->from('contact@studentbackr.com','Student Backr');
                        $m->to($donator_email,$donator_name)->subject($notification->name);
                    });

                    $request->session()->flush();

                }

                return view('front.payment',['msg'=>$msg]);
            }
            catch (\MangoPay\Libraries\ResponseException $e) {

                echo 'ERROR: '
                    . '\MangoPay\ResponseException: Code: '
                    . $e->getCode() . '<br/>Message: ' . $e->getMessage()
                    . '<br/><br/>Details: ';
                print_r($e->GetErrorDetails());

                // clear data in session to protect against double processing
                //Session::flush();
            }
        }
    }

    public function callwizard($id = null)
    {
        $locale = session('sblocale');
        if(isset($_GET['lang']))
            app()->setLocale($_GET['lang']);
        else if(isset($locale))
            app()->setLocale($locale);

        $user_info = Auth::user();
        $categories = Category::all();
        $student = Student::where('id_user',$user_info->id)->first();
        if($id) {
            $project = Project::where('id', $id)->first();
            if($project->status == 'D')
                return view('front.wizard',['user_info'=>$user_info,'categories'=>$categories,'project'=>$project,'student'=>$student,'is_wizard'=>1]);
            else
                return redirect('/project/'.$id);
        }
        else{
            return view('front.wizard',['user_info'=>$user_info,'categories'=>$categories,'student'=>$student,'is_wizard'=>1]);
        }
    }

    public function callonboard($lang='')
    {
        $locale = session('sblocale');
        if($lang)
            app()->setLocale($lang);
        else if(isset($locale))
            app()->setLocale($locale);

        $user = Auth::user();
        if($user) {
            $logged_student = Student::where('id_user', $user->id)->first();
            return view('front.onboard',['student'=>$logged_student]);
        }
        else
            return redirect('/');
    }

    public function register(Request $request){
        $locale = session('sblocale');
        if(!isset($locale))
            $locale = 'EN';

        app()->setLocale($locale);

        $msg = '';
        if($request->input('signupPassword') != $request->input('signupPasswordagain')){
            $msg = 'The passwords you entered do not match.';
            return view('front.register')->with('msg',$msg)->with('fields',$_POST);
        }
        else{
            $s_user = new User();
            $s_user->name = '';
            $s_user->email = $request->input('signupEmail');
            $s_user->password = Hash::make($request->input('signupPassword'));
            $s_user->rule = 'S';
            $s_user->active = 'Y';
            $s_user->save();
            #END USER CREATION

            #CREATE STUDENT TO ADD IN THE PROJECT
            $student = new Student();
            $student->id_user = $s_user->id;
            $student->email = $request->input('signupEmail');
            $student->language = $locale;
            $student->save();

            Auth::attempt(['email' => $request->input('signupEmail'), 'password' => $request->input('signupPassword')]);

            return redirect('/onboard?new=1');
        }
    }


}
