<?php

namespace App\Http\Controllers;

use App\Country;
use App\Student;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use MangoPay\MangoPayApi;


class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        if($id == null)
            return Student::orderBy('id','asc')->get();
        else
            return $this->show($id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $student = new Student;

        $student->first_name = $request->input('first_name');
        $student->last_name = $request->input('last_name');
        $student->dob = $request->input('dob');
        $student->gender = $request->input('gender');
        $student->email = $request->input('email');
        $student->id_country = $request->input('id_country');
        $student->address = $request->input('address');
        $student->city = $request->input('city');
        $student->phone = $request->input('phone');
        $student->annual_income = $request->input('annual_income');
        $student->biographical_inf = $request->input('biographical_inf');

        $student->save();

        return 'Student successfully created';
    }

    public function logged(){
        $user = Auth::user();
        $student = Student::where('id_user',$user->id)->first();

        return $student;
    }

    public function newmpbank($id,$type){
        $student = Student::find($id);

        require_once(app_path().'/Mangopay/Autoloader.php');
        require_once(app_path().'/Mangopay/base-mangopay.php');

        $api = new MangoPayApi();
        $api->Config->ClientId = CLIENT_ID;
        $api->Config->ClientPassword = CLIENT_PASS;
        $api->Config->TemporaryFolder = TMP_FOLDER;
        $api->Config->BaseUrl = BASE_URL;

        if($type == 'EURO'){
            $iban = str_replace(' ','',$student->iban);

            $country = Country::where('id',$student->id_country)->first();

            //checking reqired fields
            if(!$student->iban || !$student->bic || !$student->first_name || !$student->last_name || !$student->address)
                return false;
            else {
                try {
                    $BankAccount = new \MangoPay\BankAccount();
                    $BankAccount->Type = "IBAN";
                    $BankAccount->Details = new \MangoPay\BankAccountDetailsIBAN();
                    $BankAccount->Details->IBAN = $iban; //new code, use iban without white space
                    if ($student->bic)
                        $BankAccount->Details->BIC = $student->bic;
                    $BankAccount->OwnerName = $student->first_name . ' ' . $student->last_name;
                    $BankAccount->OwnerAddress = new \MangoPay\Address();
                    $BankAccount->OwnerAddress->AddressLine1 = $student->address;
                    $BankAccount->OwnerAddress->City = $student->city;
                    $BankAccount->OwnerAddress->Country = $country->iso;
                    $BankAccount->OwnerAddress->PostalCode = $student->postal_code;
                    $BankAccount->OwnerAddress->Region = 'Region';

                    $result = $api->Users->CreateBankAccount($student->mp_user_id, $BankAccount);

                    $student->mp_bank_id = $result->Id;
                    $student->save();

                    $locale = $student->language;
                    if(!$locale)
                        $locale = 'EN';

                    //get notification template
                    $notification = Notification::where('shortcut', 'you-can-now-submit')->where('language',$locale)->first();

                    //replace user name to the student name
                    $student_name = $student->first_name . ' ' . $student->last_name;
                    $content = str_replace('[USER_NAME]', $student_name, $notification->content);

                    //send notification
                    Mail::send('templates/notification', ['content' => $content], function ($m) use ($student, $student_name, $notification) {
                        $m->from('contact@studentbackr.com', 'Student Backr');
                        $m->to($student->email, $student_name)->subject($notification->name);
                    });
                } catch (\MangoPay\Libraries\Exception $e) {
                    $bankinfo = print_r($BankAccount, true);
                    echo "Error: Invalid Iban data " . $student->mp_user_id . " -- " . $bankinfo . " --" . $e;
                }
            }
        }
        elseif($type == 'POUND'){
            //checking reqired fields
            if(!$student->sort_code || !$student->account_number || !$student->first_name || !$student->last_name || !$student->address)
                return false;
            else {
                try {
                    $BankAccount = new \MangoPay\BankAccount();
                    $BankAccount->Type = "GB";
                    $BankAccount->Details = new \MangoPay\BankAccountDetailsGB();
                    $BankAccount->Details->AccountNumber = $student->account_number;
                    $BankAccount->Details->SortCode = $student->sort_code;
                    $BankAccount->OwnerName = $student->first_name . ' ' . $student->last_name;
                    $BankAccount->OwnerAddress = $student->address;
                    $result = $api->Users->CreateBankAccount($student->mp_user_id, $BankAccount);

                    $student->mp_bank_id = $result->Id;
                    $student->save();
                } catch (\MangoPay\Libraries\Exception $e) {
                    $bankinfo = print_r($BankAccount, true);
                    echo "Error: Invalid GB data " . $student->mp_user_id . " -- " . $bankinfo . " --" . $e;
                }
            }
        }
        elseif($type == 'USD'){
            //checking reqired fields
            if(!$student->aba || !$student->account_number || !$student->first_name || !$student->last_name || !$student->address)
                return false;
            else {
                try {
                    $BankAccount = new \MangoPay\BankAccount();
                    $BankAccount->Type = "US";
                    $BankAccount->Details = new \MangoPay\BankAccountDetailsUS();
                    $BankAccount->Details->AccountNumber = $student->account_number;
                    $BankAccount->Details->ABA = $student->aba;
                    $BankAccount->OwnerName = $student->first_name . ' ' . $student->last_name;
                    $BankAccount->OwnerAddress = $student->address;
                    $result = $api->Users->CreateBankAccount($student->mp_user_id, $BankAccount);

                    $student->mp_bank_id = $result->Id;
                    $student->save();
                } catch (\MangoPay\Libraries\Exception $e) {
                    $bankinfo = print_r($BankAccount, true);
                    echo "Error: Invalid USD data " . $student->mp_user_id . " -- " . $bankinfo . " --" . $e;
                }
            }
        }
        elseif($type == 'CAD'){
            //checking reqired fields
            if(!$student->account_number || !$student->bank_name || !$student->institution_number || !$student->branch_code || !$student->first_name || !$student->last_name || !$student->address)
                return false;
            else {
                try {
                    $BankAccount = new \MangoPay\BankAccount();
                    $BankAccount->Type = "CA";
                    $BankAccount->Details = new \MangoPay\BankAccountDetailsCA();
                    $BankAccount->Details->AccountNumber = $student->account_number;
                    $BankAccount->Details->BankName = $student->bank_name;
                    $BankAccount->Details->InstitutionNumber = $student->institution_number;
                    $BankAccount->Details->BranchCode = $student->branch_code;
                    $BankAccount->OwnerName = $student->first_name . ' ' . $student->last_name;
                    $BankAccount->OwnerAddress = $student->address;
                    $result = $api->Users->CreateBankAccount($student->mp_user_id, $BankAccount);

                    $student->mp_bank_id = $result->Id;
                    $student->save();
                } catch (\MangoPay\Libraries\Exception $e) {
                    $bankinfo = print_r($BankAccount, true);
                    echo "Error: Invalid CAD data " . $student->mp_user_id . " -- " . $bankinfo . " --" . $e;
                }
            }
        }
        elseif($type == 'CHF'){
            //checking reqired fields
            if(!$student->country || !$student->bic || !$student->account_number || !$student->first_name || !$student->last_name || !$student->address)
                return false;
            else {
                try {
                    $BankAccount = new \MangoPay\BankAccount();
                    $BankAccount->Type = "CA";
                    $BankAccount->Details = new \MangoPay\BankAccountDetailsOTHER();
                    $BankAccount->Details->Type = 'OTHER';
                    $BankAccount->Details->Country = $student->country;
                    $BankAccount->Details->BIC = $student->bic;
                    $BankAccount->Details->AccountNumber = $student->account_number;
                    $BankAccount->OwnerName = $student->first_name . ' ' . $student->last_name;
                    $BankAccount->OwnerAddress = $student->address;
                    $result = $api->Users->CreateBankAccount($student->mp_user_id, $BankAccount);

                    $student->mp_bank_id = $result->Id;
                    $student->save();
                } catch (\MangoPay\Libraries\Exception $e) {
                    $bankinfo = print_r($BankAccount, true);
                    echo "Error: Invalid CHF data " . $student->mp_user_id . " -- " . $bankinfo . " --" . $e;
                }
            }
        }

        if($result)
            return $result->Id;
    }

    public function newmpuser($id){

        $student = Student::find($id);

        require_once(app_path().'/Mangopay/Autoloader.php');
        require_once(app_path().'/Mangopay/base-mangopay.php');

        $api = new MangoPayApi();
        $api->Config->ClientId = CLIENT_ID;
        $api->Config->ClientPassword = CLIENT_PASS;
        $api->Config->TemporaryFolder = TMP_FOLDER;
        $api->Config->BaseUrl = BASE_URL;

        $naturalUser = new \MangoPay\UserNatural();
        $naturalUser->Email = $student->email;
        $naturalUser->FirstName = $student->first_name;
        $naturalUser->LastName = $student->last_name;
        $naturalUser->Birthday = strtotime($student->dob);
        $naturalUser->Nationality = $student->id_country;
        $naturalUser->CountryOfResidence = $student->id_country;
        //$naturalUser->Occupation = 'undefined';
        //$naturalUser->Address = $student->address;
        //$naturalUser->IncomeRange = $student->annual_income;

        if($naturalUserResult = $api->Users->Create($naturalUser)) {
            $student->mp_type = $naturalUserResult->PersonType;
            $student->mp_user_id = $naturalUserResult->Id;
            $student->save();

            if($student->currency)
                $this->newmpbank($student->id,$student->currency);

            echo "Created successfully";
        }else
            echo "Failed to create MP User";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Student::find($id);

        return $student;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $r = false;

        $student = Student::find($id);
        $student->first_name = $request->input('first_name');
        $student->last_name = $request->input('last_name');
        $student->dob = $request->input('dob');
        $student->gender = $request->input('gender');
        $student->email = $request->input('email');
        $student->id_country = $request->input('id_country');
        $student->address = $request->input('address');
        $student->city = $request->input('city');
        $student->phone = $request->input('phone');
        $student->annual_income = $request->input('annual_income');
        $student->biographical_inf = $request->input('biographical_inf');
        $student->currency = $request->input('currency');
        $student->iban = $request->input('iban');
        $student->bic = $request->input('bic');
        $student->account_number = $request->input('account_number');
        $student->sort_code = $request->input('sort_code');
        $student->aba = $request->input('aba');
        $student->branch_code = $request->input('branch_code');
        $student->bank_name = $request->input('bank_name');
        $student->institution_number = $request->input('institution_number');
        $student->postal_code = $request->input('postal_code');

        if($student->save())
            $r = 1;

        //r values:
        //false = error
        //1 = only sb saved
        //12 = sb saved and mp user created
        //123 = sb saved, mp user created and bank account created

        //create mangopay user
        if($student->first_name && $student->last_name && $student->dob && $student->id_country && !$student->mp_user_id){
            if($this->newmpuser($student->id)){
                $r.= 2;
                if(!$student->mp_bank_id && $request->input('currency')) { //create mp bank id | optional
                    if ($this->newmpbank($student->id, $student->currency))
                        $r .= 3;
                }
            }
        }

        return $r;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $student = Student::find($request->input('id'));
        $student->delete();

        return 'Student successfully deleted';
    }

    public function countries(){
        $countries = Country::all();
        echo $countries;
    }
}
