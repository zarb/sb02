<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;

use App\Http\Requests;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        if($id == null)
            return Notification::orderBy('id','asc')->get();
        else
            return $this->show($id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $notification = new Notification();
        $notification->name = $request->input('name');
        $notification->content = $request->input('content');
        $notification->language = $request->input('language');
        $notification->shortcut = $request->input('shortcut');

        $notification->save();

        return 'Notification successfully created';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $notification = Notification::find($id);

        return $notification;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $notification = Notification::find($id);
        $notification->name = $request->input('name');
        $notification->content = $request->input('content');
        $notification->language = $request->input('language');
        $notification->shortcut = $request->input('shortcut');

        $notification->save();

        return 'Notification successfully updated';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $notification = Notification::find($request->input('id'));
        $notification->delete();

        return 'Notification successfully deleted';
    }

}
