<?php

namespace App\Http\Controllers;

use App\Category;
use App\CategoryProject;
use App\Country;
use App\Donation;
use App\Donator;
use App\Log;
use App\Notification;
use App\Project;
use App\ProjectPhotos;
use App\Reward;
use App\Student;
use App\Updates;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use League\Flysystem\Exception;
use MangoPay\MangoPayApi;

class ProjectController extends Controller
{
    protected $langs = ['en','fr'];

    public function __construct()
    {
        //$this->middleware('auth');
    }
    /*
    public function __construct()
    {
        $this->middleware('auth');
    }
    */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        if($id == null) {
            $projects = Project::where('active','Y')->where('status','PU')->orderBy('created_at', 'desc')->get();
            $_project = $_project2 = [];

            foreach($projects AS $project){
                $student = Student::find($project->id_student);
                $project->student = $student;

                //amount funded
                $donations = DB::table('donations')
                    ->select(DB::raw('SUM(amount)/100 as total'))
                    ->groupBy('status')
                    ->where('status','S')
                    ->where('id_project',$project->id)
                    ->first();

                if($donations) {
                    $project->received = $donations->total;
                    $project->percent = ceil(($donations->total * 100)/$project->target);
                }else {
                    $project->received = $project->percent = 0;
                }

                if(!$project->resume)
                    $project->small_story = substr(strip_tags($project->story),0,80);
                else
                    $project->small_story = substr(strip_tags($project->resume),0,80);
                $project->small_story = preg_replace( "/\r|\n/", "", $project->small_story);
                $project->small_story = htmlspecialchars($project->small_story);

                $project->end_date = str_replace('-','/',$project->end_date);

                $d1 = strtotime(date('Y-m-d'));
                $d2 = strtotime($project->end_date);

                $dif = $d2 - $d1;

                if($dif < 0)
                    $_project2[] = $project;
                else{
                    if(array_key_exists($dif,$_project))
                        $_project[$dif + 1] = $project;    
                    else
                        $_project[$dif] = $project;
                }
            }

            ksort($_project);
            $_project = array_reverse($_project);

            $_project = $_project + $_project2;

            //echo '<pre>';
            //print_r($_project);
            header('Content-Type: application/json');
            echo json_encode($_project);
        }else{
            $project = Project::find($id);
            $student = Student::find($project->id_student);
            $project->student = $student;

            $donations = DB::table('donations')
                ->select(DB::raw('SUM(amount)/100 as total'))
                ->groupBy('status')
                ->where('status','S')
                ->where('id_project',$project->id)
                ->first();

            if($donations) {
                $project->received = $donations->total;
                $project->percent = ceil(($donations->total * 100)/$project->target);
            }else {
                $project->received = $project->percent = 0;
            }

            $cat = CategoryProject::where('id_project',$id)->first();
            if($cat)
                $project->id_category = $cat->id_category;
            else
                $project->id_category = '';

            //get project photos
            $photos = ProjectPhotos::where('id_project',$id)->get();
            if($photos)
                $project->photos = $photos;
            else
                $project->photos = '';

            $project->small_story = substr(strip_tags($project->story),0,80);
            $project->small_story = preg_replace( "/\r|\n/", "", $project->small_story);
            $project->small_story = htmlspecialchars($project->small_story);

            $project->end_date = str_replace('-','/',$project->end_date);

            return $project;
        }
    }

    public function mostUrgent(){
        $locale = session('sblocale');
        if(!isset($locale) || !in_array($locale,$this->langs))
            $locale = 'en';

        $number = 4; //number of projects

        $projects = Project::where('end_date','>=',date('Y-m-d'))
                    ->where('language','fr')
                    ->where('status','PU')
                    ->skip(0)->take(4)
                    ->orderBY('end_date','asc')->get();

        $pend = $number - $projects->count();

        if($pend > 0){
            $projects2 = Project::where('end_date','>=',date('Y-m-d'))
                ->where('language','en')
                ->where('status','PU')
                ->skip(0)->take($pend)
                ->orderBY('end_date','asc')->get();

            $projects->merge($projects2);
        }

        $_project = [];

        foreach($projects AS $project){
            $student = Student::find($project->id_student);
            $project->student = $student;

            //amount funded
            $donations = DB::table('donations')
                ->select(DB::raw('SUM(amount)/100 as total'))
                ->groupBy('status')
                ->where('status','S')
                ->where('id_project',$project->id)
                ->first();

            if($donations) {
                $project->received = $donations->total;
                $project->percent = ceil(($donations->total * 100)/$project->target);
            }else {
                $project->received = $project->percent = 0;
            }

            if(!$project->resume)
                $project->small_story = substr(strip_tags($project->story),0,80);
            else
                $project->small_story = substr(strip_tags($project->resume),0,80);
            $project->small_story = preg_replace( "/\r|\n/", "", $project->small_story);
            $project->small_story = htmlspecialchars($project->small_story);

            $d1 = strtotime(date('Y-m-d'));
            $d2 = strtotime($project->end_date);

            $dif = $d1 - $d2;


            if(array_key_exists($dif,$_project))
                $_project[$dif + 1] = $project;
            else
                $_project[$dif] = $project;
        }

        krsort($_project);


        //echo '<pre>';
        //print_r($_project);
        header('Content-Type: application/json');
        echo json_encode($_project);
    }

    public function mostRecent(){
        $locale = session('sblocale');
        if(!isset($locale) || !in_array($locale,$this->langs))
            $locale = 'en';

        $projects = Project::skip(0)->take(4)
                    ->where('status','PU')
                    ->orderBY('start_date','desc')->get();

        $_project = [];

        foreach($projects AS $project){
            $student = Student::find($project->id_student);
            $project->student = $student;

            //amount funded
            $donations = DB::table('donations')
                ->select(DB::raw('SUM(amount)/100 as total'))
                ->groupBy('status')
                ->where('status','S')
                ->where('id_project',$project->id)
                ->first();

            if($donations) {
                $project->received = $donations->total;
                $project->percent = ceil(($donations->total * 100)/$project->target);
            }else {
                $project->received = $project->percent = 0;
            }

            if(!$project->resume)
                $project->small_story = substr(strip_tags($project->story),0,80);
            else
                $project->small_story = substr(strip_tags($project->resume),0,80);
            $project->small_story = preg_replace( "/\r|\n/", "", $project->small_story);
            $project->small_story = htmlspecialchars($project->small_story);
            $_project[] = $project;
        }
        //echo '<pre>';
        //print_r($_project);
        header('Content-Type: application/json');
        echo json_encode($_project);
    }

    public function miniProjects(){
        $locale = session('sblocale');
        if(!isset($locale) || !in_array($locale,$this->langs))
            $locale = 'en';

        $projects = Project::where('end_date','>=',date('Y-m-d'))
                    ->skip(0)->take(4)
                    ->where('language',$locale)
                    ->where('status','PU')
                    ->orderBY('target','asc')->get();

        if($projects->isEmpty())
            $projects = Project::where('end_date','>=',date('Y-m-d'))
                ->skip(0)->take(4)
                ->where('language','en')
                ->where('status','PU')
                ->orderBY('target','asc')->get();

        $_project = [];

        foreach($projects AS $project){
            $student = Student::find($project->id_student);
            $project->student = $student;

            //amount funded
            $donations = DB::table('donations')
                ->select(DB::raw('SUM(amount)/100 as total'))
                ->groupBy('status')
                ->where('status','S')
                ->where('id_project',$project->id)
                ->first();

            if($donations) {
                $project->received = $donations->total;
                $project->percent = ceil(($donations->total * 100)/$project->target);
            }else {
                $project->received = $project->percent = 0;
            }

            if(!$project->resume)
                $project->small_story = substr(strip_tags($project->story),0,80);
            else
                $project->small_story = substr(strip_tags($project->resume),0,80);
            $project->small_story = preg_replace( "/\r|\n/", "", $project->small_story);
            $project->small_story = htmlspecialchars($project->small_story);
            $_project[] = $project;
        }
        //echo '<pre>';
        //print_r($_project);
        header('Content-Type: application/json');
        echo json_encode($_project);
    }

    public function mostPopular(){
        //QUERY needed: select id_project, count(id_project) as total from donations where status='S' group by id_project order by total desc
        $locale = session('sblocale');
        if(!isset($locale) || !in_array($locale,$this->langs))
            $locale = 'en';

        $projects = DB::table('donations')
            ->select(DB::raw('COUNT(id_project) as total, id_project'))
            ->groupBy('id_project')
            ->where('status','S')
            ->orderBy('total','desc')
            ->get();

        $_project = [];
        $ct = 0;

        header('Content-Type: application/json');
        foreach($projects AS $p){

            $project = Project::find($p->id_project);

            if($project->language == $locale && $project->status == 'PU') {

                $student = Student::find($project->id_student);
                $project->student = $student;

                //amount funded
                $donations = DB::table('donations')
                    ->select(DB::raw('SUM(amount)/100 as total'))
                    ->groupBy('status')
                    ->where('status', 'S')
                    ->where('id_project', $project->id)
                    ->first();

                if ($donations) {
                    $project->received = $donations->total;
                    $project->percent = ceil(($donations->total * 100) / $project->target);
                } else {
                    $project->received = $project->percent = 0;
                }

                if(!$project->resume)
                    $project->small_story = substr(strip_tags($project->story),0,80);
                else
                    $project->small_story = substr(strip_tags($project->resume),0,80);
                $project->small_story = preg_replace("/\r|\n/", "", $project->small_story);
                $project->small_story = htmlspecialchars($project->small_story);
                $_project[] = $project;

                $ct++;
            }
            if($ct == 4){
                return json_encode($_project);
            }

        }

        return json_encode($_project);
        //echo '<pre>';
        //print_r($_project);
    }

    public function removeReward($id){
        $reward = Reward::find($id);

        if($reward->delete())
            return 'ok';
        else
            return 'error';
    }

    public function search($text){
        $projects = Project::where('title',$text)
                    ->orWhere('title', 'like', '%' . $text . '%')
                    ->orWhere('resume', 'like', '%' . $text . '%')
                    ->orWhere('story', 'like', '%' . $text . '%')->get();

        $_project = [];
        foreach($projects AS $project){
            if($project->status == 'PU' && $project->active=='Y') {
                $student = Student::find($project->id_student);
                $project->student = $student;

                //amount funded
                $donations = DB::table('donations')
                    ->select(DB::raw('SUM(amount)/100 as total'))
                    ->groupBy('status')
                    ->where('status', 'S')
                    ->where('id_project', $project->id)
                    ->first();

                if ($donations) {
                    $project->received = $donations->total;
                    $project->percent = ceil(($donations->total * 100) / $project->target);
                } else {
                    $project->received = $project->percent = 0;
                }

                if(!$project->resume)
                    $project->small_story = substr(strip_tags($project->story),0,80);
                else
                    $project->small_story = substr(strip_tags($project->resume),0,80);
                $project->small_story = preg_replace("/\r|\n/", "", $project->small_story);
                $project->small_story = htmlspecialchars($project->small_story);
                $_project[] = $project;
            }
        }
        //echo '<pre>';
        //print_r($_project);
        header('Content-Type: application/json');
        echo json_encode($_project);
    }

    public function indexadmin($id = null)
    {
        if($id == null) {
            $projects = Project::where('status','!=','R')->orderBy('id', 'asc')->get();
            $_project = [];

            foreach($projects AS $project){
                $student = Student::find($project->id_student);
                $project->student = $student;

                //amount funded
                $donations = DB::table('donations')
                    ->select(DB::raw('SUM(amount)/100 as total'))
                    ->groupBy('status')
                    ->where('status','S')
                    ->where('id_project',$project->id)
                    ->first();

                if($donations) {
                    $project->received = $donations->total;
                    $project->percent = ceil(($donations->total * 100)/$project->target);
                }else {
                    $project->received = $project->percent = 0;
                }

                if(!$project->resume)
                    $project->small_story = substr(strip_tags($project->story),0,80);
                else
                    $project->small_story = substr(strip_tags($project->resume),0,80);
                $project->small_story = preg_replace( "/\r|\n/", "", $project->small_story);
                $project->small_story = htmlspecialchars($project->small_story);
                $_project[] = $project;
            }
            //echo '<pre>';
            //print_r($_project);
            header('Content-Type: application/json');
            echo json_encode($_project);
        }else{
            $project = Project::find($id);
            $student = Student::find($project->id_student);
            $project->student = $student;

            $donations = DB::table('donations')
                ->select(DB::raw('SUM(amount)/100 as total'))
                ->groupBy('status')
                ->where('status','S')
                ->where('id_project',$project->id)
                ->first();

            if($donations) {
                $project->received = $donations->total;
                $project->percent = ceil(($donations->total * 100)/$project->target);
            }else {
                $project->received = $project->percent = 0;
            }

            if(!$project->resume)
                $project->small_story = substr(strip_tags($project->story),0,80);
            else
                $project->small_story = substr(strip_tags($project->resume),0,80);
            $project->small_story = preg_replace( "/\r|\n/", "", $project->small_story);
            $project->small_story = htmlspecialchars($project->small_story);

            return $project;
        }
    }

    public function numberOfMonth($month){
        $months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
        $m = array_search($month,$months) + 1;
        if($m < 10)
            $m = '0'.$m;

        return $m;
    }

    public function formatDay($day){
        if($day<10)
            return '0'.$day;
        else
            return $day;
    }

    public function indexupdates($project_id){
        $updates = Updates::where('id_project',$project_id)->get();
        $arr_updates = [];
        foreach($updates as $up){
            $index = $up->year.$this->numberOfMonth($up->month).$this->formatDay($up->day);
            $up->day = $this->formatDay($up->day);
            $arr_updates[$index] = $up;
        }
        ksort($arr_updates);

        //echo '<pre>';
        //dd($arr_updates);
        return $arr_updates;
    }

    public function indexbackers($project_id){
        $donations = Donation::where('id_project',$project_id)->where('status','S')->orderBy('date','desc')->get();
        $arr_backers = [];
        foreach ($donations as $donation){
            $backer = Donator::where('id',$donation->id_donator)->first();
            $backer->amount = number_format($donation->amount / 100,2);
            $backer->date = substr($donation->date,0,10);

            //get reward
            $reward = Reward::find($donation->id_reward);
            $backer->reward = $reward->title;

            $arr_backers[] = $backer;
        }
        return $arr_backers;
    }

    public function indexcomments($project_id){
        $donations = Donation::where('id_project',$project_id)->where('status','S')->get();
        $arr_backers = [];
        foreach ($donations as $donation){
            if($donation->comment) {
                $backer = Donator::where('id', $donation->id_donator)->first();
                $backer->comment = $donation->comment;
                $arr_backers[] = $backer;
            }
        }
        return $arr_backers;
    }

    public function storeupdates($project_id,Request $request){
        $update = new Updates();
        $update->id_project = $project_id;
        $update->day = $request->input('day');
        $update->month = $request->input('month');
        $update->year = $request->input('year');
        $update->title = $request->input('title');
        $update->description = $request->input('description');
        $update->save();

        return $update;
    }

    public function featured($cat){
        do {
            $cat_projects = CategoryProject::where('id_category', $cat)->inRandomOrder()->get();
            foreach($cat_projects as $cp) {
                $p = Project::where('id',$cp->id_project)->where('status','PU')->where('active','Y')->where('featured','Y')->first();
                if($p)
                    $featured = $p;
            }

            if(!$featured->resume)
                $featured->small_story = substr(strip_tags($featured->story),0,80);
            else
                $featured->small_story = substr(strip_tags($featured->resume),0,80);
            $featured->small_story = preg_replace( "/\r|\n/", "", $featured->small_story);
            $featured->small_story = htmlspecialchars($featured->small_story);

            $student = Student::find($featured->id_student);
            $featured->student = $student->first_name;

            //amount funded
            $donations = DB::table('donations')
                ->select(DB::raw('SUM(amount)/100 as total'))
                ->groupBy('status')
                ->where('status','S')
                ->where('id_project',$featured->id)
                ->first();

            if($donations) {
                $featured->received = $donations->total;
                $featured->percent = ceil(($donations->total * 100)/$featured->target);
            }else {
                $featured->received = $featured->percent = 0;
            }
        }while($featured->active != 'Y');

        return $featured;
    }

    public function deleteupdate($id){
        $update = Updates::where('id',$id)->first();
        $update->delete();
    }

    public function userprojects()
    {
        $student = Student::where('id_user',Auth::user()->id)->first();
        $projects = Project::where('active','Y')->where('id_student',$student->id)->where('status','!=','F')->orderBy('id', 'asc')->get();
        $_project = [];

        foreach($projects AS $project){
            $student = Student::find($project->id_student);
            $project->student = $student;

            //amount funded
            $donations = DB::table('donations')
                ->select(DB::raw('SUM(amount)/100 as total'))
                ->groupBy('status')
                ->where('status','S')
                ->where('id_project',$project->id)
                ->first();

            if($donations) {
                $project->received = $donations->total;
                $project->percent = ceil(($donations->total * 100)/$project->target);
            }else {
                $project->received = $project->percent = 0;
            }

            //$project->small_story = substr($project->story,0,110);
            if(!$project->resume)
                $project->small_story = substr(strip_tags($project->story),0,80);
            else
                $project->small_story = substr(strip_tags($project->resume),0,80);
            $project->small_story = preg_replace( "/\r|\n/", "", $project->small_story);
            $project->small_story = htmlspecialchars($project->small_story);
            $_project[] = $project;
        }


        header('Content-Type: application/json');
        echo json_encode($_project);
    }

    public function findByCategory($id)
    {
        $projects = DB::table('projects')
                    ->join('categories_projects','categories_projects.id_project','=','projects.id')
                    ->where('categories_projects.id_category',$id)
                    ->where('active','Y')
                    ->where('status','PU')
                    ->select('projects.*')
                    ->get();
        $_project = [];

        foreach($projects AS $project){
            $student = Student::find($project->id_student);
            $project->student = $student;

            //amount funded
            $donations = DB::table('donations')
                ->select(DB::raw('SUM(amount)/100 as total'))
                ->groupBy('status')
                ->where('status','S')
                ->where('id_project',$project->id)
                ->first();

            if($donations) {
                $project->received = $donations->total;
                $project->percent = ceil(($donations->total * 100)/$project->target);
            }else {
                $project->received = $project->percent = 0;
            }

            if(!$project->resume)
                $project->small_story = substr(strip_tags($project->story),0,80);
            else
                $project->small_story = substr(strip_tags($project->resume),0,80);
            $project->small_story = preg_replace( "/\r|\n/", "", $project->small_story);
            $project->small_story = htmlspecialchars($project->small_story);
            $_project[] = $project;
        }
        //echo '<pre>';
        //print_r($_project);
        header('Content-Type: application/json');
        echo json_encode($_project);
    }

    public function findCategory($id)
    {
        $cat = CategoryProject::where('id_project',$id)->first();

        echo $cat->id_category;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //get student id by logged user
        $student = Student::where('id_user',Auth::user()->id)->first();
        /*
        if($request->input('user_picture')){
            $student->photo = '/up-picture/'.$request->input('user_picture');
            $student->save();
        }
        */

        $project = new Project();
        $project->id_student = $student->id;
        $project->title = $request->input('title');
        $project->cover = $request->input('cover');
        if($request->input('video'))
            $project->video = $request->input('video');
        $project->target = $request->input('target');
        $project->resume = $request->input('resume');
        $project->story = $request->input('story');
        $project->photo = $request->input('photo');
        $project->currency = $request->input('currency');
        //$project->status = 'D';
        $project->status = $request->input('status');
        $project->start_date = '0000-00-00';
        $project->end_date = substr($request->input('end_date'),0,10);
        $project->active = 'Y';
        if($request->input('language'))
            $project->language = $request->input('language');
        else
            $project->language='en';

        if($project->save()) {

            if($project->status == 'D') { //notification if draft project
                $locale = $student->language;
                if(!$locale)
                    $locale = 'EN';

                //get notification template
                $notification = Notification::where('shortcut', 'project-saved')->where('language',$locale)->first();

                //replace user name to the student name
                $student_name = $student->first_name . ' ' . $student->last_name;
                $content = str_replace('[USER_NAME]', $student_name, $notification->content);

                //send notification
                Mail::send('templates/notification', ['content' => $content], function ($m) use ($student, $student_name, $notification) {
                    $m->from('contact@studentbackr.com', 'Student Backr');
                    $m->to($student->email, $student_name)->subject($notification->name);
                });
            }
            else if($project->status == 'PE') { //notification if pending project

                $locale = $student->language;
                if(!$locale)
                    $locale = 'EN';

                //get notification template
                $notification = Notification::where('shortcut', 'your-project-has-been-sent')->where('language',$locale)->first();

                //replace user name to the student name
                $student_name = $student->first_name . ' ' . $student->last_name;
                $content = str_replace('[USER_NAME]', $student_name, $notification->content);

                //send notification
                Mail::send('templates/notification', ['content' => $content], function ($m) use ($student, $student_name, $notification) {
                    $m->from('contact@studentbackr.com', 'Student Backr');
                    $m->to($student->email, $student_name)->subject($notification->name);
                });
            }

            $cat = new CategoryProject();
            $cat->id_category = $request->input('category');
            $cat->id_project = $project->id;
            $cat->save();

            //creating mangopay wallet
            if($student->mp_user_id){
                require_once(app_path().'/Mangopay/Autoloader.php');
                require_once(app_path().'/Mangopay/base-mangopay.php');

                $api = new MangoPayApi();
                $api->Config->ClientId = CLIENT_ID;
                $api->Config->ClientPassword = CLIENT_PASS;
                $api->Config->TemporaryFolder = TMP_FOLDER;
                $api->Config->BaseUrl = BASE_URL;

                $Wallet = new \MangoPay\Wallet();
                $Wallet->Owners = array($student->mp_user_id);
                $Wallet->Description = "wallet for User ".$student->first_name . ' ' . $student->last_name;
                $Wallet->Currency = $project->currency;
                $result = $api->Wallets->Create($Wallet);

                $project->mp_wallet_id = $result->Id;
                $project->save();

            }

            return $project->id;
        }else
            return 'Error';
    }

    public function savereward(Request $request)
    {
        $log = new Log();
        $log->message = 'start save reward';
        $log->save();

        try {
            if ($request->input('id'))
                $reward = Reward::where('id', $request->input('id'))->first();
            else
                $reward = new Reward();

            $quantity = $request->input('quantity') ? $request->input('quantity') : 0;

            $reward->id_project = $request->input('id_project');
            $reward->title = $request->input('title');
            $reward->description = $request->input('description');
            $reward->quantity = $quantity;
            $reward->minimum = $request->input('minimum');
            //print_r($reward);
            $reward->save();

            $resp = print_r($reward, true);

            $log2 = new Log();
            $log2->message = 'Reward data: '.$resp;
            $log2->save();


            return $resp;
        }catch (Exception $e){
            $log3 = new Log();
            $log3->message = 'Saving reward error: ' . $e->getMessage();
            $log3->save();

            return $e->getMessage();

        }

        //return $reward->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::find($id);

        return $project;
    }

    /**
     * Display all rewards of a specific project
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function rewards($id){
        $rewards = Reward::where('id_project',$id)->orderBy('minimum')->get();

        $_rewards = [];
        foreach($rewards as $r){
            $r->uquantity = $r->quantity>0?false:true;
            $_rewards[] = $r;
        }

        return $_rewards;
    }

    /**
     * Display all donations of a specific project
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function donations($id){
        $donations = Donation::where('id_project',$id)->get();
        $_donations = [];
        $ct = 0;
        foreach($donations as $d){
            $donator = Donator::where('id',$d->id_donator)->get();
            $reward = Reward::where('id',$d->id_reward)->get();

            $_donations[$ct]['donator_name'] = $donator[0]->first_name.' '.$donator[0]->last_name;
            $_donations[$ct]['donator_email'] = $donator[0]->email;
            $_donations[$ct]['date'] = $d->date;
            $_donations[$ct]['amount'] = $d->amount;
            $_donations[$ct]['currency'] = $d->currency;
            $_donations[$ct]['status'] = $d->status;
            $_donations[$ct]['reward_name'] = $reward[0]->title;
            $_donations[$ct]['mp_return'] = $d->mp_return;

            $ct++;
        }

        header('Content-Type: application/json');
        echo json_encode($_donations);
    }

    public function lastDonations(){
        $limit = 30;

        $donations = Donation::orderBy('date','desc')->take($limit)->get();

        $_donations = [];
        $ct = 0;
        foreach($donations as $d){
            $donator = Donator::where('id',$d->id_donator)->get();
            $reward = Reward::where('id',$d->id_reward)->first();
            $project = Project::find($d->id_project);

            $donator_name = $donator_email = $reward_title = ''; 
            if($donator[0]->first_name)
                $donator_name = $donator[0]->first_name;
            if($donator[0]->last_name)
                $donator_name.= $donator[0]->last_name;
            if($donator[0]->email)
                $donator_email = $donator[0]->email;

            if(isset($reward->title))
                $reward_title = $reward->title;

            $_donations[$ct]['id'] = $d->id;
            $_donations[$ct]['project'] = $project->title;
            $_donations[$ct]['donator_name'] = $donator_name;
            $_donations[$ct]['donator_email'] = $donator_email;
            $_donations[$ct]['date'] = $d->date;
            $_donations[$ct]['amount'] = $d->amount / 100;
            $_donations[$ct]['currency'] = $d->currency;
            $_donations[$ct]['status'] = $d->status;
            $_donations[$ct]['reward_name'] = $reward_title;
            $_donations[$ct]['mp_return'] = $d->mp_return;
            $_donations[$ct]['mp_id'] = $d->mp_id;

            require_once(app_path().'/Mangopay/Autoloader.php');
            require_once(app_path().'/Mangopay/base-mangopay.php');

            $api = new MangoPayApi();
            $api->Config->ClientId = CLIENT_ID;
            $api->Config->ClientPassword = CLIENT_PASS;
            $api->Config->TemporaryFolder = TMP_FOLDER;
            $api->Config->BaseUrl = BASE_URL;

            if($d->mp_id){
                $result = $api->PayIns->Get($d->mp_id);
                $mp_msg = $result->ResultMessage;
            }
            else
                $mp_msg = '';

            $_donations[$ct]['message'] = $mp_msg;

            $ct++;
        }

        header('Content-Type: application/json');
        echo json_encode($_donations);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $project = Project::find($id);

        if(Auth::user()->rule == 'S') //if rule is student, get student id by logged user
            $student = Student::where('id_user',Auth::user()->id)->first();
        else //if logged user is SB Team
            $student = Student::find($project->id_student);

        $old_status = $project->status;

        $project->id_student = $request->input('id_student');
        $project->title = $request->input('title');
        $project->photo = $request->input('photo');
        $project->cover = $request->input('cover');
        if($request->input('video'))
            $project->video = $request->input('video');
        $project->currency = $request->input('currency');
        $project->target = $request->input('target');
        if($request->input('status')=='PU')
            $project->start_date = date('Y-m-d');
        else
            $project->start_date = '0000-00-00';
        $project->end_date = $request->input('end_date');
        $project->resume = $request->input('resume');
        $project->story = $request->input('story');
        $project->status = $request->input('status');
        $project->currency = $request->input('currency');
        $project->featured = $request->input('featured');
        if($request->input('language'))
            $project->language = $request->input('language');
        else
            $project->language='en';

        //creating mangopay wallet
        if($student->mp_user_id && !$project->mp_wallet_id){
            require_once(app_path().'/Mangopay/Autoloader.php');
            require_once(app_path().'/Mangopay/base-mangopay.php');

            $api = new MangoPayApi();
            $api->Config->ClientId = CLIENT_ID;
            $api->Config->ClientPassword = CLIENT_PASS;
            $api->Config->TemporaryFolder = TMP_FOLDER;
            $api->Config->BaseUrl = BASE_URL;

            $Wallet = new \MangoPay\Wallet();
            $Wallet->Owners = array($student->mp_user_id);
            $Wallet->Description = "wallet for User ".$student->first_name . ' ' . $student->last_name;
            $Wallet->Currency = $project->currency;
            $result = $api->Wallets->Create($Wallet);

            $project->mp_wallet_id = $result->Id;
        }

        $project->save();
        //saving category
        CategoryProject::where('id_project',$project->id)->delete();
        $cat = new CategoryProject();
        $cat->id_project = $project->id;
        $cat->id_category = $request->input('id_category');
        $cat->save();
        $cat->save();

        if($old_status != 'PU' && $project->status == 'PU'){ //Project has Published

            $locale = $student->language;
            if(!$locale)
                $locale = 'EN';

            //get notification template
            $notification = Notification::where('shortcut','your-project-is-now-online')->where('language',$locale)->first();

            //replace user name to the student name
            $student_name = $student->first_name.' '.$student->last_name;
            $content = str_replace('[USER_NAME]',$student_name,$notification->content);

            //send notification
            Mail::send('templates/notification',['content'=>$content],function($m) use ($student,$student_name,$notification){
                $m->from('contact@studentbackr.com','Student Backr');
                $m->to($student->email,$student_name)->subject($notification->name);
            });

        }
        else if($project->status == 'PE' && Auth::user()->rule == 'S') { //notification if pending project and logged user is a student
            $locale = $student->language;
            if(!$locale)
                $locale = 'EN';

            //get notification template
            $notification = Notification::where('shortcut', 'your-project-has-been-sent')->where('language',$locale)->first();

            //replace user name to the student name
            $student_name = $student->first_name . ' ' . $student->last_name;
            $content = str_replace('[USER_NAME]', $student_name, $notification->content);

            //send notification
            Mail::send('templates/notification', ['content' => $content], function ($m) use ($student, $student_name, $notification) {
                $m->from('contact@studentbackr.com', 'Student Backr');
                $m->to($student->email, $student_name)->subject($notification->name);
            });
        }
        else if($project->status == 'D' && Auth::user()->rule == 'S'){ //Draft project saved and logged user is a student
            $locale = $student->language;
            if(!$locale)
                $locale = 'EN';

            //get notification template
            $notification = Notification::where('shortcut','project-saved')->where('language',$locale)->first();

            //replace user name to the student name
            $student_name = $student->first_name.' '.$student->last_name;
            $content = str_replace('[USER_NAME]',$student_name,$notification->content);

            //send notification
            Mail::send('templates/notification',['content'=>$content],function($m) use ($student,$student_name,$notification){
                $m->from('contact@studentbackr.com','Student Backr');
                $m->to($student->email,$student_name)->subject($notification->name);
            });

        }

        echo $project->id;
        //return 'Project successfully updated';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        CategoryProject::where('id_project',$id)->delete();
//        Reward::where('id_project',$id)->delete();
//        Donation::where('id_project',$id)->delete();
//
//        $project = Project::find($id);
//        $project->delete();

        $project = Project::find($id);
        $project->status = 'R';
        $project->save();

        return 'Project successfully deleted';
    }

    public function countries(){
        $countries = Country::all();
        echo $countries;
    }

    public function payout($id_project,Request $request){
        $msg = '';
        if($request->input('act')) {

            require_once(app_path().'/Mangopay/Autoloader.php');
            require_once(app_path().'/Mangopay/base-mangopay.php');

            $api = new MangoPayApi();
            $api->Config->ClientId = CLIENT_ID;
            $api->Config->ClientPassword = CLIENT_PASS;
            $api->Config->TemporaryFolder = TMP_FOLDER;
            $api->Config->BaseUrl = BASE_URL;

            try {
                $PayOut = new \MangoPay\PayOut();
                $PayOut->AuthorId = $request->input('author_id');
                $PayOut->DebitedWalletID = $request->input('wallet_id');
                $PayOut->DebitedFunds = new \MangoPay\Money();
                $PayOut->DebitedFunds->Currency = $request->input('currency');
                $PayOut->DebitedFunds->Amount = $request->input('amount');
                $PayOut->Fees = new \MangoPay\Money();
                $PayOut->Fees->Currency = $request->input('currency');
                $PayOut->Fees->Amount = $request->input('fees');
                $PayOut->PaymentType = "BANK_WIRE";
                $PayOut->MeanOfPaymentDetails = new \MangoPay\PayOutPaymentDetailsBankWire();

                $PayOut->MeanOfPaymentDetails->BankAccountId = $request->input('bank_id');

                if($result = $api->PayOuts->Create($PayOut))
                    $msg = "Payout gerenated Successfully";
            }
            catch(\MangoPay\Libraries\Exception $e){
                $msg = 'Error: ' . $e;
            }
        }

        $project = Project::find($id_project);
        $donations = Donation::where('id_project',$id_project)->where('status','S')->get();

        $student = Student::find($project->id_student);

        $total_funded = 0;
        foreach($donations as $d){
            $total_funded+= $d->amount;
        }

        return view('projects.payout')
                ->with('project',$project)
                ->with('donations',$donations)
                ->with('total_funded',$total_funded)
                ->with('student',$student)
                ->with('msg',$msg);
    }
}
