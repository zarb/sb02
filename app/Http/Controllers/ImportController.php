<?php

namespace App\Http\Controllers;

use App\Donation;
use App\Donator;
use App\Http\Requests;
use App\Project;
use App\Reward;
use App\Student;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ImportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $url = 'http://old.studentbackr.com/dump_projects.php';
        $dados = file_get_contents($url);
        $dados = json_decode($dados);


        //echo '<pre>';
        //print_r($dados);
        //die();


        foreach($dados as $item){
            $project = new Project();
            $project->title = $item->title;

            if($item->photo){

                try {
                    $parts = explode(".", $item->photo);
                    $name_no_space = str_replace(' ', '', $project->title);
                    $ext = end($parts);

                    $img_in = @file_get_contents($item->photo);
                    $img_out = 'img_' . $name_no_space . "." . $ext;
                    if (!file_put_contents('projects_photos/' . $img_out, $img_in))
                        echo 'erro em ' . $img_out;

                    $project->photo = $img_out;
                    $project->cover = $img_out;
                }catch (Exception $e){
                    echo '<br><br>Error in '.$item->photo.'<br><br>';
                }
            }else
                $project->photo = '';


            #CREATE A NEW USER FOR THE STUDENT
            $s_old = $item->student;
            $s_user = new User();
            $s_user->name = $s_old->first_name . ' ' . $s_old->last_name;
            $s_user->email = $s_old->email;
            $s_user->password = Hash::make($s_old->dob);
            $s_user->rule = 'S';
            $s_user->active = 'Y';
            $s_user->save();
            echo "<br>User id: ".$s_user->id;
            #END USER CREATION

            #CREATE STUDENT TO ADD IN THE PROJECT
            $student = new Student();
            $student->id_user = $s_user->id;
            $student->first_name = $s_old->first_name;
            $student->last_name = $s_old->last_name;
            $student->email = $s_old->email;
            $tmp_sphoto = explode('studentbackr/',$s_old->photo);
            if(isset($tmp_sphoto[1])){
                try {
                    $url_Sphoto = 'http://studentbackr.com/' . $tmp_sphoto[1];
                    $parts = explode(".", $url_Sphoto);
                    $ext = end($parts);

                    $imgS_in = @file_get_contents($url_Sphoto);
                    $nameS_no_space = str_replace(' ', '', $student->first_name) . rand(1, 1000);
                    $imgS_out = 'img_' . $nameS_no_space . "." . $ext;

                    if (!file_put_contents('profile_photos/' . $imgS_out, $imgS_in))
                        echo 'erro 2 em ' . $imgS_out;

                    $student->photo = $imgS_out;
                }catch (Exception $e){
                    echo '<br><br>Error 2 in '.$tmp_sphoto[1].'<br><br>';
                }

            }
            else
                $student->photo = $s_old->photo;


            /*
            $tmp_sphoto = explode('studentbackr/',$s_old->photo);
            if(isset($tmp_sphoto[1]))
                $student->photo = $tmp_sphoto[1];
            else
                $student->photo = $s_old->photo;
            */
            $student->dob = $s_old->dob;
            //$student->gender = $s_old[''];
            if(!$s_old->id_country) // if without country set it as UNDEFINED
                $s_old->id_country = '256';
            $student->id_country = $s_old->id_country;
            $student->city = $s_old->city;
            $student->address = $s_old->address;
            $student->phone = $s_old->phone;
            $student->annual_income = $s_old->annual_income;
            $student->biographical_inf = $s_old->biographical_inf;
            $student->kyc_document = $s_old->kyc_document;
            $student->mp_user_id = $s_old->mp_user_id;
            $student->mp_bank_id = $s_old->mp_bank_id;
            $student->mp_type = $s_old->mp_type;
            $student->save();
            echo "<br>Student id: ".$student->id;
            #END STUDENT CREATION

            $project->id_student = $student->id;
            $project->video = $item->video;
            $project->currency = $item->currency;
            $project->target = $item->target;
            $project->start_date = $item->start_date;
            $project->end_date = $item->end_date;
            $project->story = $item->story;
            $project->status = $item->status;
            $project->mp_wallet_id = $item->mp_wallet_id;
            $project->active = 'Y';
            $project->language = substr($item->language,0,2);
            $project->save();
            echo "<br>Project id: ".$project->id;

            //INSERTING REWARDS
            foreach ($item->rewards as $old_reward) {
                $reward = new Reward();
                $reward->id_project = $project->id;
                $reward->title = $old_reward->title;
                $reward->description = $old_reward->description;
                $reward->quantity = $old_reward->quantity;
                $reward->minimum = $old_reward->minimum;
                $reward->save();
                echo "<br>Reward id: ".$reward->id;

                //INSERTING DONATOR AND DONATION
                foreach($old_reward->donations as $old_donation){
                    $donator = new Donator();
                    $donator->first_name = $old_donation->name;
                    $donator->email = $old_donation->email;
                    $donator->save();
                    echo "<br>&nbsp;&nbsp;Donator id: ".$donator->id;

                    $donation = new Donation();
                    $donation->id_project = $project->id;
                    $donation->id_reward = $reward->id;
                    $donation->id_donator = $donator->id;
                    $donation->date = $old_donation->date;
                    $donation->amount = ($old_donation->amount * 100);
                    $donation->save();
                    echo "<br>&nbsp;&nbsp;Donator id: ".$donation->id;
                }
            }
            //echo '<pre>';
            //print_r($project);
            //die();

        }

        print_r($dados);
        //return view('import.index');
    }

    public function import_users()
    {
        $url = 'http://old.studentbackr.com/dump_projects.php';
        $dados = file_get_contents($url);
        $dados = json_decode($dados);


        //echo '<pre>';
        //print_r($dados);
        //die();


        foreach($dados as $item){
            #CREATE A NEW USER FOR THE STUDENT
            $s_old = $item;

            //checking if user already exists
            $tmp_user = Student::where('email',$item->email)->first();
            if(!$tmp_user) {
                $s_user = new User();
                $s_user->name = $s_old->first_name . ' ' . $s_old->last_name;
                $s_user->email = $s_old->email;
                $s_user->password = Hash::make($s_old->dob);
                $s_user->rule = 'S';
                $s_user->active = 'Y';
                $s_user->save();
                #END USER CREATION

                #CREATE STUDENT TO ADD IN THE PROJECT
                $student = new Student();
                $student->id_user = $s_user->id;
                $student->first_name = $s_old->first_name;
                $student->last_name = $s_old->last_name;
                $student->email = $s_old->email;
                $tmp_sphoto = explode('studentbackr/', $s_old->photo);
                if (isset($tmp_sphoto[1])) {
                    try {
                        $url_Sphoto = 'http://studentbackr.com/' . $tmp_sphoto[1];
                        $parts = explode(".", $url_Sphoto);
                        $ext = end($parts);

                        $imgS_in = @file_get_contents($url_Sphoto);
                        $nameS_no_space = str_replace(' ', '', $student->first_name) . rand(1, 1000);
                        $imgS_out = 'img_' . $nameS_no_space . "." . $ext;

                        if (!file_put_contents('profile_photos/' . $imgS_out, $imgS_in))
                            echo 'erro 2 em ' . $imgS_out;

                        $student->photo = $imgS_out;
                    } catch (Exception $e) {
                        echo '<br><br>Error 2 in ' . $tmp_sphoto[1] . '<br><br>';
                    }
                } else
                    $student->photo = $s_old->photo;


                /*
                $tmp_sphoto = explode('studentbackr/',$s_old->photo);
                if(isset($tmp_sphoto[1]))
                    $student->photo = $tmp_sphoto[1];
                else
                    $student->photo = $s_old->photo;
                */
                $student->dob = $s_old->dob;
                //$student->gender = $s_old[''];
                //if (!$s_old->id_country) // if without country set it as UNDEFINED
                    $s_old->id_country = '256';
                $student->id_country = $s_old->id_country;
                $student->city = $s_old->city;
                $student->address = $s_old->address;
                $student->phone = $s_old->phone;
                $student->annual_income = $s_old->annual_income;
                $student->biographical_inf = $s_old->biographical_inf;
                $student->kyc_document = $s_old->kyc_document;
                $student->mp_user_id = $s_old->mp_user_id;
                $student->mp_bank_id = $s_old->mp_bank_id;
                $student->mp_type = $s_old->mp_type;
                if($student->save())
                    echo $student->email.'<br>';
                else
                    echo '[ERRO]'.$student->email.'<br>';
                #END STUDENT CREATION

            }

        }

        //print_r($dados);
        //return view('import.index');
    }

    public function emails(){
        $users = User::all();
        foreach($users as $u){
            echo strtolower($u->email).'<br>';
        }
    }
}
