<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

use App\Http\Requests;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        if($id == null)
            return Page::orderBy('id','asc')->get();
        else
            return $this->show($id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $page = new Page();
        $page->title = $request->input('title');
        $page->content = $request->input('content');
        $page->language = $request->input('language');
        $page->shortcut = $request->input('shortcut');
        $page->seo_title = $request->input('seo_title');
        $page->seo_description = $request->input('seo_description');
        $up_status = 1;

        if ( !empty( $_FILES ) ) {
            $file_name = rand(10000, 99999) . $_FILES['file']['name'];
            $file_src = 'seo_photos/' . $file_name;
            if (move_uploaded_file($_FILES['file']['tmp_name'], $file_src)) {
                $page->seo_image = $file_name;
            }else
                $up_status = 2;
        }

        $page->save();

        return 'Page successfully created';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = Page::find($id);

        return $page;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $page = Page::find($id);
        $page->title = $request->input('title');
        $page->content = $request->input('content');
        $page->language = $request->input('language');
        $page->shortcut = $request->input('shortcut');
        $page->seo_title = $request->input('seo_title');
        $page->seo_description = $request->input('seo_description');
        $page->seo_image = $request->input('seo_image');
        
        $page->save();

        return 'Page successfully updated';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $page = Page::find($request->input('id'));
        $page->delete();

        return 'Page successfully deleted';
    }

}
