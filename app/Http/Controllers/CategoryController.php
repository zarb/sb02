<?php

namespace App\Http\Controllers;

use App\Category;
use App\Project;
use App\CategoryProject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        $user = Auth::user();
        $comum = true;
        if($user){
            if($user->rule == 'A')
                $comum = false;
        }

        if($id == null){
            $locale = session('sblocale')?session('sblocale'):'EN';
            if(isset($locale) && $comum)
                $categories = Category::where('language',$locale)->orderBy('id','asc')->get();
            else
                $categories = Category::orderBy('id','asc')->get();

            return $categories;
        }
        else{
            $category = Category::find($id);

            return $category;
        }

    }

    public function getByLanguage($lang = 'EN'){
        $categories = Category::where('language',$lang)->orderBy('id','asc')->get();

        return $categories;
    }

    public function featured($id = null)
    {
        $locale = session('sblocale');
        if(!$locale)
            $locale = 'EN';
        if($id == null) {
            $categories = Category::where('language',$locale)->inRandomOrder()->get();
            //foreach in categories to search published projects
            $arr_cat = $temp_cat = [];
            foreach($categories as $cat){
                //search published projects
                $cat_projects = CategoryProject::where('id_category', $cat->id)->get();
                foreach($cat_projects as $cp){
                    $p = Project::where('id',$cp->id_project)->where('status','PU')->where('active','Y')->where('featured','Y')->first();
                    if($p)
                        $temp_cat[$cp->id_category]=1;
                }
            }

            foreach($temp_cat as $id_cat => $temp){
                $c = Category::find($id_cat);
                $arr_cat[] = $c;
            }

            return $arr_cat;
        }else
            return $this->show($id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Category();
        $category->name = $request->input('name');
        $category->active = $request->input('active');
        $category->language = $request->input('language');

        $category->save();

        return 'Category successfully created';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id);

        return $category;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $category->name = $request->input('name');
        $category->active = $request->input('active');
        $category->language = $request->input('language');

        $category->save();

        return 'Category successfully updated';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $category = Category::find($request->input('id'));
        $category->delete();

        return 'Category successfully deleted';
    }

}
