<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/import', 'ImportController@index');
//Route::get('/import_users', 'ImportController@import_users');

Route::auth();

Route::post('/api/v1/projectphotos/{id}',function($id){

    $photos = json_decode($_POST['photos']);
    foreach ($photos as $photo_name){
        $photo = new \App\ProjectPhotos();
        $photo->id_project = $id;
        $photo->photo = $photo_name;
        $photo->save();
    }
});

Route::get('emails','ImportController@emails');

Route::post('/up-cover', function(){
    if ( !empty( $_FILES ) ) {
        $file_name = rand(10000, 99999) . $_FILES['file']['name'];
        $file_src = 'projects_photos/' . $file_name;
        if (move_uploaded_file($_FILES['file']['tmp_name'], $file_src)) {
            echo $file_name;
        }else
            echo 'upload error';
    }
});

Route::post('/up-seo', function(){
    if ( !empty( $_FILES ) ) {
        $file_name = rand(10000, 99999) . $_FILES['file']['name'];
        $file_src = 'seo_photos/' . $file_name;
        if (move_uploaded_file($_FILES['file']['tmp_name'], $file_src)) {
            echo $file_name;
        }else
            echo 'upload error';
    }
});

Route::post('/up-picture', function(){
    if ( !empty( $_FILES ) ) {
        $file_name = rand(10000, 99999) . $_FILES['file']['name'];
        $file_src = 'profile_photos/' . $file_name;
        if (move_uploaded_file($_FILES['file']['tmp_name'], $file_src)) {
            $user = Auth::user();
            $student = \App\Student::where('id_user',$user->id)->first();
            $student->photo = $file_name;
            $student->save();

            echo $file_name;
        }else
            echo 'upload error';
    }
});

Route::post('/up-project-picture', function(){
    if ( !empty( $_FILES ) ) {
        $file_name = rand(10000, 99999) . $_FILES['file2']['name'];
        $file_src = 'projects_photos/' . $file_name;
        if (move_uploaded_file($_FILES['file2']['tmp_name'], $file_src)){
            echo $file_name;
        }
        else
            echo 'upload error';
    }
});

Route::get('/admin/students', function () {
    return view('students.index');
});

Route::get('/admin/projects', function () {
    $categories = \App\Category::all();
    return view('projects.index',['categories'=>$categories]);
});
Route::get('/admin/payout/{id}', 'ProjectController@payout');
Route::post('/admin/payout/{id}', 'ProjectController@payout');
Route::get('/admin/categories', function () {
    return view('categories.index');
});
Route::get('/admin/pages', function () {
    return view('pages.index');
});
Route::get('/admin/notifications', function () {
    return view('notifications.index');
});
Route::get('/admin/donations', function () {
    return view('donations.index');
});
Route::get('admin',function(){
    return view('admin_template');
});

Route::get('/home', function(){
    $user = Auth::user();
    if($user->rule == 'A' || $user->rule == 'T')
        return view('projects.index');
    else{
        return redirect()->action('FrontController@callonboard');
    }
});

Route::get('/', 'FrontController@index');
Route::get('/phpinfo', function(){
    phpinfo();
});
Route::get('/contact/{lang?}', 'FrontController@contact');
Route::post('/contact', function (){
    //sending e-mail to contact@studentbackr.com
    Mail::send('templates/contact',['dados'=>$_POST],function($m){
        $m->from('contact@studentbackr.com','Student Backr');
        $m->to('contact@studentbackr.com','StudentBackr')->subject('New Contact');
    });

    //sending e-mail to user
    Mail::send('templates/contact',['dados'=>$_POST],function($m){
        $m->from('contact@studentbackr.com','Student Backr');
        $m->to($_POST['email'],$_POST['first_name'])->subject('New Contact');
    });

    return view('front.contact',['msg'=>'Your message was successfully submitted. Thank you!']);
});
//Route::post('/contact', 'FrontController@send_contact');
Route::get('/budget-calculator/{lang?}', function ($lang = 'en'){

    app()->setLocale($lang);
    return view('front.calculator');
});
Route::get('/all-projects/{lang?}', 'FrontController@allprojects');
Route::get('/signup/{lang?}', 'FrontController@callregister');
Route::post('/signup/{lang?}', 'FrontController@register');
Route::get('/profile/{lang?}', 'FrontController@callprofile');
Route::get('/my-account/{lang?}', function($lang = 'en'){
    $user = Auth::user();
    if(!$user)
        return redirect('/login/'.$lang);
    else
        return redirect()->action('FrontController@callprofile');
});
Route::get('/profile/edit/{i?}', 'FrontController@calleditprofile');
Route::get('/wizard/{id?}', 'FrontController@callwizard');
Route::get('/donate/{id}', 'FrontController@calldonate');
Route::get('/project/{id}', 'FrontController@callproject');
Route::get('/onboard/{lang?}', 'FrontController@callonboard');
Route::post('/payinfo/{id}', 'FrontController@callpayinfo');
Route::get('/payment/', 'FrontController@callpayment');
Route::get('/sponsorship-programme/{lang?}', function($lang='en'){
    app()->setLocale($lang);

    $page = 'sponsorship-programme';
    $uri_path = 'http://www.studentbackr.com/' . $page. '/' . $lang;

    return view('front.sponsors',['uri_path'=>$uri_path]);
});
Route::get('/partnerships/{lang?}', function($lang='en'){
    app()->setLocale($lang);

    $page = 'partnerships';
    $uri_path = 'http://www.studentbackr.com/' . $page. '/' . $lang;

    return view('front.partnerships',['uri_path'=>$uri_path]);
});
Route::get('/press/{lang?}', function($lang = 'en'){
    app()->setLocale($lang);

    $page = 'press';
    $uri_path = 'http://www.studentbackr.com/' . $page. '/' . $lang;
    
    return view('front.press',['uri_path'=>$uri_path]);
});
Route::get('/crowdfunding-in-60-seconds/{lang?}', function($lang = 'en'){
    app()->setLocale($lang);

    $page = 'crowdfunding-in-60-seconds';
    $uri_path = 'http://www.studentbackr.com/' . $page. '/' . $lang;

    return view('front.in60seconds',['lang'=>$lang,'uri_path'=>$uri_path]);
});
Route::get('/{page?}/{lang?}', 'FrontController@callcontent');

/*
//other language routes
Route::get('/signup/{lang}', function(){
    $lang = Request::segment(2);


    return redirect()->action('FrontController@callregister');
});
//end other language routes
*/

//api routes
Route::get('/api/v1/comments/{project_id}', 'ProjectController@indexcomments');
Route::get('/api/v1/backers/{project_id}', 'ProjectController@indexbackers');
//updates begin
Route::get('/api/v1/updates/{project_id}', 'ProjectController@indexupdates');
Route::get('/api/v1/delete_update/{id}', 'ProjectController@deleteupdate');
Route::post('/api/v1/updates/{project_id}', 'ProjectController@storeupdates');
//updates end
//student begin
Route::get('/api/v1/students/{id?}', 'StudentController@index');
Route::get('/api/v1/student', 'StudentController@logged');
Route::post('/api/v1/students/', 'StudentController@store');
Route::post('/api/v1/students/{id}', 'StudentController@update');
Route::delete('/api/v1/students/{id}', 'StudentController@destroy');
Route::post('/api/v1/students/newmpuser/{id}', 'StudentController@newmpuser');
//students end

//all countries
Route::get('/api/v1/countries', 'StudentController@countries');

//projects begin
Route::get('/api/v1/projects/{id?}', 'ProjectController@index');
Route::get('/api/v1/projects/search/{text}', 'ProjectController@search');
Route::get('/api/v1/projectsadmin/{id?}', 'ProjectController@indexadmin');
Route::get('/api/v1/urgent', 'ProjectController@mostUrgent');
Route::get('/api/v1/recent', 'ProjectController@mostRecent');
Route::get('/api/v1/mini', 'ProjectController@miniProjects');
Route::get('/api/v1/popular', 'ProjectController@mostPopular');
Route::get('/api/v1/featured/{id}', 'ProjectController@featured');
Route::get('/api/v1/userprojects/', 'ProjectController@userprojects');
Route::get('/api/v1/projects/cat/{id}', 'ProjectController@findByCategory');
Route::get('/api/v1/projects/category/{id}', 'ProjectController@findCategory');
Route::post('/api/v1/projects/', 'ProjectController@store');
Route::post('/api/v1/projects/{id}', 'ProjectController@update');
Route::get('/api/v1/projects/remove/{id}', 'ProjectController@destroy');
Route::get('/api/v1/projects/deletephoto/{id}',function($id){
    \App\ProjectPhotos::find($id)->delete();
});
Route::get('/api/v1/projects/rewards/{id?}', 'ProjectController@rewards');
Route::post('/api/v1/reward/', 'ProjectController@savereward');
Route::get('/api/v1/reward/remove/{id}', 'ProjectController@removeReward');
Route::get('/api/v1/projects/donations/{id?}', 'ProjectController@donations');
Route::get('/api/v1/lastdonations/', 'ProjectController@lastDonations');
//projects end

//categories begin
Route::get('/api/v1/featuredcategories/{id?}', 'CategoryController@featured');
Route::get('/api/v1/categories/{id?}', 'CategoryController@index');
Route::get('/api/v1/categoriesbylang/{lang?}', 'CategoryController@getByLanguage');
Route::post('/api/v1/categories/', 'CategoryController@store');
Route::post('/api/v1/categories/{id}', 'CategoryController@update');
Route::delete('/api/v1/categories/{id}', 'CategoryController@destroy');
//categories end

//pages begin
Route::get('/api/v1/pages/{id?}', 'PageController@index');
Route::post('/api/v1/pages/', 'PageController@store');
Route::post('/api/v1/pages/{id}', 'PageController@update');
Route::post('/api/v1/uppages',function(){
    print_r($_REQUEST);
});
Route::delete('/api/v1/pages/{id}', 'PageController@destroy');
//pages end

//notifications begin
Route::get('/api/v1/notifications/{id?}', 'NotificationController@index');
Route::post('/api/v1/notifications/', 'NotificationController@store');
Route::post('/api/v1/notifications/{id}', 'NotificationController@update');
Route::delete('/api/v1/notifications/{id}', 'NotificationController@destroy');
//notifications end
//API end



//TESTS
//Route::get('/home/es', 'HomeController@index');
//Route::get('students/index', 'StudentController@show');
//Route::get('student/{id}', 'StudentController@show');


