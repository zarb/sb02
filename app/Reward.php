<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model{

    protected $fillable = array('id_project','title','description','minimum','quantity','purchase');

}