<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model{

    protected $fillable = array('name','shortcut','content','language');

}